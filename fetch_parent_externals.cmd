echo "verso-demokit: Fetching externals from repositories..."

IF NOT EXIST ..\verso-imgui (
	git clone https://gitlab.com/dahliazone/pc/lib/verso-imgui.git ..\verso-imgui
) else (
	pushd ..\verso-imgui
	git pull --rebase
	popd
)

echo "verso-imgui: Resursing into..."
pushd ..\verso-imgui
call fetch_parent_externals.cmd
popd

echo "verso-demokit: All done"
