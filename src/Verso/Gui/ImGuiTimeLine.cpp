#include <Verso/Gui/ImGuiTimeLine.hpp>
#include <Verso/System/Logger.hpp>
#include <Verso/Time/Timestamp.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Input/Keyboard/KeyScancode.hpp>
#include <Verso/DemoKit/DemoPlayerSettings.hpp>
#include <cmath>

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui.h>
#include <imgui_internal.h>

namespace Verso {


bool timelineElement(float startSeconds, float lengthSeconds, float viewScrollPosSeconds, float viewWidthSeconds,
					 float row, float rowHeight, float elementHeight, float topBarHeight,
					 size_t id, const char* content)
{
	bool clicked = false;

	ImGui::PushID(1245 + static_cast<int>(id) * 1000);

	ImGuiWindow* imGuiWindow = ::ImGui::GetCurrentWindow();
	ImDrawList* dl = imGuiWindow->DrawList;
	const ImGuiWindowTempData& dc = imGuiWindow->DC;
	const ImGuiStyle& style = ImGui::GetStyle();
	const ImVec2 padding = style.WindowPadding;
	const float rounding = 2.0f;

	const ImVec2 visibleSize = ImVec2(imGuiWindow->Size.x - padding.x, 0);
	float widthOfASecond = visibleSize.x / viewWidthSeconds;

	const ImVec2 pos((startSeconds - viewScrollPosSeconds) * widthOfASecond,
					 topBarHeight + row * rowHeight + (rowHeight-elementHeight) / 2.0f);
	const ImRect elementRect(
				dc.CursorPos + ImVec2(pos.x, pos.y),
				dc.CursorPos + ImVec2(pos.x + lengthSeconds * widthOfASecond, pos.y + elementHeight));

	float hue = id * 0.05f;
	ImGui::PushStyleColor(ImGuiCol_Button, ImColor::HSV(hue, 0.6f, 0.6f).Value);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImColor::HSV(hue, 0.7f, 0.7f).Value);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImColor::HSV(hue, 0.8f, 0.8f).Value);

	ImU32 col = ImGui::GetColorU32(ImGuiCol_Button, 1.0f);
	if (ImGui::IsMouseHoveringRect(elementRect.Min, elementRect.Max)) {
		col = ImGui::GetColorU32(ImGuiCol_ButtonHovered, 1.0f);

		// Handle input
		if (ImGui::GetIO().MouseClicked[0] == true) {
			clicked = true;
		}
	}

	dl->AddRectFilled(elementRect.Min, elementRect.Max, col, rounding, 0); // ImGuiCorner_TopLeft | ImGuiCorner_TopRight

	float textPaddingLeft = 5.0f;
	float textPaddingRight = 2.0f;
	const ImRect textRect(elementRect.Min + ImVec2(textPaddingLeft, 0), elementRect.Max - ImVec2(textPaddingRight, 0));
	ImGui::RenderTextClipped(textRect.Min, textRect.Max, content, nullptr, nullptr, ImVec2(0.0f,0.5f));

	ImGui::PopStyleColor(3);
	ImGui::PopID();

	return clicked;
}


bool timeLineTimeMeasure(DemoPlayerSettings& settings, float* timeNowSeconds, float* viewScrollPosSeconds, float* viewWidthSeconds,
						 DemoPart** selected, float topBarHeight)
{
	bool changed = false;

	ImGuiWindow* imGuiWindow = ::ImGui::GetCurrentWindow();
	ImDrawList* dl = imGuiWindow->DrawList;
	ImGuiWindowTempData& dc = imGuiWindow->DC;
	ImGuiStyle& style = ImGui::GetStyle();
	const ImVec2 padding = style.WindowPadding;
	const float rounding = 0.0f; //6.0f;

	const ImVec2 topBarSize(imGuiWindow->Size.x - padding.x, topBarHeight);
	const ImRect topBarSizeRect(dc.CursorPos, dc.CursorPos + topBarSize);
	const ImVec2 timelineSize(topBarSize.x, imGuiWindow->Size.y - padding.y);
	const ImRect timelineSizeRect(dc.CursorPos, dc.CursorPos + timelineSize);

	float widthOfASecond = topBarSize.x / (*viewWidthSeconds);
	const ImVec2 fullSizePos
			(dc.CursorPos.x - widthOfASecond * (*viewScrollPosSeconds),
			 dc.CursorPos.y);
	const ImVec2 fullSize = ImVec2(static_cast<float>(widthOfASecond * settings.general.duration), topBarHeight);
	const ImRect fullSizeRect(
				fullSizePos,
				fullSizePos + fullSize);

	// Set interaction region
	const char idLabel[] = "demoKitTimeLineTimeBar";
	const ImGuiID id = imGuiWindow->GetID(idLabel);
	if (ImGui::ItemAdd(topBarSizeRect, id)) {
		ImU32 inRangeCol = ImGui::ColorConvertFloat4ToU32(ImVec4(0.7f, 0.7f, 0.7f, 1.0f)); // \TODO: style.Colors
		ImU32 inRangeHoverCol = ImGui::ColorConvertFloat4ToU32(ImVec4(0.75f, 0.75f, 0.75f, 1.0f)); // \TODO: style.Colors
		ImU32 outOfRangeCol = ImGui::ColorConvertFloat4ToU32(ImVec4(0.4f, 0.4f, 0.4f, 1.0f)); // \TODO: style.Colors
		ImU32 outOfRangeHoverCol = ImGui::ColorConvertFloat4ToU32(ImVec4(0.45f, 0.45f, 0.45f, 1.0f)); // \TODO: style.Colors
		ImU32 secondsBarTopCol = ImGui::ColorConvertFloat4ToU32(ImVec4(0.3f, 0.3f, 0.3f, 1.0f)); // \TODO: style.Colors
		ImU32 secondsBarBottomCol = ImGui::ColorConvertFloat4ToU32(ImVec4(0.4f, 0.4f, 0.4f, 1.0f)); // \TODO: style.Colors
		ImU32 secondsMarkerTextCol = ImGui::ColorConvertFloat4ToU32(ImVec4(0.2f, 0.2f, 0.2f, 1.0f)); // \TODO: style.Colors
		ImU32 timeNowSecondsMarkerCol = ImGui::ColorConvertFloat4ToU32(ImVec4(0.7f, 0.7f, 0.7f, 1.0f)); // \TODO: style.Colors
		ImU32 timeNowSecondsMarkerShadowCol = ImGui::ColorConvertFloat4ToU32(ImVec4(0.3f, 0.3f, 0.3f, 1.0f)); // \TODO: style.Colors

		// Active logic?
		// ImGui::IsMouseDown(0)
		// ImGui::IsMouseDragging(0)
		// ImGui::IsItemHovered()
		// ImGui::IsItemClicked(0)

		// Render active timeline area
		ImU32 activeAreaColor = inRangeCol;
		if (ImGui::IsMouseHoveringRect(topBarSizeRect.Min, topBarSizeRect.Max)) {
			activeAreaColor = inRangeHoverCol;
		}

		dl->AddRectFilled(fullSizeRect.Min, fullSizeRect.Max, activeAreaColor, rounding, 0); // ImGuiCorner_TopLeft | ImGuiCorner_TopRight

		// Render rest of timeline area (not containing any active demoparts)
		if (fullSizeRect.Max.x < topBarSize.x) {
			ImU32 inactiveAreaColor = outOfRangeCol;
			if (ImGui::IsMouseHoveringRect(topBarSizeRect.Min, topBarSizeRect.Max)) {
				inactiveAreaColor = outOfRangeHoverCol;
			}

			dl->AddRectFilled(ImVec2(fullSizeRect.Max.x, fullSizeRect.Min.y), topBarSizeRect.Max, inactiveAreaColor, rounding, 0); // ImGuiCorner_TopLeft | ImGuiCorner_TopRight
		}

		// Render timeline markers
		{
			const float ratio = 5.0f;
			int secondsPerLine = 1;

			if ((*viewWidthSeconds) >= ratio * 60.0f * 60.0f) {
				secondsPerLine = 60 * 60;
			}
			else if ((*viewWidthSeconds) >= ratio * 60.0f * 30.0f) {
				secondsPerLine = 60 * 30;
			}
			else if ((*viewWidthSeconds) >= ratio * 60.0f * 15.0f) {
				secondsPerLine = 60 * 15;
			}
			else if ((*viewWidthSeconds) >= ratio * 60.0f * 10.0f) {
				secondsPerLine = 60 * 10;
			}
			else if ((*viewWidthSeconds) >= ratio * 60.0f * 5.0f) {
				secondsPerLine = 60 * 5;
			}
			else if ((*viewWidthSeconds) >= ratio * 60.0f * 2.0f) {
				secondsPerLine = 60 * 2;
			}
			else if ((*viewWidthSeconds) >= ratio * 60.0f) {
				secondsPerLine = 60;
			}
			else if ((*viewWidthSeconds) >= ratio * 30.0f) {
				secondsPerLine = 30;
			}
			else if ((*viewWidthSeconds) >= ratio * 15.0f) {
				secondsPerLine = 15;
			}
			else if ((*viewWidthSeconds) >= ratio * 10.0f) {
				secondsPerLine = 10;
			}
			else if ((*viewWidthSeconds) >= ratio * 5.0f) {
				secondsPerLine = 5;
			}
			else if ((*viewWidthSeconds) >= ratio * 2.0f) {
				secondsPerLine = 2;
			}

			float stepping = secondsPerLine * widthOfASecond;
			int markerCount = static_cast<int>((*viewWidthSeconds) / secondsPerLine);
			int secondsStart = static_cast<int>((*viewScrollPosSeconds) / static_cast<float>(secondsPerLine));
			float rel = (*viewScrollPosSeconds) / static_cast<float>(secondsPerLine);
			float offset = -stepping * (rel - secondsStart);

			for (int i=-1; i<=markerCount; i++) {
				float x = dc.CursorPos.x + offset + i * stepping;
				float y = dc.CursorPos.y;
				dl->AddLine(ImVec2(x, y),
							ImVec2(x, y + topBarHeight),
							secondsBarTopCol, 1.0f);
				dl->AddLine(ImVec2(x, y + topBarHeight),
							ImVec2(x, y + topBarHeight + imGuiWindow->Size.y),
							secondsBarBottomCol, 1.0f);

				Verso::UString timeStr = Verso::Timestamp::seconds(static_cast<float>(secondsStart+i) * static_cast<float>(secondsPerLine)).asDigitalClockString(false);
				dl->AddText(ImVec2(x+1, y+2), secondsMarkerTextCol, timeStr.c_str());
			}
		}

		// Render some test elements
		{
			int elementId = 0;
			const float rowPadding = 0.15f * ImGui::GetFontSize();
			const float rowHeight = 2.0f * rowPadding + ImGui::GetFontSize();

			//void timelineElement(float startSeconds, float lengthSeconds, float viewScrollPosSeconds, float viewWidthSeconds,
			//                     float row, float rowHeight, float topBarHeight,
			//                     size_t id, const char* content)

			for (DemoPart* i : settings.demoParts) {
				const DemoPartSettings& dpSettings = i->getSettings();
				UString t(dpSettings.name);
//				t += "-p";
//				t.append2(dpSettings.priority);
//				t += "-s";
//				t.append2(dpSettings.subsequentPriority);
//				t += "-d";
//				t.append2(dpSettings.duplicateLevel);
				float row = static_cast<float>(dpSettings.subsequentPriority); // \TODO: needs some preprocessing to take out unused rows
				bool clicked = timelineElement(static_cast<float>(dpSettings.start), static_cast<float>(dpSettings.duration),
											   *viewScrollPosSeconds, *viewWidthSeconds,
											   row, rowHeight, rowHeight,
											   topBarHeight,
											   elementId++, t.c_str()); //dpSettings.name.c_str()
				if (clicked == true) {
					(*selected) = i;
				}
			}

		/*
			float row = 0;
			timelineElement(0.0f, 1.5f, *viewScrollPosSeconds, *viewWidthSeconds, row++, rowHeight, topBarHeight, elementId++, "pleasing");

			timelineElement(1.0f, 3.0f, *viewScrollPosSeconds, *viewWidthSeconds, row, rowHeight, topBarHeight, elementId++, "agreeable");
			timelineElement(10.0f, 5.0f, *viewScrollPosSeconds, *viewWidthSeconds, row++, rowHeight, topBarHeight, elementId++, "very agreeable");

			timelineElement(4.5f, 25.0f, *viewScrollPosSeconds, *viewWidthSeconds, row++, rowHeight, topBarHeight, elementId++, "delightful");

			timelineElement(10.0f, 5.0f, *viewScrollPosSeconds, *viewWidthSeconds, row, rowHeight, topBarHeight, elementId++, "amiably pleasant");
			timelineElement(60.0f, 5.0f, *viewScrollPosSeconds, *viewWidthSeconds, row++, rowHeight, topBarHeight, elementId++, "aligned");

			timelineElement(15.0f, 3.0f, *viewScrollPosSeconds, *viewWidthSeconds, row++, rowHeight, topBarHeight, elementId++, "alluring");
		*/
		}

		// Render position cursor
		{
			float x = topBarSizeRect.Min.x + ((*timeNowSeconds) - (*viewScrollPosSeconds)) * widthOfASecond;
			float y = topBarSizeRect.Min.y;

			dl->AddLine(ImVec2(x-1, y),
						ImVec2(x-1, y + imGuiWindow->Size.y),
						timeNowSecondsMarkerShadowCol, 1.0f);
			dl->AddLine(ImVec2(x, y),
						ImVec2(x, y + imGuiWindow->Size.y),
						timeNowSecondsMarkerCol, 1.0f);
			dl->AddLine(ImVec2(x+1, y),
						ImVec2(x+1, y + imGuiWindow->Size.y),
						timeNowSecondsMarkerShadowCol, 1.0f);
		}
	}

	// Advance next item cursor
	ImGui::ItemSize(topBarSizeRect, style.FramePadding.y);

	// Handle input
	const ImGuiIO& io = ImGui::GetIO();
	if (ImGui::IsMouseHoveringRect(timelineSizeRect.Min, timelineSizeRect.Max) && io.MouseWheel != 0.0f) {
		// Ctrl + Mouse wheel: scale timeline
		if (io.KeysDown[static_cast<uint32_t>(Verso::KeyScancode::LCtrl)] == true ||
				io.KeysDown[static_cast<uint32_t>(Verso::KeyScancode::RCtrl)] == true)
		{
			float scalingSpeed = 1.25f;
			float oldViewWidthSeconds = *viewWidthSeconds;

			if (io.MouseWheel > 0.0f) {
				*viewWidthSeconds /= scalingSpeed;
			}
			else if (io.MouseWheel < 0.0f) {
				*viewWidthSeconds *= scalingSpeed;
			}

			if (*viewWidthSeconds < 1.0f) {
				*viewWidthSeconds = 1.0f;
			}
			else if (*viewWidthSeconds > 60.0f * 60.0f * 10.0f) {
				*viewWidthSeconds = 60.0f * 60.0f * 10.0f;
			}

			float ratio = (io.MousePos.x - timelineSizeRect.Min.x) / (timelineSizeRect.Max.x - timelineSizeRect.Min.x);
			*viewScrollPosSeconds += (oldViewWidthSeconds - (*viewWidthSeconds)) * ratio;
			if (*viewScrollPosSeconds < 0.0f) {
				*viewScrollPosSeconds = 0.0f;
			}
		}

		// Mouse wheel: scroll timeline
		else {
			float scrollSpeed = 0.10f;

			if (io.MouseWheel > 0.0f) {
				*viewScrollPosSeconds -= scrollSpeed * (*viewWidthSeconds);
			}
			else if (io.MouseWheel < 0.0f) {
				*viewScrollPosSeconds += scrollSpeed * (*viewWidthSeconds);
			}

			if (*viewScrollPosSeconds < 0.0f) {
				*viewScrollPosSeconds = 0.0f;
			}
		}
	}

	return changed;
}


bool demoTimeLine(DemoPlayerSettings& settings, float* timeNowSeconds, float* viewScrollPosSeconds, float* viewWidthSeconds,
				  DemoPart** selected)
{
	bool changed = false;

	const float topBarPadding = 0.15f * ImGui::GetFontSize();
	const float topBarHeight = 2.0f * topBarPadding + ImGui::GetFontSize();

	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0.0f, 0.0f));
	ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 3.0f);
	ImGui::BeginChild("scrolling", ImVec2(0, ImGui::GetContentRegionAvail().y), true, ImGuiWindowFlags_HorizontalScrollbar);
	{
		if (timeLineTimeMeasure(settings, timeNowSeconds, viewScrollPosSeconds, viewWidthSeconds, selected, topBarHeight)) {
			changed = true;
		}
	}
	ImGui::EndChild();
	ImGui::PopStyleVar(3);


	return changed;

	/*
	static int lines = 10;
	int n = 0;
	//if (n > 0) ImGui::SameLine();
	ImGui::PushID(n + line * 1000);
	char num_buf[16];
	const char* label = (!(n%15)) ? "FizzBuzz" : (!(n%3)) ? "Fizz" : (!(n%5)) ? "Buzz" : (snprintf(num_buf, 16, "%d", n), num_buf);
	float hue = n*0.05f;
	ImGui::PushStyleColor(ImGuiCol_Button, ImColor::HSV(hue, 0.6f, 0.6f).Value);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImColor::HSV(hue, 0.7f, 0.7f).Value);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImColor::HSV(hue, 0.8f, 0.8f).Value);
	ImGui::Button(label, ImVec2(40.0f + sinf(static_cast<float>(line + n)) * 20.0f, 0.0f));
	ImGui::PopStyleColor(3);
	ImGui::PopID();
	*/

	/*
	for (int line = 0; line < lines; line++)
	{
		// Display random stuff (for the sake of this trivial demo we are using basic Button+SameLine. If you want to create your own time line for a real application you may be better off
		// manipulating the cursor position yourself, aka using SetCursorPos/SetCursorScreenPos to position the widgets yourself. You may also want to use the lower-level ImDrawList API)
		int num_buttons = 10 + ((line & 1) ? line * 9 : line * 3);
		for (int n = 0; n < num_buttons; n++)
		{
			if (n > 0) ImGui::SameLine();
			ImGui::PushID(n + line * 1000);
			char num_buf[16];
			const char* label = (!(n%15)) ? "FizzBuzz" : (!(n%3)) ? "Fizz" : (!(n%5)) ? "Buzz" : (snprintf(num_buf, 16, "%d", n), num_buf);
			float hue = n*0.05f;
			ImGui::PushStyleColor(ImGuiCol_Button, ImColor::HSV(hue, 0.6f, 0.6f).Value);
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImColor::HSV(hue, 0.7f, 0.7f).Value);
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImColor::HSV(hue, 0.8f, 0.8f).Value);
			ImGui::Button(label, ImVec2(40.0f + sinf(static_cast<float>(line + n)) * 20.0f, 0.0f));
			ImGui::PopStyleColor(3);
			ImGui::PopID();
		}
	}*/

	/*
	float scroll_x = ImGui::GetScrollX(), scroll_max_x = ImGui::GetScrollMaxX();
	float scroll_x_delta = 0.0f;
	ImGui::SmallButton("<<"); if (ImGui::IsItemActive()) scroll_x_delta = -ImGui::GetIO().DeltaTime * 1000.0f; ImGui::SameLine();
	ImGui::Text("Scroll from code"); ImGui::SameLine();
	ImGui::SmallButton(">>"); if (ImGui::IsItemActive()) scroll_x_delta = +ImGui::GetIO().DeltaTime * 1000.0f; ImGui::SameLine();
	ImGui::Text("%.0f/%.0f", scroll_x, scroll_max_x);
	if (scroll_x_delta != 0.0f)
	{
		ImGui::BeginChild("scrolling"); // Demonstrate a trick: you can use Begin to set yourself in the context of another window (here we are already out of your child window)
		ImGui::SetScrollX(ImGui::GetScrollX() + scroll_x_delta);
		ImGui::End();
	}*/
}


} // End namespace Verso

