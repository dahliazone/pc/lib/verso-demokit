#include <Verso/System/JsonHelperDemoKit.hpp>
#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui.h>
#include <imgui_internal.h>

namespace Verso {

namespace JsonHelper {


PlayMode readPlayMode(const JsonPath& parent, const UString& name, bool required, const PlayMode& defaultValue)
{
	UString playModeStr = parent.readString(name, required, "");
	if (!playModeStr.isEmpty()) {
		PlayMode playMode = stringToPlayMode(playModeStr);
		if (playMode == PlayMode::Unset) {
			UString error("Unknown value (" + playModeStr + ") in field \"" + parent.currentPath + "." + name + "\"! Must be one of: \"Production\", \"Development\".");
			VERSO_ILLEGALFORMAT("verso-demokit", error.c_str(), parent.fileName.c_str());
		}
		return playMode;
	}
	else if (required == true) {
		UString error("Cannot read required field \"" + parent.currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-demokit", error.c_str(), parent.fileName.c_str());
	}

	return defaultValue;
}


DebugMode readDebugMode(const JsonPath& parent, const UString& name, bool required, const DebugMode& defaultValue)
{
	UString debugModeStr = parent.readString(name, required, "");
	if (!debugModeStr.isEmpty()) {
		DebugMode debugMode = stringToDebugMode(debugModeStr);
		if (debugMode == DebugMode::Unset) {
			UString error("Unknown value (" + debugModeStr + ") in field \"" + parent.currentPath + "." + name + "\"! Must be one of: \"Editor\", \"TimeControls\", \"Fullscreen\".");
			VERSO_ILLEGALFORMAT("verso-demokit", error.c_str(), parent.fileName.c_str());
		}
		return debugMode;
	}
	else if (required == true) {
		UString error("Cannot read required field \"" + parent.currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-demokit", error.c_str(), parent.fileName.c_str());
	}

	return defaultValue;
}


} // End namespace JsonHelper


} // End namespace Verso

