#include <Verso/DemoKit/DemoKitDemo.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/Audio/Audio2dBass.hpp>
#include <Verso/Render/Camera/CameraFps.hpp>
#include <Verso/System/assert.hpp>

namespace Verso {


DemoKitDemo::DemoKitDemo(const IDemoPartFactory* demoPartFactory, const UString& sourceJsonFileName) :
	created(false),
	quitting(false),
	demoPartFactory(demoPartFactory),
	sourceJsonFileName(sourceJsonFileName),
	window(nullptr),
	demoPlayer(),
	demoKitUi(nullptr),
	audioDevice(0, AudioDeviceType::Unset, ""),
	muteAudio(false),

	camera(nullptr),
//	testTexture(),

	imageRect(),

	fbo(nullptr)
//	iChannel0(nullptr),
//	iDepth0(nullptr),
//	iChannel1(nullptr),
//	iChannel2(nullptr),
{
}


DemoKitDemo::DemoKitDemo(DemoKitDemo&& original) :
	created(std::move(original.created)),
	quitting(std::move(original.quitting)),
	demoPartFactory(std::move(original.demoPartFactory)),
	sourceJsonFileName(std::move(original.sourceJsonFileName)),
	window(std::move(original.window)),
	demoPlayer(std::move(original.demoPlayer)),
	demoKitUi(std::move(original.demoKitUi)),
	audioDevice(std::move(original.audioDevice)),
	muteAudio(std::move(original.muteAudio)),

	camera(std::move(original.camera)),
//	testTexture(std::move(original.testTexture)),

	imageRect(std::move(original.imageRect)),

	fbo(std::move(original.fbo))
//	iChannel0(std::move(original.iChannel0)),
//	iDepth0(std::move(original.iDepth0)),
//	iChannel1(std::move(original.iChannel1)),
//	iChannel2(std::move(original.iChannel2)),
{
}


DemoKitDemo& DemoKitDemo::operator =(DemoKitDemo&& original)
{
	if (this != &original) {
		created = std::move(original.created);
		quitting = std::move(original.quitting);
		demoPartFactory = std::move(original.demoPartFactory);
		sourceJsonFileName = std::move(original.sourceJsonFileName);
		window = std::move(original.window);
		demoPlayer = std::move(original.demoPlayer);
		demoKitUi = std::move(original.demoKitUi);
		audioDevice = std::move(original.audioDevice);
		muteAudio = std::move(original.muteAudio);

		camera = std::move(original.camera);
//		testTexture = std::move(original.testTexture);

		imageRect = std::move(original.imageRect);

		fbo = std::move(original.fbo);
//		iChannel0 = std::move(original.iChannel0);
//		iDepth0 = std::move(original.iDepth0);
//		iChannel1 = std::move(original.iChannel1);
//		iChannel2 = std::move(original.iChannel2);

		// clear original
		original.created = false;
		original.demoPartFactory = nullptr;
		original.window = nullptr;
		original.demoKitUi = nullptr;
		original.camera = nullptr;
//		original.iChannel0 = nullptr;
//		original.iDepth0 = nullptr;
//		original.iChannel1 = nullptr;
//		original.iChannel2 = nullptr;
	}
	return *this;
}


DemoKitDemo::~DemoKitDemo()
{
	DemoKitDemo::destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////

void DemoKitDemo::create(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");
	VERSO_LOG_INFO("verso-demokit", "Demo create ==================================================");
	this->window = &window;

	// Load DemoKit JSON
	demoPlayer.createFromFile(demoPartFactory, sourceJsonFileName);
	VERSO_LOG_INFO("verso-demokit", demoPlayer.toStringDebug());

	// Init Render
	// \TODO: render is not actually based on Window Opengl intance as it should
	Render::create(window,
				   demoPlayer.settings.demoPaths.pathShaders(),
				   demoPlayer.settings.demoPaths.pathShadersVerso());

	window.getResourceManager().setupDefaultTexture(demoPlayer.settings.demoPaths.pathTextures() + "default.png");

	// Setup audio
	Audio2dBass& audio2d = Audio2dBass::instance();
	audio2d.create(muteAudio, audioDevice.deviceId);

	// Create DemoParts
	demoPlayer.createDemoParts(window, audio2d);

	demoKitUi = new DemoKitUi(*this, demoPlayer);
	demoKitUi->create(window);

	camera = new CameraTarget();
	camera->create(&window, "verso-demokit/DemoKitDemo/camera", true);

//	testTexture.createFromFile(demoPlayer.settings.demoPaths.pathTextures()+"backgrounds/test1.jpg",
//							   TextureParameters());

	VERSO_LOG_DEBUG_VARIABLE("verso-demokit", "window->getRenderResolutioni()", window.getRenderResolutioni().toString());

	fbo = window.getResourceManager().createFbo(&window, "Verso-DemoKit/DemoKitDemo/fbo");
	fbo->createColorTexture(TextureParameters(
								"iChannel0",
								TexturePixelFormat::Rgb,
								MinFilter::Nearest, MagFilter::Nearest,
								WrapStyle::ClampToEdge, WrapStyle::ClampToEdge));
	fbo->createDepthTexture(TextureParameters(
								"iDepth",
								TexturePixelFormat::DepthComponent,
								MinFilter::Nearest, MagFilter::Nearest,
								WrapStyle::ClampToEdge, WrapStyle::ClampToEdge));
	if (fbo->isComplete() == false) {
		VERSO_ERROR("verso-demokit", "FBO is incomplete", "");
	}

	if (demoPlayer.settings.debug.playMode == PlayMode::Development) {
		window.setCursorShown(true);
	}
	else {
		window.setCursorShown(false);
	}

	created = true;

	reset(window);

	VERSO_LOG_INFO("verso-demokit", "Demo create DONE =============================================");
}


void DemoKitDemo::reset(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == true, "IDemoKitApp must be created before calling reset()");

	camera->reset(Vector3f(0.0f, 0.0f, 1.0f), Vector3f::up());
	camera->setTarget(Vector3f(0.0f, 0.0f, 0.0f));
	camera->setPosition(Vector3f(0.0f, 0.0f, -3.0f));
	camera->setProjectionOrthographic(Rangef(0.001f, 10.0f));

	demoPlayer.startDemo(window);
}


void DemoKitDemo::destroy() VERSO_NOEXCEPT
{
	if (DemoKitDemo::isCreated() == false) {
		return;
	}

	demoPlayer.stopDemo();

	VERSO_LOG_INFO("verso-demokit", "Demo destroy =================================================");

	//	testTexture.destroy();

	if (camera != nullptr) {
		camera->destroy();
		delete camera;
		camera = nullptr;
	}

	delete demoKitUi;
	demoKitUi = nullptr;

	Render::destroy();

	demoPlayer.destroy();

	Audio2dBass::instance().destroy();

	this->window = nullptr;
	created = false;
	VERSO_LOG_INFO("verso-demokit", "Demo destroy DONE ============================================");
}


bool DemoKitDemo::isCreated() const
{
	return created;
}


void DemoKitDemo::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	demoPlayer.handleInput(window, time);
	demoKitUi->handleInput(window, time);

	if (demoPlayer.isDemoOver() == true) {
		quit();
	}
}


void DemoKitDemo::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	if (event.type == EventType::Resized) {
		// \TODO: some old attempt of resizing
//		window.onResize(event.size);
//		destroyRenderFbo();
//		createRenderFbo();
	}

	demoPlayer.handleEvent(window, time, event);
	demoKitUi->handleEvent(window, time, event);
}


void DemoKitDemo::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	Audio2dBass::instance().update();

	fbo->bind();
	demoPlayer.render(window, time);
	fbo->unbind();

	window.resetRelativeViewport();
	window.applyDrawableViewport();

	if (demoKitUi->getEnableUi() == false) {
		Render::clearScreen(ClearFlag::SolidColor, demoPlayer.getDemoPlayerSettings().general.borderColor);
		//Opengl::blendTranscluent();
		Render::draw2dDrawableResolution(
					window, *camera, *fbo->getColorDrawTexture(), Vector3f(0.5f, 0.5f),
					Align(HAlign::Center, VAlign::Center),
					Vector2f(1.0f, 1.0f),
					RefScale::ViewportSize_KeepAspectRatio_FitRect, 1.0f, 0.0f, true, true);
	}
	else {
		demoKitUi->updateRenderTexture(fbo->getColorReadTexture());
		demoKitUi->render(window, time);
	}
}


bool DemoKitDemo::isQuitting() const
{
	return quitting;
}


void DemoKitDemo::quit()
{
	quitting = true;
}


///////////////////////////////////////////////////////////////////////////////////////////
// IDemoKitApp interface
///////////////////////////////////////////////////////////////////////////////////////////

void DemoKitDemo::preCreate(const AudioDevice& audioDevice, bool muteAudio)
{
	this->audioDevice = audioDevice;
	this->muteAudio = muteAudio;
}


UString DemoKitDemo::getSourceJsonFileName() const
{
	return sourceJsonFileName;
}


const DemoPlayerSettings& DemoKitDemo::getDemoPlayerSettings() const
{
	return demoPlayer.getDemoPlayerSettings();
}


bool DemoKitDemo::saveRenderFboToFile(const UString& fileName, const ImageSaveFormat& imageSaveFormat) const
{
	Texture* tex = fbo->getColorReadTexture(0);
	if (tex != nullptr) {
		tex->bind();
		bool result = tex->saveToFile(fileName, imageSaveFormat);
		tex->unbind();
		return result;
	}
	else {
		return false;
	}
}


} // End namespace Verso

