#include <Verso/DemoKit/DemoKitSetupDialog.hpp>
#include <Verso/DemoKit/DemoPlayer.hpp>
#include <Verso/Display/WindowOpengl.hpp>
#include <Verso/Gui/ImGuiVersoImpl.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/System/File.hpp>
#include <Verso/System/CommandLineParser.hpp>
#include <Verso/System/CommandLineParser/HelpFormatter.hpp>
#include <Verso/Time/FrameTimer.hpp>
#include <Verso/Audio/Audio2dBass.hpp>
#include <imgui.h>

namespace Verso {


DemoKitSetupDialog::DemoKitSetupDialog(const IDemoPartFactory* demoPartFactory, const UString& defaultSourceFileName, const UString& binaryName) :
	// public
	showDialog(true),
	sourceFileName(),
	demoPlayerSettings(),
	display(nullptr),
	displayMode(nullptr),
	downscaleRatio(nullptr),
//	antialiasLevel(nullptr),
	audioDevice(nullptr),

	fullscreen(true),
	customResolution(false),
//	loop(),
	vsync(),
	unrestrictedFps(),
	muteAudio(),
	record(false),
	recordResolution(),
	recordSaveFormat(ImageSaveFormat::Tga),

	// private
	imgui(),
	smallerFont(nullptr),
	biggerFont(nullptr),
	demoPartFactory(demoPartFactory),
	defaultSourceFileName(defaultSourceFileName),
	binaryName(binaryName),
	logoTexture(nullptr),
	logoDrawWidth(0.0f),
	logoDrawHeight(0.0f),
	windowWidth(400),
	windowHeight(0),
	logoPaddingHorizontal(50),

	displays(),
	displayModesFullscreen(),
	displayModesWindowed(),
	refreshRates(),
	downscaleRatios(),
//	antialiasLevels(),
	audioDevices(),
	extraDisplayModes(false),
	esotericDisplayModes(false),
	refreshRateSelector(nullptr)
{
}


DemoKitSetupDialog::DemoKitSetupDialog(DemoKitSetupDialog&& original) :
	// public
	showDialog(std::move(original.showDialog)),
	sourceFileName(std::move(original.sourceFileName)),
	demoPlayerSettings(std::move(original.demoPlayerSettings)),
	display(std::move(original.display)),
	displayMode(std::move(original.displayMode)),
	downscaleRatio(std::move(original.downscaleRatio)),
//	antialiasLevel(std::move(original.antialiasLevel)),
	audioDevice(std::move(original.audioDevice)),

	fullscreen(std::move(original.fullscreen)),
	customResolution(std::move(original.customResolution)),
//	loop(std::move(original.loop)),
	vsync(std::move(original.vsync)),
	unrestrictedFps(std::move(original.unrestrictedFps)),
	muteAudio(std::move(original.muteAudio)),
	record(std::move(original.record)),
	recordResolution(std::move(original.recordResolution)),
	recordSaveFormat(std::move(original.recordSaveFormat)),

	// private
	imgui(std::move(original.imgui)),
	smallerFont(std::move(original.smallerFont)),
	biggerFont(std::move(original.biggerFont)),
	demoPartFactory(std::move(original.demoPartFactory)),
	defaultSourceFileName(std::move(original.defaultSourceFileName)),
	binaryName(std::move(original.binaryName)),
	logoTexture(std::move(original.logoTexture)),
	logoDrawWidth(std::move(original.logoDrawWidth)),
	logoDrawHeight(std::move(original.logoDrawHeight)),
	windowWidth(std::move(original.windowWidth)),
	windowHeight(std::move(original.windowHeight)),
	logoPaddingHorizontal(std::move(original.logoPaddingHorizontal)),

	displays(std::move(original.displays)),
	displayModesFullscreen(std::move(original.displayModesFullscreen)),
	displayModesWindowed(std::move(original.displayModesWindowed)),
	refreshRates(std::move(original.refreshRates)),
	downscaleRatios(std::move(original.downscaleRatios)),
//	antialiasLevels(std::move(original.antialiasLevels)),
	audioDevices(std::move(original.audioDevices)),
	extraDisplayModes(std::move(original.extraDisplayModes)),
	esotericDisplayModes(std::move(original.esotericDisplayModes)),
	refreshRateSelector(std::move(original.refreshRateSelector))
{
}


DemoKitSetupDialog& DemoKitSetupDialog::operator =(DemoKitSetupDialog&& original)
{
	if (this != &original) {
		// public
		showDialog = std::move(original.showDialog);
		sourceFileName = std::move(original.sourceFileName);
		demoPlayerSettings = std::move(original.demoPlayerSettings);
		display = std::move(original.display);
		displayMode = std::move(original.displayMode);
		downscaleRatio = std::move(original.downscaleRatio);
		//antialiasLevel = std::move(original.antialiasLevel);
		audioDevice = std::move(original.audioDevice);

		fullscreen = std::move(original.fullscreen);
		customResolution = std::move(original.customResolution);
		//loop = std::move(original.loop);
		vsync = std::move(original.vsync);
		unrestrictedFps = std::move(original.unrestrictedFps);
		muteAudio = std::move(original.muteAudio);
		record = std::move(original.record);
		recordResolution = std::move(original.recordResolution);
		recordSaveFormat = std::move(original.recordSaveFormat);

		// private
		imgui = std::move(original.imgui);
		smallerFont = std::move(original.smallerFont);
		biggerFont = std::move(original.biggerFont);
		demoPartFactory = std::move(original.demoPartFactory);
		defaultSourceFileName = std::move(original.defaultSourceFileName);
		binaryName = std::move(original.binaryName);
		logoTexture = std::move(original.logoTexture);
		logoDrawWidth = std::move(original.logoDrawWidth);
		logoDrawHeight = std::move(original.logoDrawHeight);
		windowWidth = std::move(original.windowWidth);
		windowHeight = std::move(original.windowHeight);
		logoPaddingHorizontal = std::move(original.logoPaddingHorizontal);

		displays = std::move(original.displays);
		displayModesFullscreen = std::move(original.displayModesFullscreen);
		displayModesWindowed = std::move(original.displayModesWindowed);
		refreshRates = std::move(original.refreshRates);
		downscaleRatios = std::move(original.downscaleRatios);
		//antialiasLevels = std::move(original.antialiasLevels);
		audioDevices = std::move(original.audioDevices);
		extraDisplayModes = std::move(original.extraDisplayModes);
		esotericDisplayModes = std::move(original.esotericDisplayModes);
		refreshRateSelector = std::move(original.refreshRateSelector);

		// clear original
		// - public
		original.display = nullptr;
		original.displayMode = nullptr;
		original.downscaleRatio = nullptr;
		//original.antialiasLevel = nullptr;
		original.audioDevice = nullptr;
		// - private
		original.smallerFont = nullptr;
		original.biggerFont = nullptr;
		original.logoTexture = nullptr;
		original.refreshRateSelector = nullptr;
	}
	return *this;
}


DemoKitSetupDialog::~DemoKitSetupDialog()
{
	imgui.destroy();
}


bool DemoKitSetupDialog::run(int argc, char* argv[])
{
	std::vector<UString> args(Args::convert(argc, argv));

	init();

	bool returnValue = processArgs(args);
	if (returnValue == false) {
		return false;
	}

	// Load demo settings needed for setting up setup dialog & demo window
	{
		DemoPlayer demoPlayer;
		demoPlayer.createFromFile(demoPartFactory, sourceFileName);
		demoPlayerSettings = std::move(demoPlayer.settings);
	}

	VERSO_LOG_INFO("verso-demokit", "Selected options (command-line):");
	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Input", sourceFileName);
	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Display mode", displayMode->toString());
	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Display", display->name);
	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Downscale ratio", downscaleRatio->toString());
//	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Antialias level", antialiasLevel->toString());
	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Audio", audioDevice->toString());
	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Fullscreen", fullscreen);
#ifdef _WIN32
	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Custom resolution", customResolution);
#endif
//	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Loop", loop);
	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Vsync", vsync);
	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Unrestricted FPS", unrestrictedFps);
	VERSO_LOG_INFO_VARIABLE("verso-demokit", "Mute audio", muteAudio);
	if (record == true) {
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Record demo", record);
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Record resolution", recordResolution);
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Record format", recordSaveFormat);
	}

	if (showDialog == true) {
		returnValue = runDialog();

		VERSO_ASSERT_MSG("verso-demokit", display!=nullptr, "Display was not chosen for some reason!");
		VERSO_ASSERT_MSG("verso-demokit", displayMode!=nullptr, "DisplayMode was not chosen for some reason!");
		VERSO_ASSERT_MSG("verso-demokit", downscaleRatio!=nullptr, "Downscale ratio was not chosen for some reason!");
//		VERSO_ASSERT_MSG("verso-demokit", antialiasLevel!=nullptr, "Antialias was not chosen for some reason!");

		VERSO_LOG_INFO("verso-demokit", "Selected options:");
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Input", sourceFileName);
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Display mode", displayMode->toString());
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Display", display->name);
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Downscale ratio", downscaleRatio->toString());
//		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Antialias level", antialiasLevel->toString());
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Audio", audioDevice->toString());
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Fullscreen", fullscreen);
#ifdef _WIN32
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Custom resolution", customResolution);
#endif
//		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Loop", loop);
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Vsync", vsync);
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Unrestricted FPS", unrestrictedFps);
		VERSO_LOG_INFO_VARIABLE("verso-demokit", "Mute audio", muteAudio);
	}

	return returnValue;
}


void DemoKitSetupDialog::init()
{
	displays = Display::getDisplays();
	VERSO_ASSERT_MSG("verso-demokit", displays.size() > 0, "Could not find any displays!");

	downscaleRatios = RenderDownscaleRatio::getRenderDownscaleRatios();
//	antialiasLevels = MultisampleAntialiasLevel::getAntialiasLevels();
}


bool DemoKitSetupDialog::findDisplayMode(const DisplayMode& selected, bool dieOnError)
{
	if (fullscreen == true) {
		displayMode = DisplayMode::findIdenticalFromList(selected, displayModesFullscreen);
		if (displayMode == nullptr) {
			if (dieOnError == true) {
				UString err(selected.toString());
				err += "\ndisplayModesFullscreen:\n";
				for (auto& dm : displayModesFullscreen) {
					err += "  " + dm.toString() + "\n";
				}
				VERSO_ERROR("verso-demokit", "Internal error: cannot find fullscreen display mode that should be found. Tried to find desktop mode:", err.c_str());
			}
			else {
				return false;
			}
		}
	}
	else {
		displayMode = DisplayMode::findIdenticalFromList(selected, displayModesWindowed);
		if (displayMode == nullptr) {
			if (dieOnError == true) {
				UString err(selected.toString());
				err += "\ndisplayModesWindowed:\n";
				for (auto& dm : displayModesWindowed) {
					err += "  " + dm.toString() + "\n";
				}
				VERSO_ERROR("verso-demokit", "Internal error: cannot find windowed display mode that should be found. Tried to find desktop mode:", err.c_str());
			}
			else {
				return false;
			}
		}
	}
	return true;
}


void DemoKitSetupDialog::findRefreshRate(const RefreshRate& selected)
{
	refreshRateSelector = RefreshRate::findIdenticalFromList(selected, refreshRates);
}


bool DemoKitSetupDialog::processArgs(const std::vector<UString>& args)
{
	Options options;

	// Input file name
	Option optionInput("i", "input", "Input file");
	optionInput.setArgNames("FILE");
	optionInput.setArgDefaultValues(defaultSourceFileName);
	options.addOption(optionInput);

	// Display
	Option optionDisplay("d", "display", "Display index or name, e.g. 0 or \"LG\"");
	optionDisplay.setArgNames("DISPLAY");
	UString tmpDefaultDisplayIndex;
	tmpDefaultDisplayIndex.append2(InputManager::instance().getMouseActiveDisplayIndex(displays));
	optionDisplay.setArgDefaultValues(tmpDefaultDisplayIndex);
	options.addOption(optionDisplay);

	// List displays
	Option optionListDisplays("D", "displays", "List displays (and exit)");
	options.addOption(optionListDisplays);

	// Downscale ratio
	UString descriptionDownscaleRatio("Resolution downscale ratio: ");
	bool first = true;
	for (auto& it : downscaleRatios) {
		if (first == true) {
			first = false;
		}
		else {
			descriptionDownscaleRatio.append2(", ");
		}
		descriptionDownscaleRatio += it.toString();
	}
	Option optionDownscaleRatio("s", "downscale", descriptionDownscaleRatio);
	optionDownscaleRatio.setArgNames("RATIO");
	optionDownscaleRatio.setArgDefaultValues("1:1");
	options.addOption(optionDownscaleRatio);

	// Fullscreen
	Option optionFullscreenDesktopResolution("f", "fullscreen-desktop", "Fullscreen with desktop resolution (default)");
	options.addOption(optionFullscreenDesktopResolution);

#ifdef _WIN32
	// Fullscreen with resolution change
	Option optionFullscreenCustomResolution("F", "fullscreen-custom", "Fullscreen by changing to custom resolution (Windows only)");
	options.addOption(optionFullscreenCustomResolution);
#endif

	// Window
	Option optionWindow("w", "window", "Windowed mode");
	options.addOption(optionWindow);

	// Display mode
	Option optionDisplayMode("m", "displaymode", "Display mode, e.g. 0 or \"1920x1080 @ 60hz\" (not applied for --fullscreen-desktop)");
	optionDisplayMode.setArgNames("MODE");
	options.addOption(optionDisplayMode);

	// List display modes
	Option optionListDisplayModes("M", "displaymodes", "List display modes for selected display & windowed/fullscreen option (and exit)");
	options.addOption(optionListDisplayModes);

	// Anti-alias level
//	UString descriptionAntialias("Anti-alias level: ");
//	first = true;
//	for (auto& it : antialiasLevels) {
//		if (first == true) {
//			first = false;
//		}
//		else {
//			descriptionAntialias.appeewqeqweqwnd2(", ");
//		}
//		descriptionAntialias += it.toString();
//	}
//	Option optionAntialias("n", "antialias", descriptionAntialias);
//	optionAntialias.setArgNames("LEVEL");
//	optionAntialias.setArgDefaultValues("0x");
//	options.addOption(optionAntialias);

	// Audio device
#ifdef __linux__
	Option optionAudioDevice("a", "audio", "Audio device, e.g. 0 or \"Built-in output\" (default: PulseAudio or system default)");
#else
	Option optionAudioDevice("a", "audio", "Audio device, e.g. 0 or \"Built-in output\" (default: system default)");
#endif
	optionAudioDevice.setArgNames("DEVICE");
	options.addOption(optionAudioDevice);

	// List audio devices
	Option optionListAudioDevices("A", "listaudio", "List audio devices (and exit)");
	options.addOption(optionListAudioDevices);

	// Mute audio
	Option optionMuteAudio("U", "muteaudio", "Mute audio");
	options.addOption(optionMuteAudio);

	// Loop
//	Option optionLoop("o", "loop", "Loop playing");
//	options.addOption(optionLoop);

	// No VSync
	Option optionNoVsync("V", "novsync", "Disable vsync");
	options.addOption(optionNoVsync);

	// Unrestricted FPS
	Option optionUnrestrictedFps("R", "unrestrictedfps", "Unrestricted FPS");
	options.addOption(optionUnrestrictedFps);

	// Skip setup dialog
	Option optionSkipDialog("k", "skipdialog", "No setup dialog (use defaults & given options)");
	options.addOption(optionSkipDialog);

	// Record
	Option optionRecord("e", "record", "Record demo to images (also mute audio, disable vsync, skip dialog)");
	optionRecord.setArgNames("RESW", "RESH");
	options.addOption(optionRecord);

	// Image record format
	Option optionImageRecordFormat("E", "recordformat", "Record image format: \"Png\", \"Bmp\", \"Tga\" (default: \"Png\")");
	optionImageRecordFormat.setArgNames("FORMAT");
	options.addOption(optionImageRecordFormat);

	// Help
	Option optionHelp("h", "help", "This help (and exit)");
	options.addOption(optionHelp);


	CommandLineParser parser;
	HelpFormatter helpFormatter;
	try {
		parser.parse(options, args);
	}
	catch (IllegalFormatException& e) {
		helpFormatter.printHelp(binaryName, options);
		std::cout << "Error: " << e.getMessage() << std::endl;
		return false;
	}


	// Input file name
	if (parser.hasOption(optionInput)) {
		sourceFileName = parser.getOptionValues(optionInput)[0];
	}


	// Display
	if (parser.hasOption(optionDisplay)) {
		UString tmpArgument = parser.getOptionValues(optionDisplay)[0];

		// Find display by index
		try {
			int tmpIndex = tmpArgument.toValue<int>();
			if (tmpIndex < 0 || tmpIndex > static_cast<int>(displays.size())) {
				VERSO_ILLEGALFORMAT("verso-demokit", "Display index out of range", tmpArgument.c_str());
			}
			display = &displays[static_cast<size_t>(tmpIndex)];
		}

		// Find display by it's name
		catch (IllegalFormatException&) {
			std::vector<Display> matching = Display::getDisplay(tmpArgument);
			if (matching.size() > 0) {
				display = &displays[static_cast<size_t>(matching[0].index)]; // Choose first matching
			}
			else {
				helpFormatter.printHelp(binaryName, options);
				std::cout << "Error: Invalid argument for --"<<optionDisplay.longOpt<<": " << tmpArgument << std::endl;
				return false;
			}
		}
	}
	else {
		// Use default from parser
		size_t displayIndex = parser.getOptionValues(optionDisplay)[0].toValue<size_t>();
		display = &displays[displayIndex];
	}


	// Window / Fullscreen
	if (parser.hasOption(optionWindow)) {
		fullscreen = false;
		customResolution = false;
	}
#ifdef _WIN32
	else if (parser.hasOption(optionFullscreenCustomResolution)) {
		fullscreen = true;
		customResolution = true;
	}
#endif
	else if (parser.hasOption(optionFullscreenDesktopResolution)) {
		fullscreen = true;
		customResolution = false;
	}
	// default
	else {
		fullscreen = true;
		customResolution = false;
	}


	// Now that display has been chosen, retrieve the display modes for it
	displayModesFullscreen = DisplayMode::getFullscreenModes(display->index);
	displayModesWindowed = DisplayMode::getWindowedModes(
				display->index,
				static_cast<DisplayModeRatings>(DisplayModeRating::Normal | DisplayModeRating::Extra));
	refreshRates = DisplayMode::getFullscreenRefreshRates(display->index);

	// Display modes (fullscreen with desktop resolution -> ignore display mode option)
	if (parser.hasOption(optionDisplayMode) && !(fullscreen == true && customResolution == false)) {
		UString tmpArgument = parser.getOptionValues(optionDisplayMode)[0];

		// Find DisplayMode by index
		try {
			int tmpIndex = tmpArgument.toValue<int>();
			if (fullscreen == true) {
				if (tmpIndex < 0 || tmpIndex > static_cast<int>(displayModesFullscreen.size())) {
					VERSO_ILLEGALFORMAT("verso-demokit", "index out of range", tmpArgument.c_str());
				}
				displayMode = &displayModesFullscreen[static_cast<size_t>(tmpIndex)];
			}
			else {
				if (tmpIndex < 0 || tmpIndex > static_cast<int>(displayModesWindowed.size())) {
					VERSO_ILLEGALFORMAT("verso-demokit", "index out of range", tmpArgument.c_str());
				}
				displayMode = &displayModesWindowed[static_cast<size_t>(tmpIndex)];
			}
		}

		// Find DisplayMode by it's name
		catch (IllegalFormatException&) {
			bool result;
			if (fullscreen == true) {
				result = findDisplayMode(DisplayMode::getFullscreenMode(display->index, tmpArgument), false);
			}
			else {
				result = findDisplayMode(DisplayMode::getWindowedMode(display->index, tmpArgument), false);
			}
			if (result == false) {
				helpFormatter.printHelp(binaryName, options);
				std::cout << "Error: Invalid argument for --"<<optionDisplayMode.longOpt<<": " << tmpArgument << std::endl;
				return false;
			}
		}
	}
	// Set default display mode
	else {
		// Select desktop mode if exists in the list (Windows + 125% scaling then it might not)
		if (findDisplayMode(DisplayMode::getDesktopMode(display->index), false) == false) {
			// If desktop mode is not found then default to first one in the list
			displayMode = &displayModesFullscreen[0];
		}
	}

	// Set refresh rate from selected mode
	findRefreshRate(displayMode->refreshRate);

	// Downscale ratio
	if (parser.hasOption(optionDownscaleRatio)) {
		RenderDownscaleRatio selected(parser.getOptionValues(optionDownscaleRatio)[0]);
		downscaleRatio = RenderDownscaleRatio::findIdenticalFromList(selected, downscaleRatios);
	}

	// Anti-alias
//	if (parser.hasOption(optionAntialias)) {
//		MultisampleAntialiasLevel selected(parser.getOptionValues(optionAntialias)[0]);
//		antialiasLevel = MultisampleAntialiasLevel::findIdenticalFromList(selected, antialiasLevels);
//	}

	// Populate audio devices
	audioDevices = Audio2dBass::instance().getDevices();

	// Audio device
	if (parser.hasOption(optionAudioDevice)) {
		UString tmpArgument = parser.getOptionValues(optionAudioDevice)[0];

		// Find AudioDevice by index
		try {
			int tmpIndex = tmpArgument.toValue<int>();
			if (tmpIndex < 0 || tmpIndex > static_cast<int>(audioDevices.size())) {
				VERSO_ILLEGALFORMAT("verso-demokit", "index out of range", tmpArgument.c_str());
			}
			audioDevice = &audioDevices[static_cast<size_t>(tmpIndex)];
		}

		// Find AudioDevice by it's name
		catch (IllegalFormatException&) {
			try {
				for (size_t i=0; i<audioDevices.size(); ++i) {
					if (audioDevices[i].name == tmpArgument) {
						audioDevice = &audioDevices[i];
						break;
					}
				}
			}
			catch (ObjectNotFoundException&) {
				helpFormatter.printHelp(binaryName, options);
				std::cout << "Error: Invalid argument for --"<<optionAudioDevice.longOpt<<": " << tmpArgument << std::endl;
				return false;
			}
		}
	}
	// Set default audio device
	else {
		for (size_t i=0; i<audioDevices.size(); ++i) {
			if (audioDevices[i].defaultDevice == true) {
				audioDevice = &audioDevices[i];
				break;
			}
		}
	}

	// Loop
//	if (parser.hasOption(optionLoop)) {
//		loop = true;
//	}
//	else {
//		loop = false;
//	}

	// VSync
	if (parser.hasOption(optionNoVsync)) {
		vsync = false;
	}
	else {
		vsync = true;
	}

	// Unrestricted FPS
	if (parser.hasOption(optionUnrestrictedFps)) {
		unrestrictedFps = true;
	}
	else {
		unrestrictedFps = false;
	}

	// Mute audio
	if (parser.hasOption(optionMuteAudio)) {
		muteAudio = true;
	}
	else {
		muteAudio = false;
	}

	// Skip dialog
	if (parser.hasOption(optionSkipDialog)) {
		showDialog = false;
	}
	else {
		showDialog = true;
	}

	// Record demo
	if (parser.hasOption(optionRecord)) {
		record = true;
		muteAudio = true;
		vsync = false;
		showDialog = false;
		recordResolution.x = parser.getOptionValues(optionRecord)[0].toValue<int>();
		recordResolution.y = parser.getOptionValues(optionRecord)[1].toValue<int>();
	}
	else {
		record = false;
	}

	// Record image format
	if (parser.hasOption(optionImageRecordFormat)) {
		UString tmpArgument(parser.getOptionValues(optionImageRecordFormat)[0]);
		recordSaveFormat = stringToImageSaveFormat(tmpArgument);
		if (recordSaveFormat == ImageSaveFormat::Unset) {
			helpFormatter.printHelp(binaryName, options);
			std::cout << "Error: Invalid argument for --"<<optionImageRecordFormat.longOpt<<": " << tmpArgument << std::endl;
			return false;
		}
	}

	// PARAMETERS THAT CAN EXIT
	bool shouldExit = false;

	// List displays
	if (parser.hasOption(optionListDisplays)) {
		VERSO_LOG_INFO("verso-demokit", "Found "<<displays.size()<<" displays!");
		for (auto display : displays) {
			VERSO_LOG_INFO_VARIABLE("verso-demokit", display.toString(), DisplayMode::getDesktopMode(display.index).toString());
		}

		shouldExit = true;
	}

	// List display modes
	if (parser.hasOption(optionListDisplayModes)) {
		if (fullscreen == true) {
			VERSO_LOG_INFO("verso-demokit", "Found "<<displayModesFullscreen.size()<<" fullscreen display modes on: "<<display->name);
			{
				size_t index = 0;
				for (auto displayMode : displayModesFullscreen) {
					VERSO_LOG_INFO_VARIABLE("verso-demokit", index, displayMode.toString());
					index++;
				}
			}
		}
		else {
			VERSO_LOG_INFO("verso-demokit", "Found "<<displayModesWindowed.size()<<" windowed display modes on: "<<display->name);
			{
				size_t index = 0;
				for (auto displayMode : displayModesWindowed) {
					VERSO_LOG_INFO_VARIABLE("verso-demokit", index, displayMode.toString());
					index++;
				}
			}
		}
		shouldExit = true;
	}

	// List audio devices
	if (parser.hasOption(optionListAudioDevices)) {
		VERSO_LOG_INFO("verso-demokit", "Found "<<audioDevices.size()<<" audio devices");
		{
			size_t index = 0;
			for (auto audioDevice : audioDevices) {
				VERSO_LOG_INFO_VARIABLE("verso-demokit", index, audioDevice.toString());
				index++;
			}
		}
		shouldExit = true;
	}

	// Help
	if (parser.hasOption(optionHelp)) {
		helpFormatter.printHelp(binaryName, options);
		shouldExit = true;
	}

	if (shouldExit) {
		return false;
	}

	return true;
}



bool DemoKitSetupDialog::runDialog()
{
	InputManager::instance().create();
	size_t displayIndex = InputManager::instance().getMouseActiveDisplayIndex(displays);

	windowHeight = 200;

	DisplayMode customMode(Vector2i(windowWidth, windowHeight),
						   displays[displayIndex].getCurrentMode().bitsPerPixel, displays[displayIndex].getCurrentMode().refreshRate);
	WindowOpengl window("DemoKitSetupDialog", customMode, displays[displayIndex], demoPlayerSettings.setupDialog.windowTitle, static_cast<WindowStyles>(WindowStyle::Hidden));
	window.setPositionAlignedOnDisplay(displays[displayIndex], Align(HAlign::Center, VAlign::Center));

	imgui.create(window);
	ImGui::SetCurrentContext(imgui.getContext());

	demoPlayerSettings.setupDialog.style.applyStyleToImGui();

	if (!demoPlayerSettings.setupDialog.fontFileName.isEmpty()) {
		UString ttfFontFileName(demoPlayerSettings.demoPaths.pathFonts()+demoPlayerSettings.setupDialog.fontFileName);
		ImGuiIO& io = ImGui::GetIO();
		smallerFont = io.Fonts->AddFontFromFileTTF(ttfFontFileName.c_str(), 18.0f);
		IM_ASSERT(smallerFont != NULL);
		biggerFont = io.Fonts->AddFontFromFileTTF(ttfFontFileName.c_str(), 28.0f);
		IM_ASSERT(biggerFont != NULL);

		windowHeight += 50;
		Vector2i newResolution(windowWidth, windowHeight);
		window.setWindowResolution(newResolution);
		window.onResize(); // hack without event queue
		window.setPositionAlignedOnDisplay(displays[displayIndex], Align(HAlign::Center, VAlign::Center));
	}

	Opengl::printInfo(false);
	GL_CHECK_FOR_ERRORS("", "");

	window.makeActive();
	Render::clearScreen(ColorGenerator::getColor(ColorRgb::Black), true, true);
	window.swapScreenBuffer();

	if (!demoPlayerSettings.setupDialog.logoFileName.isEmpty()) {
		TextureParameters params("texture", TexturePixelFormat::Rgba,
								 MinFilter::Linear, MagFilter::Linear,
								 WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);

		logoTexture = new Texture();
		logoTexture->createFromFile(window, demoPlayerSettings.demoPaths.pathJsons()+demoPlayerSettings.setupDialog.logoFileName, params);
		logoDrawWidth = windowWidth - logoPaddingHorizontal;
		logoDrawHeight = (logoDrawWidth * logoTexture->getResolutionf().y) / logoTexture->getResolutionf().x;

		windowHeight += static_cast<int>(logoDrawHeight) + 20;
		Vector2i newResolution(windowWidth, windowHeight);
		window.setWindowResolution(newResolution);
		window.onResize(); // hack without event queue
		window.setPositionAlignedOnDisplay(displays[displayIndex], Align(HAlign::Center, VAlign::Center));
	}

	window.show();
	GL_CHECK_FOR_ERRORS("", "");

	bool returnValue = false;
	FrameTimer frameTimer(static_cast<double>(customMode.refreshRate.hz));
	while (window.isOpen()) {
		frameTimer.update();
		FrameTimestamp frameTime(frameTimer);

		Event event;
		while (window.pollEvent(event)) {
			if (event.type == EventType::Resized) {
				window.onResize();
			}

			else if (event.type == EventType::Quit) {
				window.close();
				returnValue = false;
				break;
			}
			else if (event.type == EventType::Closed) {
				window.close();
				returnValue = false;
				break;
			}

			else if (event.type == EventType::Keyboard) {
				KeyboardEvent& kb = event.keyboard;
				if (kb.type == KeyboardEventType::Key) {
					// \TODO: maybe dear imgui supports start button focus? would be better solution than this.
					/*if (kb.key.keyCode == KeyCode::Return || kb.key.keyCode == KeyCode::Return2 || kb.key.keyCode == KeyCode::KpEnter) {
						window.close();
						returnValue = true;
						break;
					}
					else */ if (kb.key.keyCode == KeyCode::Escape) {
						window.close();
						returnValue = false;
						break;
					}
				}
			}

			imgui.handleEvent(window, frameTime, event);
		}

		if (window.isOpen()) {
			window.makeActive();

			imgui.newFrame(window, frameTime, false);

			static bool quitted = false;
			static bool continueStart = false;
			Opengl::blend(BlendMode::None);
			runSetupGui(quitted, continueStart, demoPlayerSettings.setupDialog);

			Render::clearScreen(demoPlayerSettings.setupDialog.backgroundColor, true, true);
			imgui.render(window, window.getDrawableViewporti());

			window.swapScreenBuffer();
			frameTimer.addRenderFrame();

			if (quitted == true) {
				window.close();
				returnValue = continueStart;
			}
		}
	}

	imgui.destroy();

	return returnValue;
}


void DemoKitSetupDialog::runSetupGui(bool& quitted, bool& continueStart, const DemoKitSetupDialogSettings& setupDialogSettings)
{
	//bool returnValue = false;
	static bool active = true;

	//context->NavDisableHighlight = false;
	imgui.enablKbHighlight();

	int rootFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar |
					ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove |
					ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoSavedSettings;

	ImGui::SetNextWindowPos(ImVec2(0,0));
	ImGui::SetNextWindowSize(ImGui::GetIO().DisplaySize);
	if (biggerFont != nullptr) {
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(22,0));
	}
	else {
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(15,0));
	}
	ImGui::Begin("Setup Dialog", &active, rootFlags);
	{
		if (logoTexture != nullptr) {
			ImGui::NewLine();
			ImGui::SetCursorPos(ImVec2((windowWidth - logoDrawWidth) * 0.5f, ImGui::GetCursorPosY()));
			ImGui::Image(reinterpret_cast<void*>(static_cast<uint64_t>(logoTexture->getHandle())), ImVec2(logoDrawWidth, logoDrawHeight));
		}

		ImGui::NewLine();


		ImGui::PushItemWidth(windowWidth * 0.65f);
		{
			// Display
			{
				UString preview = "";
				if (display != nullptr) {
					preview = display->toString();
				}
				if (ImGui::BeginCombo("Display", preview.c_str())) {
					for (auto& d : displays) {
						bool isSelected = (display == &d);
						if (ImGui::Selectable(d.toString().c_str(), isSelected)) {
							if (display != &d) {
								display = &d;
								displayModesFullscreen = DisplayMode::getFullscreenModes(display->index);
								displayModesWindowed = DisplayMode::getWindowedModes(
											display->index,
											static_cast<DisplayModeRatings>(DisplayModeRating::All));
								refreshRates = DisplayMode::getFullscreenRefreshRates(display->index);

								// Select desktop mode if exists in the list (Windows + 125% scaling then it might not)
								if (findDisplayMode(DisplayMode::getDesktopMode(display->index), false) == false) {
									// If desktop mode is not found then default to first one in the list
									displayMode = &displayModesFullscreen[0];
								}

								findRefreshRate(displayMode->refreshRate);
							}
						}
						if (isSelected) {
							// Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
							ImGui::SetItemDefaultFocus();
						}
					}
					ImGui::EndCombo();
				}
			}

			// Downscale ratio
			{
				UString preview = "";
				if (downscaleRatio != nullptr) {
					preview = downscaleRatio->toStringRenderAtResolution(displayMode->resolution);
				}
				if (ImGui::BeginCombo("Resolution", preview.c_str())) {
					for (auto& d : downscaleRatios) {
						bool isSelected = (downscaleRatio == &d);
						if (ImGui::Selectable(d.toStringRenderAtResolution(displayMode->resolution).c_str(), isSelected)) {
							downscaleRatio = &d;
						}
						if (isSelected) {
							// Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
							ImGui::SetItemDefaultFocus();
						}
					}
					ImGui::EndCombo();
				}
			}
		}
		ImGui::PopItemWidth();

		ImGui::PushItemWidth(windowWidth * 0.65f);
		{
			// Antialias level
			// antialiasLevel

			// Fullscreen
			if (ImGui::Checkbox("Fullscreen", &fullscreen) == true) {
				// Restore desktop mode if exists in the list (Windows + 125% scaling then it might not)
				if (findDisplayMode(DisplayMode::getDesktopMode(display->index), false) == false) {
					// If desktop mode is not found then default to first one in the list
					displayMode = &displayModesFullscreen[0];
				}

				findRefreshRate(DisplayMode::getDesktopMode(display->index).refreshRate);

				customResolution = false;
			}

			ImGui::SameLine();
			ImGui::Checkbox("Vertical Sync", &vsync);

//			ImGui::SameLine();
//			ImGui::Checkbox("Loop", &loop);

			// \TODO: bool unrestrictedFps
//			ImGui::Checkbox("Extra", &extraDisplayModes);
//			ImGui::SameLine();
//			ImGui::Checkbox("Esoteric", &esotericDisplayModes);

#ifdef _WIN32
			if (fullscreen == true) {
				ImGui::SameLine();
				if (ImGui::Checkbox("Custom resolution", &customResolution) == true) {
					if (customResolution == false) {
						// Restore desktop mode if exists in the list (Windows + 125% scaling then it might not)
						if (findDisplayMode(DisplayMode::getDesktopMode(display->index), false) == false) {
							// If desktop mode is not found then default to first one in the list
							displayMode = &displayModesFullscreen[0];
						}

						findRefreshRate(DisplayMode::getDesktopMode(display->index).refreshRate);
					}
				}
			}
#endif

			if (fullscreen == false || (fullscreen == true && customResolution == true)) {
				ImGui::PushItemWidth(windowWidth * 0.65f);
				{
					// Size
					{
						UString preview = "";
						if (displayMode != nullptr) {
							preview = displayMode->toString();
						}
						if (fullscreen == true) {
							if (ImGui::BeginCombo("##Window1", preview.c_str())) {
								for (auto& dm : displayModesFullscreen) {
									if (dm.refreshRate == *refreshRateSelector) {
										bool isSelected = (displayMode == &dm);
										if (ImGui::Selectable(dm.toString().c_str(), isSelected)) {
											displayMode = &dm;
										}
										if (isSelected) {
											// Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
											ImGui::SetItemDefaultFocus();
										}
									}
								}
								ImGui::EndCombo();
							}
						}
						else {
							if (ImGui::BeginCombo("Window size##Window2", preview.c_str())) {
								for (auto& dm : displayModesWindowed) {
									bool listMode = true;
									if (dm.rating == DisplayModeRating::Extra && extraDisplayModes == false) {
										listMode = false;
									}
									if (dm.rating == DisplayModeRating::Esoteric && esotericDisplayModes == false) {
										listMode = false;
									}
									if (listMode == true) {
										bool isSelected = (displayMode == &dm);
										if (ImGui::Selectable(dm.toString().c_str(), isSelected)) {
											displayMode = &dm;
										}
										if (isSelected) {
											// Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
											ImGui::SetItemDefaultFocus();
										}
									}
								}
								ImGui::EndCombo();
							}
						}
					}
				}

				if (fullscreen == true) {
					ImGui::SameLine();

					ImGui::PushItemWidth(windowWidth * 0.2f);
					{
						// Refresh rate
						{
							UString preview = "";
							if (refreshRateSelector != nullptr) {
								preview.append2(refreshRateSelector->hz);
								preview += " hz";
							}
							if (ImGui::BeginCombo("##Hz", preview.c_str())) {
								if (fullscreen == false) {
									ImGui::Selectable(UString::from(refreshRateSelector->hz).c_str(), true);
									ImGui::SetItemDefaultFocus();
								}
								else {
									for (auto& r : refreshRates) {
										bool isSelected = (refreshRateSelector == &r);
										UString str;
										str.append2(r.hz);
										str += " hz";
										if (ImGui::Selectable(str.c_str(), isSelected)) {
											refreshRateSelector = &r;
											if (fullscreen == true && displayMode->refreshRate != *refreshRateSelector) {
												for (size_t i=0; i<displayModesFullscreen.size(); ++i) {
													if (displayModesFullscreen[i].refreshRate == *refreshRateSelector) {
														displayMode = &displayModesFullscreen[i];
														break;
													}
												}
											}
										}
										if (isSelected) {
											// Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
											ImGui::SetItemDefaultFocus();
										}
									}
								}
								ImGui::EndCombo();
							}
						}
					}
					ImGui::PopItemWidth();
				}
			}

			// Audio device
			{
				UString preview = "";
				if (audioDevice != nullptr) {
					preview = audioDevice->toString();
				}
				if (ImGui::BeginCombo("##Audio", preview.c_str())) {
					for (auto& d : audioDevices) {
						bool isSelected = (audioDevice == &d);
						if (ImGui::Selectable(d.toString().c_str(), isSelected)) {
							audioDevice = &d;
						}
						if (isSelected) {
							// Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
							ImGui::SetItemDefaultFocus();
						}
					}
					ImGui::EndCombo();
				}
			}

			ImGui::SameLine();

			ImGui::Checkbox("Mute", &muteAudio);
		}
		ImGui::PopItemWidth();


		ImGui::NewLine();

		if (biggerFont != nullptr) {
			ImGui::PushFont(biggerFont);
		}
		{
			float mainButtonWidth = 175.0f;
			if (ImGui::Button(setupDialogSettings.startButtonText.c_str(), ImVec2(mainButtonWidth, 0)) == true) {
				quitted = true;
				continueStart = true;
			}
			ImGui::SetItemDefaultFocus();

			ImGui::SameLine();

			if (ImGui::Button(setupDialogSettings.quitButtonText.c_str(), ImVec2(mainButtonWidth, 0)) == true) {
				quitted = true;
				continueStart = false;
			}
		}
		if (biggerFont != nullptr) {
			ImGui::PopFont();
		}
	}
	ImGui::End();
	ImGui::PopStyleVar(1);
}


} // End namespace Verso

