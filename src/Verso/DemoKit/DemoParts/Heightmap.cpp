#include <Verso/DemoKit/DemoParts/Heightmap.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>
#include <Verso/Render/Camera/CameraFps.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/System/JsonHelperDemoKit.hpp>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(true, true);
const BlendMode defaultBlendMode = BlendMode::Transcluent;


Heightmap::Heightmap(const DemoPaths* demoPaths, const JsonPath& demoPartJson,
					 std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) :
	DemoPart(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator),
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	blendMode(defaultBlendMode),
	unitSize(),
	gridSize(),
	offsetPosition(),
	//sourceFileName(), // \TODO:

	created(false),
	vao("DemoKit/Heightmap"),
	material()
{
	const JsonPath& params = demoPartJson.getAttribute("params", false);

	clearParam.parseAttributeAsObject(params, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(params, "depth", false, defaultDepthParam);

	blendMode = JsonHelper::readBlendMode(params, "blend", false, defaultBlendMode);

	unitSize = params.readVector2f("unitSize", false, Vector2f(1.0f, 1.0f));

	gridSize = params.readVector2i("gridSize", false, Vector2i(10, 10));

	offsetPosition = params.readVector3f("offsetPosition", false, Vector3f(0.0f, 0.0f, 0.0f));

//		sourceFileName = params.readString("source", true); // \TODO:
}


Heightmap::Heightmap(Heightmap&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	blendMode(std::move(original.blendMode)),
	unitSize(std::move(original.unitSize)),
	gridSize(std::move(original.gridSize)),
	offsetPosition(std::move(original.offsetPosition)),
	//sourceFileName(std::move(original.sourceFileName)), // \TODO:

	created(std::move(original.created)),
	vao(std::move(original.vao)),
	material(std::move(original.material))
{
	//original.ptr = 0;
}


Heightmap::~Heightmap()
{
	Heightmap::destroy();
}


Heightmap& Heightmap::operator =(Heightmap&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		blendMode = std::move(original.blendMode);
		unitSize = std::move(original.unitSize);
		gridSize = std::move(original.gridSize);
		offsetPosition = std::move(original.offsetPosition);
		//sourceFileName = std::move(original.sourceFileName); // \TODO:

		created = std::move(original.created);
		vao = std::move(original.vao);
		material = std::move(original.material);

		//original.ptr = 0;
	}
	return *this;
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////
void Heightmap::create(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");

	material.create(window,
					demoPaths->pathTextures()+"KhufuPyramid/sandstone256.jpg",
					demoPaths->pathTextures()+"sand-specular.png",
					demoPaths->pathTextures()+"heightmaps/dunes.png",
					50.0f,
					PhongMaterialParam::gold());

	VaoGenerator::gridPlane3d(
				vao, unitSize, gridSize,
				Vector2f(static_cast<float>(gridSize.x), static_cast<float>(gridSize.y)) / 2.0f,
				offsetPosition, static_cast<BufferTypes>(BufferType::All), RgbaColorf(1.0f, 0.0f, 0.0f));

	created = true;

	reset(window);
}


void Heightmap::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == true, "DemoPart must be created before calling reset()");
}


void Heightmap::destroy() VERSO_NOEXCEPT
{
	if (Heightmap::isCreated() == false) {
		return;
	}

	material.destroy();

	created = false;
}


bool Heightmap::isCreated() const
{
	return created;
}


void Heightmap::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window;
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);
	Opengl::setPolygonRenderMode(PolygonRenderMode::Fill);

	// \TODO: implement Heightmap DemoPart

	Opengl::blend(BlendMode::None);
}


} // End namespace Verso

