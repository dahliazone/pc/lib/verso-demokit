#include <Verso/DemoKit/DemoParts/SimpleScroller.hpp>
#include <Verso/System/JsonHelperDemoKit.hpp>
#include <Verso/Render/Render.hpp>
#include <cmath>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, false);
const BlendMode defaultBlendMode = BlendMode::Transcluent;
const UString defaultScrollerText("Placeholder scroller text... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet quam quam, ut suscipit ante dignissim a. Praesent rhoncus sapien vitae dolor lacinia, sed finibus est molestie. Aliquam non rhoncus nisi, et pretium tellus. Sed convallis nibh massa, ut pretium leo tempor at. Quisque dolor dolor, vulputate sed dapibus id, consectetur ut massa. Pellentesque quis ex scelerisque, accumsan dolor ac, ornare justo. Nullam id massa eget arcu ultrices tristique et ut enim. Donec egestas leo a dapibus vehicula. Curabitur efficitur leo sed ligula efficitur semper. Proin at dolor in dui lobortis efficitur. Suspendisse fermentum est nec ligula accumsan pulvinar. Praesent elit ex, finibus ac orci ac, tincidunt placerat leo. Praesent in neque quam.");
const float defaultRelativePositionY = 0.5f;
const float defaultScrollSpeedX = 0.4f;
const float defaultScrollPosX = 1.0f;
const float defaultAlpha = 1.0f;
const Vector2f defaultRelativeCharacterSize(0.08f, 0.08f);
const Vector2f defaultRelativeSpacing(0.01f, 0.01f);
const RefScale defaultRefScale = RefScale::ViewportSize_KeepAspectRatio_FromX;
const Degree defaultAngleZ = 0.0f;


SimpleScroller::SimpleScroller(
		const DemoPaths* demoPaths, const JsonPath& demoPartJson,
		std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) :
	DemoPart(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator),
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	blendMode(defaultBlendMode),
	fontParam(),
	text(defaultScrollerText),
	relativePositionYKeyframes(defaultRelativePositionY),
	scrollSpeedX(defaultScrollSpeedX),
	scrollPosX(defaultScrollPosX),
	alphaKeyframes(defaultAlpha),
	relativeCharacterSizeKeyframes(defaultRelativeCharacterSize),
	relativeSpacingKeyframes(defaultRelativeSpacing),
	refScale(defaultRefScale),
	angleZKeyframes(defaultAngleZ),

	created(false),
	window(nullptr),
	camera(),
	currentScrollPosX(defaultScrollPosX),
	font(nullptr)
{
	const JsonPath& params = demoPartJson.getAttribute("params", false);

	clearParam.parseAttributeAsObject(params, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(params, "depth", false, defaultDepthParam);

	blendMode = JsonHelper::readBlendMode(params, "blend", false, defaultBlendMode);

	fontParam.parseAttributeAsObject(params);

	text = params.readStringOrArrayOfStrings("text", false, defaultScrollerText);

	// Make sure there are no newlines
	text.replaceAll('\n', ' ');

	relativePositionYKeyframes = params.readFloatKeyframes("relativePositionY", false, FloatKeyframes(defaultRelativePositionY));

	scrollSpeedX = params.readNumberf("scrollSpeedX", false, defaultScrollSpeedX);

	scrollPosX = params.readNumberf("scrollPosX", false, defaultScrollPosX);
	currentScrollPosX = scrollPosX;

	alphaKeyframes = params.readFloatKeyframes("alpha", false, FloatKeyframes(defaultAlpha));

	relativeCharacterSizeKeyframes = params.readVector2fKeyframes("relativeCharacterSize", false, Vector2fKeyframes(defaultRelativeCharacterSize));

	relativeSpacingKeyframes = params.readVector2fKeyframes("relativeSpacing", false, Vector2fKeyframes(defaultRelativeSpacing));

	refScale = params.readRefScale("refScale", false, defaultRefScale);

	angleZKeyframes = params.readFloatKeyframes("angleZ", false, FloatKeyframes(defaultAngleZ));
}


SimpleScroller::SimpleScroller(SimpleScroller&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	blendMode(std::move(original.blendMode)),
	fontParam(std::move(original.fontParam)),
	text(std::move(original.text)),
	relativePositionYKeyframes(std::move(original.relativePositionYKeyframes)),
	scrollSpeedX(std::move(original.scrollSpeedX)),
	scrollPosX(std::move(original.scrollPosX)),
	alphaKeyframes(std::move(original.alphaKeyframes)),
	relativeCharacterSizeKeyframes(std::move(original.relativeCharacterSizeKeyframes)),
	relativeSpacingKeyframes(std::move(original.relativeSpacingKeyframes)),
	refScale(std::move(original.refScale)),
	angleZKeyframes(std::move(original.angleZKeyframes)),

	created(std::move(original.created)),
	window(std::move(original.window)),
	camera(std::move(original.camera)),
	currentScrollPosX(std::move(original.currentScrollPosX)),
	font(std::move(original.font))
{
	original.window = nullptr;
	original.font = nullptr;
}


SimpleScroller::~SimpleScroller()
{
	SimpleScroller::destroy();
}


SimpleScroller& SimpleScroller::operator =(SimpleScroller&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		blendMode = std::move(original.blendMode);
		fontParam = std::move(original.fontParam);
		text = std::move(original.text);
		relativePositionYKeyframes = std::move(original.relativePositionYKeyframes);
		scrollSpeedX = std::move(original.scrollSpeedX);
		scrollPosX = std::move(original.scrollPosX);
		alphaKeyframes = std::move(original.alphaKeyframes);
		relativeCharacterSizeKeyframes = std::move(original.relativeCharacterSizeKeyframes);
		relativeSpacingKeyframes = std::move(original.relativeSpacingKeyframes);
		refScale = std::move(original.refScale);
		angleZKeyframes = std::move(original.angleZKeyframes);

		created = std::move(original.created);
		window = std::move(original.window);
		camera = std::move(original.camera);
		currentScrollPosX = std::move(original.currentScrollPosX);
		font = std::move(original.font);

		original.window = nullptr;
		original.font = nullptr;
	}
	return *this;
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////

void SimpleScroller::create(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");
	this->window = &window;

	// Setup camera
	camera.create(&window, "DemoKit/SimpleScroller/camera." + getInstanceStr(), false);

	font = new BitmapFontFixedWidth();
	font->createFromFile(
				window, demoPaths->pathFonts() + fontParam.texture.sourceFileName,
				fontParam.texture.minFilter,
				fontParam.texture.magFilter,
				Vector2f(1.0f / 16.0f, 1.0f / 16.0f),
				fontParam.firstFontCharacterIndex,
				fontParam.tabSize);

	created = true;

	reset(window);
}


void SimpleScroller::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == true, "DemoPart must be created before calling reset()");

	camera.reset(Vector3f(0.0f, 0.0f, 1.0f), Vector3f::up());
	camera.setProjectionOrthographic(Rangef(0.001f, 10.0f));
	camera.setTarget(Vector3f(0.0f, 0.0f, 0.0f));
	camera.setPosition(Vector3f(0.0f, 0.0f, -9.9f));

	currentScrollPosX = scrollPosX;
}


void SimpleScroller::destroy() noexcept
{
	if (SimpleScroller::isCreated() == false) {
		return;
	}

	camera.destroy();

	delete font;
	font = nullptr;

	created = false;
}


bool SimpleScroller::isCreated() const
{
	return created;
}


void SimpleScroller::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	this->currentScrollPosX = this->scrollPosX - this->scrollSpeedX * seconds;

	const Vector3f point(
				currentScrollPosX,
				relativePositionYKeyframes.getValueInterpolated(seconds),
				0.0f);

	const Vector2f relativeCharacterSize = relativeCharacterSizeKeyframes.getValueInterpolated(seconds);

	if (point.x > 1.0f || point.x + text.size() * relativeCharacterSize.x < 0.0f) {
		return;
	}

	if (point.y + relativeCharacterSize.y*1.5f < 0.0f || point.y > 1.0f) {
		return;
	}

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);
	Opengl::setPolygonRenderMode(PolygonRenderMode::Fill);

	int firstIndex = 0;
	if (point.x < 0.0f) {
		firstIndex = static_cast<int>(floor(-point.x / relativeCharacterSize.x));
	}
	Vector3f pointAdjusted(point.x + firstIndex * relativeCharacterSize.x, point.y, point.z);
	UString textToBeRendered = text.substring(
		firstIndex,
		static_cast<int>(floor((1.0f + 2.0f * relativeCharacterSize.x) / relativeCharacterSize.x)));

	font->renderTextIndividualLetters(
				window, camera, textToBeRendered,
				pointAdjusted, relativeCharacterSize,
				relativeSpacingKeyframes.getValueInterpolated(seconds),
				refScale,
				alphaKeyframes.getValueInterpolated(seconds),
				angleZKeyframes.getValueInterpolated(seconds));

	Opengl::blend(BlendMode::None);

	GL_CHECK_FOR_ERRORS("", "");
}


} // End namespace Verso

