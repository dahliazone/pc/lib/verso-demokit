#include <Verso/DemoKit/DemoParts/ImageViewer.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>
#include <Verso/Render/Camera/CameraFps.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/System/JsonHelperDemoKit.hpp>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, false);

const Vector3f defaultCameraPosition(0.0f, 0.0f, -9.9f);
const Vector3f defaultCameraTarget(0.0f, 0.0f, 0.0f);
const Vector3f defaultCameraDesiredUp(Vector3f::up());
const float defaultCameraFov(90.0f);
const Rangef defaultCameraNearFarPlane(0.001f, 10.0f);
const CameraParam defaultCameraParam(
		CameraType::Target, ProjectionType::Orthographic,
		Vector3fKeyframes(defaultCameraPosition), Vector3fKeyframes(defaultCameraTarget),
		Vector3fKeyframes(defaultCameraDesiredUp),
		FloatKeyframes(defaultCameraFov), defaultCameraNearFarPlane);

const BlendMode defaultBlendMode = BlendMode::Transcluent;
const Vector2f defaultPosition(0.5f, 0.5f);
const Align defaultAlign(HAlign::Center, VAlign::Center);
const Vector2f defaultRelSize(1.0f, 1.0f);
const RefScale defaultRefScale(RefScale::ViewportSize_KeepAspectRatio_FitRect);
const float defaultAngle(0.0f);
const float defaultAlpha(1.0f);
const Vector2f defaultTextureOffset(0.0f, 0.0f);


const Verso::TextureParametersParam defaultTextureParametersParam(
		"", "", Verso::TexturePixelFormat::Unset,
		Verso::MinFilter::Linear, Verso::MagFilter::Linear,
		Verso::WrapStyle::ClampToEdge, Verso::WrapStyle::ClampToEdge);


ImageViewer::ImageViewer(const DemoPaths* demoPaths, const JsonPath& demoPartJson,
						 std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) :
	DemoPart(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator),
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	cameraParam(defaultCameraParam),
	blendMode(defaultBlendMode),
	textureParam(defaultTextureParametersParam),
	positionKeyframes(),
	alignFromPosition(),
	relSizeKeyframes(defaultRelSize),
	refScale(defaultRefScale),
	rotationAngleKeyframes(defaultAngle),
	alphaKeyframes(defaultAlpha),
	textureOffsetKeyframes(defaultTextureOffset),

	created(false),
	texture(),
	camera()
{
	const JsonPath& params = demoPartJson.getAttribute("params", false);

	clearParam.parseAttributeAsObject(params, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(params, "depth", false, defaultDepthParam);

	cameraParam.parseAttributeAsObject(params, "camera", false, defaultCameraParam);

	blendMode = JsonHelper::readBlendMode(params, "blend", false, defaultBlendMode);

	textureParam.parseAttributeAsObject(params, "texture", true, defaultTextureParametersParam);

	positionKeyframes = params.readVector2fKeyframes("position", false, Vector2fKeyframes(defaultPosition));

	alignFromPosition = params.readAlign("alignFromPosition", false, defaultAlign);

	relSizeKeyframes = params.readVector2fKeyframes("relSize", false, Vector2fKeyframes(defaultRelSize));

	refScale = params.readRefScale("refScale", false, defaultRefScale);

	rotationAngleKeyframes = params.readFloatKeyframes("angle", false, FloatKeyframes(defaultAngle));

	alphaKeyframes = params.readFloatKeyframes("alpha", false, FloatKeyframes(defaultAlpha));

	textureOffsetKeyframes = params.readVector2fKeyframes("textureOffset", false, Vector2fKeyframes(defaultTextureOffset));
}


ImageViewer::ImageViewer(ImageViewer&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	cameraParam(std::move(original.cameraParam)),
	blendMode(std::move(original.blendMode)),
	textureParam(std::move(original.textureParam)),
	positionKeyframes(std::move(original.positionKeyframes)),
	alignFromPosition(std::move(original.alignFromPosition)),
	relSizeKeyframes(std::move(original.relSizeKeyframes)),
	refScale(std::move(original.refScale)),
	rotationAngleKeyframes(std::move(original.rotationAngleKeyframes)),
	alphaKeyframes(std::move(original.alphaKeyframes)),
	textureOffsetKeyframes(std::move(original.textureOffsetKeyframes)),

	created(std::move(original.created)),
	texture(std::move(original.texture)),
	camera(std::move(original.camera))
{
	// reset original
}


ImageViewer& ImageViewer::operator =(ImageViewer&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		cameraParam = std::move(original.cameraParam);
		blendMode = std::move(original.blendMode);
		textureParam = std::move(original.textureParam);
		positionKeyframes = std::move(original.positionKeyframes);
		alignFromPosition = std::move(original.alignFromPosition);
		relSizeKeyframes = std::move(original.relSizeKeyframes);
		refScale = std::move(original.refScale);
		rotationAngleKeyframes = std::move(original.rotationAngleKeyframes);
		alphaKeyframes = std::move(original.alphaKeyframes);
		textureOffsetKeyframes = std::move(original.textureOffsetKeyframes);

		created = std::move(original.created);
		texture = std::move(original.texture);
		camera = std::move(original.camera);

		// reset original
	}
	return *this;
}


ImageViewer::~ImageViewer()
{
	ImageViewer::destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////
void ImageViewer::create(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");

	// Texture
	texture.createFromFile(
				window,
				demoPaths->pathTextures()+textureParam.sourceFileName, TextureParameters("iChannel0"));
	texture.setWrapStyle(textureParam.wrapStyleS, textureParam.wrapStyleT);
	texture.setMinFilter(textureParam.minFilter);
	texture.setMagFilter(textureParam.magFilter);

	// Setup camera
	camera.create(&window, "DemoKit/ImageViewer/camera." + getInstanceStr(), false);

	created = true;

	reset(window);
}


void ImageViewer::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == true, "DemoPart must be created before calling reset()");

	camera.set(cameraParam);
}


void ImageViewer::destroy() VERSO_NOEXCEPT
{
	if (ImageViewer::isCreated() == false) {
		return;
	}

	texture.destroy();

	created = false;
}


bool ImageViewer::isCreated() const
{
	return created;
}


void ImageViewer::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	camera.setPosition(cameraParam.positionKeyframes.getValueInterpolated(seconds));
	camera.setTarget(cameraParam.targetKeyframes.getValueInterpolated(seconds));
	camera.setDesiredUp(cameraParam.desiredUpKeyframes.getValueInterpolated(seconds));
	if (cameraParam.projectionType == ProjectionType::Perspective) {
		camera.setFovY(cameraParam.fovYKeyframes.getValueInterpolated(seconds));
	}
	else if (cameraParam.projectionType == ProjectionType::Orthographic) {
		camera.setOrthographicZoomLevel(cameraParam.orthographicZoomLevelKeyframes.getValueInterpolated(seconds));
		camera.setOrthographicRotation(cameraParam.orthographicRotationKeyframes.getValueInterpolated(seconds));
	}

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);
	Opengl::setPolygonRenderMode(PolygonRenderMode::Fill);

	Render::draw2d(
				window, camera, texture,
				positionKeyframes.getValueInterpolated(seconds),
				alignFromPosition,
				relSizeKeyframes.getValueInterpolated(seconds),
				refScale,
				alphaKeyframes.getValueInterpolated(seconds),
				rotationAngleKeyframes.getValueInterpolated(seconds),
				textureOffsetKeyframes.getValueInterpolated(seconds));

	Opengl::blend(BlendMode::None);
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString
///////////////////////////////////////////////////////////////////////////////////////////

UString ImageViewer::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "\n" + newLinePadding2 + "params: {";
	{
		UString newLinePadding3 = newLinePadding2 + "  ";
		str += "\n" + newLinePadding3 + "textureParam=" + textureParam.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "clearParam=" + clearParam.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "depthParam=" + depthParam.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "cameraParam=" + cameraParam.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "blendMode=" + blendModeToString(blendMode);
		str += ",\n" + newLinePadding3 + "positionKeyframes=" + positionKeyframes.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "alignFromPosition=" + alignFromPosition.toString();
		str += ",\n" + newLinePadding3 + "relSizeKeyframes=" + relSizeKeyframes.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "refScale=" + refScaleToString(refScale);
		str += ",\n" + newLinePadding3 + "rotationAngleKeyframes=" + rotationAngleKeyframes.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "alphaKeyframes=" + alphaKeyframes.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "textureOffsetKeyframes=" + textureOffsetKeyframes.toString(newLinePadding3);
	}
	str += ",\n" + newLinePadding2 + "}";

	str += ",\n" + newLinePadding2 + "state: {";
	{
		UString newLinePadding3 = newLinePadding2 + "  ";
		str += "\n" + newLinePadding3 + "created=";
		if (created == true) {
			str += "true";
		}
		else {
			str += "false";
		}
		str += ",\n" + newLinePadding3 + "texture=" + texture.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "camera=" + camera.toString(newLinePadding3);
	}
	str += ",\n" + newLinePadding + "}";

	return str;
}


UString ImageViewer::toStringDebug(const UString& newLinePadding) const
{
	UString str("ImageViewer(\n");
	str += toString(newLinePadding);
	str += "\n)";
	return str;
}


} // End namespace Verso

