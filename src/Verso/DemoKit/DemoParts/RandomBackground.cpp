#include <Verso/DemoKit/DemoParts/RandomBackground.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, false);
const BlendMode defaultBlendMode = BlendMode::None;


RandomBackground::RandomBackground(const DemoPaths* demoPaths, const JsonPath& demoPartJson,
								   std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) :
	DemoPart(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator),
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	blendMode(defaultBlendMode),
	sourcePath(demoPaths->pathTextures()),

	created(false),
	texBg(),
	camera()
{
	const JsonPath& params = demoPartJson.getAttribute("params", false);

	clearParam.parseAttributeAsObject(params, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(params, "depth", false, defaultDepthParam);

	blendMode = JsonHelper::readBlendMode(params, "blend", false, defaultBlendMode);

	sourcePath = File::addSlashToPathIfNeeded(params.readString("sourcePath", false, demoPaths->pathTextures()));

	clearParam.parseAttributeAsObject(params);
}


RandomBackground::RandomBackground(RandomBackground&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	blendMode(std::move(original.blendMode)),
	sourcePath(std::move(original.sourcePath)),

	created(std::move(original.created)),
	texBg(std::move(original.texBg)),
	camera(std::move(original.camera))
{
	//original.ptr = 0;
}


RandomBackground& RandomBackground::operator =(RandomBackground&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		blendMode = std::move(original.blendMode);
		sourcePath = std::move(original.sourcePath);
		clearParam = std::move(original.clearParam);

		created = std::move(original.created);
		texBg = std::move(original.texBg);
		camera = std::move(original.camera);

		//original.ptr = 0;
	}
	return *this;
}


RandomBackground::~RandomBackground()
{
	RandomBackground::destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////
void RandomBackground::create(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");

	UString searchPath = demoPaths->pathTextures()+sourcePath;
	std::vector<UString> files = File::ls(searchPath, true, false, false);
	if (files.size() == 0) {
		VERSO_FILENOTFOUND("verso-demokit", "Cannot find any files in the given path", searchPath.c_str());
	}

	Random::initSeedWithTime();
	size_t startIndex = Random::uintRange(0, static_cast<int>(files.size())-1);
	bool found = false;
	for (size_t i=0; i<files.size(); ++i) {
		UString fileName = searchPath+files[(startIndex+i)%files.size()];
		if (texBg.isSupportedByFileExtension(fileName)) {
			texBg.createFromFile(window, fileName, TextureParameters("texture"));
			texBg.setWrapStyle(WrapStyle::ClampToEdge, WrapStyle::ClampToEdge);
			texBg.setMinFilter(MinFilter::Linear);
			texBg.setMagFilter(MagFilter::Linear);

			found = true;
			break;
		}
	}
	if (found == false) {
		VERSO_FILENOTFOUND("verso-demokit", "Cannot find any supported files in the given path", searchPath.c_str());
	}

	// Setup camera
	camera.create(&window, "DemoKit/RandomBackground/camera." + getInstanceStr(), false);

	created = true;

	reset(window);
}


void RandomBackground::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == true, "DemoPart must be created before calling reset()");

	//camera.reset(Vector3f(0.0f, 0.0f, 1.0f), Vector3f::up());
	//camera.setProjectionOrthographic(Rangef(0.001f, 10.0f));
	camera.reset(Vector3f(0.0f, 0.0f, 1.0f), Vector3f::up());
	camera.setProjectionOrthographic(Rangef(0.001f, 10.0f));
	camera.setTarget(Vector3f(0.0f, 0.0f, 0.0f));
	camera.setPosition(Vector3f(0.0f, 0.0f, -9.9f));
}


void RandomBackground::destroy() VERSO_NOEXCEPT
{
	if (RandomBackground::isCreated() == false) {
		return;
	}

	camera.destroy();

	texBg.destroy();

	created = false;
}


bool RandomBackground::isCreated() const
{
	return created;
}


void RandomBackground::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);
	Opengl::setPolygonRenderMode(PolygonRenderMode::Fill);

	Render::draw2d(
				window, camera, texBg,
				Vector3f(0.5f, 0.5f),
				Align(HAlign::Center, VAlign::Center),
				Vector2f(1.0f, 1.0f),
				RefScale::ViewportSize_KeepAspectRatio_FitRect,
				1.0f,
				0.0f);

	Opengl::blend(BlendMode::None);
}


} // End namespace Verso

