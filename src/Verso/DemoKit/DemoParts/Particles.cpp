#include <Verso/DemoKit/DemoParts/Particles.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/Render/VaoGenerator/VaoGenerator.hpp>
#include <Verso/Render/Camera/CameraFps.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/System/JsonHelperDemoKit.hpp>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, true);
const BlendMode defaultBlendMode = BlendMode::Additive;


Particles::Particles(const DemoPaths* demoPaths, const JsonPath& demoPartJson,
					 std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) :
	DemoPart(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator),
	// Params
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	blendMode(defaultBlendMode),
	particlesParam(),
	behaviourParam(),
	emitterParam(),
	sequencerParam(),

	// State
	created(false),
	particleBehaviour(nullptr),
	particleSystem(nullptr),
	particleEmitter(nullptr),
	particleSequencer(nullptr)
{
	const JsonPath& params = demoPartJson.getAttribute("params", false);

	clearParam.parseAttributeAsObject(params, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(params, "depth", false, defaultDepthParam);

	blendMode = JsonHelper::readBlendMode(params, "blend", false, defaultBlendMode);

	particlesParam.parseAttributeAsObject(params);

	behaviourParam.parseAttributeAsObject(params);

	emitterParam.parseAttributeAsObject(params);

	sequencerParam.parseAttributeAsObject(params);
}


Particles::Particles(Particles&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	blendMode(std::move(original.blendMode)),
	particlesParam(std::move(original.particlesParam)),
	behaviourParam(std::move(original.behaviourParam)),
	emitterParam(std::move(original.emitterParam)),
	sequencerParam(std::move(original.sequencerParam)),

	created(std::move(original.created)),
	particleBehaviour(std::move(original.particleBehaviour)),
	particleSystem(std::move(original.particleSystem)),
	particleEmitter(std::move(original.particleEmitter)),
	particleSequencer(std::move(original.particleSequencer))
{
	original.particleBehaviour = nullptr;
	original.particleSystem = nullptr;
	original.particleEmitter = nullptr;
	original.particleSequencer = nullptr;
}


Particles::~Particles()
{
	Particles::destroy();
}


Particles& Particles::operator =(Particles&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		blendMode = std::move(original.blendMode);
		particlesParam = std::move(original.particlesParam);
		behaviourParam = std::move(original.behaviourParam);
		emitterParam = std::move(original.emitterParam);
		sequencerParam = std::move(original.sequencerParam);

		created = std::move(original.created);
		particleBehaviour = std::move(original.particleBehaviour);
		particleSystem = std::move(original.particleSystem);
		particleEmitter = std::move(original.particleEmitter);
		particleSequencer = std::move(original.particleSequencer);

		original.particleBehaviour = nullptr;
		original.particleSystem = nullptr;
		original.particleEmitter = nullptr;
		original.particleSequencer = nullptr;
	}
	return *this;
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////
void Particles::create(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");

	particleBehaviour = behaviourParam.createBehaviour();

	particleSystem = particlesParam.createParticleSystem(window, "TODO");
	particleSystem->setParticleBehaviour(particleBehaviour);

	particleEmitter = emitterParam.createEmitter();
	particleEmitter->setParticleSystem(particleSystem);

	particleSequencer = sequencerParam.createParticleSequencer();
	particleSequencer->setParticleEmitter(particleEmitter);

	created = true;

	reset(window);
}


void Particles::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == true, "DemoPart must be created before calling reset()");

	particleBehaviour->reset();
	particleSystem->reset();
	particleEmitter->reset();
	particleSequencer->reset();

}


void Particles::destroy() VERSO_NOEXCEPT
{
	if (Particles::isCreated() == false) {
		return;
	}

	delete particleSequencer;
	particleSequencer = nullptr;

	delete particleEmitter;
	particleEmitter = nullptr;

	delete particleSystem;
	particleSystem = nullptr;

	delete particleBehaviour;
	particleBehaviour = nullptr;

	created = false;
}


bool Particles::isCreated() const
{
	return created;
}


void Particles::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window;
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	particleSequencer->sequence(time);
	particleSystem->calculate(time);

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);

	// \TODO: implement Particles::DemoPart
//	particleBehaviour->reset();
//	particleSystem->reset();
//	particleEmitter->reset();
//	particleSequencer->reset();

	//particleSystem->render(window, camera);

	Opengl::blend(BlendMode::None);
}


} // End namespace Verso

