#include <Verso/DemoKit/DemoParts/Sprites3d.hpp>
#include <Verso/System/JsonHelperDemoKit.hpp>
#include <Verso/Render/Render.hpp>
#include <cmath>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(true, true);

const Vector3f defaultCameraPosition(0.0f, 0.0f, -9.9f);
const Vector3f defaultCameraTarget(0.0f, 0.0f, 0.0f);
const Vector3f defaultCameraDesiredUp(Vector3f::up());
const float defaultCameraFov(90.0f);
const Rangef defaultCameraNearFarPlane(0.001f, 10.0f);
const CameraParam defaultCameraParam(
		CameraType::Target, ProjectionType::Perspective,
		Vector3fKeyframes(defaultCameraPosition), Vector3fKeyframes(defaultCameraTarget),
		Vector3fKeyframes(defaultCameraDesiredUp),
		FloatKeyframes(defaultCameraFov), defaultCameraNearFarPlane);


Sprites3d::Sprites3d(
		const DemoPaths* demoPaths, const JsonPath& demoPartJson,
		std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) :
	DemoPart(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator),
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	cameraParam(),
	sprite3dParams(),

	created(false),
	window(nullptr),
	camera(),
	textures(),
	sprites()
{
	const JsonPath& params = demoPartJson.getAttribute("params", false);

	clearParam.parseAttributeAsObject(params, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(params, "depth", false, defaultDepthParam);

	cameraParam.parseObject(params.getAttribute("camera", false), defaultCameraParam);

	sprite3dParams = Sprite3dParam::parseAttributeAsArray(params, "sprites3d", true);
}


Sprites3d::Sprites3d(Sprites3d&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	cameraParam(std::move(original.cameraParam)),
	sprite3dParams(std::move(original.sprite3dParams)),

	created(std::move(original.created)),
	window(std::move(original.window)),
	camera(std::move(original.camera)),
	textures(std::move(original.textures)),
	sprites(std::move(original.sprites))
{
	original.window = nullptr;
}


Sprites3d& Sprites3d::operator =(Sprites3d&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		cameraParam = std::move(original.cameraParam);
		sprite3dParams = std::move(original.sprite3dParams);

		created = std::move(original.created);
		window = std::move(original.window);
		camera = std::move(original.camera);
		textures = std::move(original.textures);
		sprites = std::move(original.sprites);

		original.window = nullptr;
	}
	return *this;
}


Sprites3d::~Sprites3d()
{
	Sprites3d::destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////

void Sprites3d::create(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");
	this->window = &window;

	camera.create(&window, "verso-demokit/Sprites3d/camera." + getInstanceStr(), false);

	ResourceManager& resourceManager = window.getResourceManager();

	for (auto& sprite3dParam : sprite3dParams) {
		TextureParameters textureParameters(sprite3dParam.texture);
		textures.push_back(
					resourceManager.loadTexture(demoPaths->pathTextures()+sprite3dParam.texture.sourceFileName, textureParameters));

		Sprite3d sprite;
		sprite.createFromTexture(textures[textures.size() - 1]);

		if (sprite3dParam.animationParams.size() > 0) {
			sprite.setAnimationsFromTexture(sprite3dParam.animationParams);
			sprite.setCurrentAnimationIndex(sprite3dParam.currentAnimationIndex.getValueInterpolated(0.0f));
		}

		sprites.push_back(sprite);
	}

	created = true;

	reset(window);
}


void Sprites3d::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == true, "DemoPart must be created before calling reset()");

	camera.set(cameraParam);

	size_t i = 0;
	for (auto& sprite : sprites) {
		sprite.setRelOffset(sprite3dParams[i].relOffset);
		sprite.setRefScale(sprite3dParams[i].refScale);
		i++;
	}
}


void Sprites3d::destroy() noexcept
{
	if (Sprites3d::isCreated() == false) {
		return;
	}

	for (auto& sprite : sprites) {
		sprite.destroy();
	}

	if (window != nullptr) {
		ResourceManager& resourceManager = window->getResourceManager();
		for (auto* texture : textures) {
			resourceManager.releaseTexture(texture);
		}
	}

	camera.destroy();

	created = false;
}


bool Sprites3d::isCreated() const
{
	return created;
}


void Sprites3d::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	camera.setPosition(cameraParam.positionKeyframes.getValueInterpolated(seconds));
	camera.setTarget(cameraParam.targetKeyframes.getValueInterpolated(seconds));
	camera.setDesiredUp(cameraParam.desiredUpKeyframes.getValueInterpolated(seconds));
	if (cameraParam.projectionType == ProjectionType::Perspective) {
		camera.setFovY(cameraParam.fovYKeyframes.getValueInterpolated(seconds));
	}
	else if (cameraParam.projectionType == ProjectionType::Orthographic) {
		camera.setOrthographicZoomLevel(cameraParam.orthographicZoomLevelKeyframes.getValueInterpolated(seconds));
		camera.setOrthographicRotation(cameraParam.orthographicRotationKeyframes.getValueInterpolated(seconds));
	}

	size_t i = 0;
	for (auto& sprite : sprites) {
		auto& p = sprite3dParams[i];
		sprite.setPosition(p.positionKeyframes.getValueInterpolated(seconds));
		sprite.setRelSize(p.relSizeKeyframes.getValueInterpolated(seconds));
		sprite.setYaw(p.yawAngleKeyframes.getValueInterpolated(seconds));
		sprite.setPitch(p.pitchAngleKeyframes.getValueInterpolated(seconds));
		sprite.setRoll(p.rollAngleKeyframes.getValueInterpolated(seconds));
		sprite.setAlpha(p.alphaKeyframes.getValueInterpolated(seconds));
		sprite.setCurrentAnimationIndex(p.currentAnimationIndex.getValueInterpolated(seconds));
		i++;
	}

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	i = 0;
	for (auto& sprite : sprites) {
		Opengl::blend(sprite3dParams[i].blendMode);
		sprite.render(window, time, camera);
		i++;
	}

	Opengl::blend(BlendMode::None);
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString
///////////////////////////////////////////////////////////////////////////////////////////

UString Sprites3d::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	UString newLinePadding2 = newLinePadding + "  ";
	str += "\n" + newLinePadding2 + "params: {";
	{
		UString newLinePadding3 = newLinePadding2 + "  ";
		str += ",\n" + newLinePadding3 + "clearParam=" + clearParam.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "cameraParam=" + cameraParam.toString(newLinePadding3);
		str += ",\n" + newLinePadding3 + "sprite3dParam=[";
		UString newLinePadding4 = newLinePadding3 + "  ";
		size_t i = 0;
		for (auto& sprite3dParam : sprite3dParams) {
			if (i != 0) {
				str += ",";
			}
			str += "\n" + newLinePadding4 + sprite3dParam.toString(newLinePadding4);
			i++;
		}
		str += "]";
	}
	str += ",\n" + newLinePadding2 + "}";

	str += ",\n" + newLinePadding2 + "state: {";
	{
		UString newLinePadding3 = newLinePadding2 + "  ";
		str += "\n" + newLinePadding3 + "created=";
		if (created == true) {
			str += "true";
		}
		else {
			str += "false";
		}
		str += ",\n" + newLinePadding3 + "camera=" + camera.toString(newLinePadding3);
		UString newLinePadding4 = newLinePadding3 + "  ";
		size_t i = 0;
		for (auto& texture : textures) {
			if (i != 0) {
				str += ",";
			}
			str += "\n" + newLinePadding4;
			if (texture == nullptr) {
				str += "null";
			}
			else {
				str += texture->toString(newLinePadding4);
			}
			i++;
		}
		str += "]";
		str += ",\n" + newLinePadding3 + "sprites=" + camera.toString(newLinePadding3);
		i = 0;
		for (auto& sprite : sprites) {
			if (i != 0) {
				str += ",";
			}
			str += "\n" + newLinePadding4 + sprite.toString(newLinePadding4);
			i++;
		}
		str += "]";
	}
	str += ",\n" + newLinePadding + "}";

	return str;
}


UString Sprites3d::toStringDebug(const UString& newLinePadding) const
{
	UString str("Sprites3d(\n");
	str += toString(newLinePadding);
	str += "\n)";
	return str;
}


} // End namespace Verso

