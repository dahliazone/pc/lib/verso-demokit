#include <Verso/DemoKit/DemoParts/ClearScreen.hpp>
#include <Verso/System/JsonHelperDemoKit.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


ClearFlag clearFlagDefault = ClearFlag::SolidColor;
RgbaColorfKeyframes colorKeyframesDefault = RgbaColorfKeyframes(ColorGenerator::getColor(ColorRgb::Black));


ClearScreen::ClearScreen(const DemoPaths* demoPaths, const JsonPath& demoPartJson,
						 std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) :
	DemoPart(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator),
	clearParam(clearFlagDefault, colorKeyframesDefault),

	created(false)
{
	const JsonPath& params = demoPartJson.getAttribute("params", false);

	clearParam.parseAttributeAsObject(params, "clear", false, ClearParam(clearFlagDefault, colorKeyframesDefault));
}


ClearScreen::ClearScreen(ClearScreen&& original) noexcept :
	DemoPart(std::move(original)),
	clearParam(std::move(original.clearParam)),

	created(std::move(original.created))
{
}

ClearScreen& ClearScreen::operator =(ClearScreen&& original) noexcept
{
	if (this != &original) {
		clearParam = std::move(original.clearParam);

		created = std::move(original.created);
	}
	return *this;
}


ClearScreen::~ClearScreen()
{
	ClearScreen::destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////
void ClearScreen::create(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");

	created = true;

	reset(window);
}


void ClearScreen::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == true, "DemoPart must be created before calling reset()");
}


void ClearScreen::destroy() VERSO_NOEXCEPT
{
	if (ClearScreen::isCreated() == false) {
		return;
	}

	created = false;
}


bool ClearScreen::isCreated() const
{
	return created;
}


void ClearScreen::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window;

	float seconds = static_cast<float>(time.getElapsed().asSeconds());
	Render::clearScreen(clearParam.clearFlag, clearParam.colorKeyframes.getValueInterpolated(seconds));
}


} // End namespace Verso

