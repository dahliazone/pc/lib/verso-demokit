#include <Verso/DemoKit/DemoParts/Text.hpp>
#include <Verso/System/JsonHelperDemoKit.hpp>
#include <Verso/Render/Render.hpp>

namespace Verso {


const ClearParam defaultClearParam;
const DepthParam defaultDepthParam(false, false);
const BlendMode defaultBlendMode = BlendMode::Transcluent;
const UString defaultText("Placeholder text...\nLorem ipsum dolor sit amet,\nconsectetur adipiscing elit.\nPraesent aliquet quam quam,\nut suscipit ante dignissim a.");
const Vector2f defaultRelativePosition(0.5f, 0.5f);
const Vector2f defaultRelativeCharacterSize(0.08f, 0.08f);
const float defaultAlpha = 1.0f;
const Vector2f defaultRelativeSpacing = Vector2f(0.01f, 0.01f);
const RefScale defaultRefScale = RefScale::ViewportSize_KeepAspectRatio_FromX;
const Degree defaultAngleZ = 0.0f;
const float defaultRevealCharactersPerSecond = 0.0f;


Text::Text(
		const DemoPaths* demoPaths, const JsonPath& demoPartJson,
		std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) :
	DemoPart(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator),
	// params
	clearParam(defaultClearParam),
	depthParam(defaultDepthParam),
	blendMode(defaultBlendMode),
	fontParam(),
	text(defaultText),
	relativePositionKeyframes(defaultRelativePosition),
	alphaKeyframes(defaultAlpha),
	relativeCharacterSize(defaultRelativeCharacterSize),
	relativeSpacing(defaultRelativeSpacing),
	refScale(defaultRefScale),
	angleZKeyframes(defaultAngleZ),
	revealCharactersPerSecond(),
	// state
	created(false),
	window(nullptr),
	camera(),
	font(nullptr),
	textToBeRendered("")
{
	const JsonPath& params = demoPartJson.getAttribute("params", false);

	clearParam.parseAttributeAsObject(params, "clear", false, defaultClearParam);

	depthParam.parseAttributeAsObject(params, "depth", false, defaultDepthParam);

	blendMode = JsonHelper::readBlendMode(params, "blend", false, defaultBlendMode);

	fontParam.parseAttributeAsObject(params);

	text = params.readStringOrArrayOfStrings("text", false, defaultText);

	relativePositionKeyframes = params.readVector2fKeyframes("relativePosition", false, Vector2fKeyframes(defaultRelativePosition));

	alphaKeyframes = params.readFloatKeyframes("alpha", false, FloatKeyframes(defaultAlpha));

	relativeCharacterSize = params.readVector2fArrayFormat("relativeCharacterSize", false, defaultRelativeCharacterSize);

	relativeSpacing = params.readVector2fArrayFormat("relativeSpacing", false, defaultRelativeSpacing);

	refScale = params.readRefScale("refScale", false, defaultRefScale);

	angleZKeyframes = params.readFloatKeyframes("angleZ", false, FloatKeyframes(defaultAngleZ));

	revealCharactersPerSecond = params.readNumberf("revealCharactersPerSecond", false, defaultRevealCharactersPerSecond);
}


Text::Text(Text&& original) noexcept :
	DemoPart(std::move(original)),
	// params
	clearParam(std::move(original.clearParam)),
	depthParam(std::move(original.depthParam)),
	blendMode(std::move(original.blendMode)),
	fontParam(std::move(original.fontParam)),
	text(std::move(original.text)),
	relativePositionKeyframes(std::move(original.relativePositionKeyframes)),
	alphaKeyframes(std::move(original.alphaKeyframes)),
	relativeCharacterSize(std::move(original.relativeCharacterSize)),
	relativeSpacing(std::move(original.relativeSpacing)),
	refScale(std::move(original.refScale)),
	angleZKeyframes(std::move(original.angleZKeyframes)),
	revealCharactersPerSecond(std::move(original.revealCharactersPerSecond)),
	// state
	created(std::move(original.created)),
	window(std::move(original.window)),
	camera(std::move(original.camera)),
	font(std::move(original.font)),
	textToBeRendered(std::move(original.textToBeRendered))
{
	original.window = nullptr;
	original.font = nullptr;
}


Text::~Text()
{
	Text::destroy();
}


Text& Text::operator =(Text&& original) noexcept
{
	if (this != &original) {
		// params
		clearParam = std::move(original.clearParam);
		depthParam = std::move(original.depthParam);
		blendMode = std::move(original.blendMode);
		fontParam = std::move(original.fontParam);
		text = std::move(original.text);
		relativePositionKeyframes = std::move(original.relativePositionKeyframes);
		alphaKeyframes = std::move(original.alphaKeyframes);
		relativeCharacterSize = std::move(original.relativeCharacterSize);
		relativeSpacing = std::move(original.relativeSpacing);
		refScale = std::move(original.refScale);
		angleZKeyframes = std::move(original.angleZKeyframes);
		revealCharactersPerSecond = std::move(original.revealCharactersPerSecond);
		// state
		created = std::move(original.created);
		window = std::move(original.window);
		camera = std::move(original.camera);
		font = std::move(original.font);
		textToBeRendered = std::move(original.textToBeRendered);

		original.window = nullptr;
		original.font = nullptr;
	}
	return *this;
}


///////////////////////////////////////////////////////////////////////////////////////////
// IProgramOpengl interface
///////////////////////////////////////////////////////////////////////////////////////////

void Text::create(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");
	this->window = &window;

	// Setup camera
	camera.create(&window, "DemoKit/Text/camera." + getInstanceStr(), false);

	font = new BitmapFontFixedWidth();
	font->createFromFile(
				window, demoPaths->pathFonts() + fontParam.texture.sourceFileName,
				fontParam.texture.minFilter,
				fontParam.texture.magFilter,
				Vector2f(1.0f / 16.0f, 1.0f / 16.0f),
				fontParam.firstFontCharacterIndex,
				fontParam.tabSize);

	created = true;

	reset(window);
}


void Text::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == true, "DemoPart must be created before calling reset()");

	camera.reset(Vector3f(0.0f, 0.0f, 1.0f), Vector3f::up());
	camera.setProjectionOrthographic(Rangef(0.001f, 10.0f));
	camera.setTarget(Vector3f(0.0f, 0.0f, 0.0f));
	camera.setPosition(Vector3f(0.0f, 0.0f, -9.9f));

	if (revealCharactersPerSecond == 0.0f) {
		textToBeRendered = text;
	}
	else {
		textToBeRendered = "";
	}
	font->updateText(textToBeRendered, relativeSpacing, true);
}


void Text::destroy() noexcept
{
	if (Text::isCreated() == false) {
		return;
	}

	camera.destroy();

	delete font;
	font = nullptr;

	created = false;
}


bool Text::isCreated() const
{
	return created;
}


void Text::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	float seconds = static_cast<float>(time.getElapsed().asSeconds());

	if (revealCharactersPerSecond == 0.0f) {
		textToBeRendered = text;
	}
	else {
		int maxSize = static_cast<int>(text.size());
		int revealCharAmount = static_cast<int>(floor(revealCharactersPerSecond * seconds));
		if (revealCharAmount > maxSize) {
			revealCharAmount = maxSize;
		}
		if (revealCharAmount > 0) {
			textToBeRendered = text.substring(0, revealCharAmount);
		}
		else {
			textToBeRendered = "";
		}
	}

	font->updateText(textToBeRendered, relativeSpacing, true);

	Render::clearScreen(
				clearParam.clearFlag,
				clearParam.colorKeyframes.getValueInterpolated(seconds));

	Opengl::depthTest(depthParam.depthTest);
	Opengl::depthWrite(depthParam.depthWrite);

	Opengl::blend(blendMode);
	Opengl::setPolygonRenderMode(PolygonRenderMode::Fill);

	const Vector3f point(relativePositionKeyframes.getValueInterpolated(seconds));

	font->renderText(window, camera,
					 point,
					 relativeCharacterSize,
					 refScale,
					 alphaKeyframes.getValueInterpolated(seconds),
					 angleZKeyframes.getValueInterpolated(seconds));

	Opengl::blend(BlendMode::None);
}


} // End namespace Verso

