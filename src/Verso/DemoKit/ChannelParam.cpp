#include <Verso/DemoKit/ChannelParam.hpp>

namespace Verso {


ChannelParam::ChannelParam() :
	sources()
{
}


ChannelParam::ChannelParam(const ChannelParam& original) :
	sources(original.sources)
{
}


ChannelParam::ChannelParam(const ChannelParam&& original) noexcept :
	sources(std::move(original.sources))
{
}


ChannelParam& ChannelParam::operator =(const ChannelParam& original)
{
	if (this != &original) {
		sources = original.sources;
	}
	return *this;
}


ChannelParam& ChannelParam::operator =(ChannelParam&& original) noexcept
{
	if (this != &original) {
		sources = std::move(original.sources);
	}
	return *this;
}


ChannelParam::~ChannelParam()
= default;


void ChannelParam::parse(const JsonPath& path, bool required) {
	JsonPathArray channelPath(path.asArray("", required ? 1 : -1));

	for (auto& channel : channelPath.array) {
		if (channel.isString("")) {
			sources.emplace_back(channel.value->AsString());
		}
		else {
			UString error("Expected strings in channel array at \"");
			error += path.currentPath;
			error += "\"!";
			VERSO_ILLEGALFORMAT("verso-demokit", error.c_str(), path.fileName.c_str());
		}
	}
}


UString ChannelParam::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str;
	str += "[ ";
	for (auto it = sources.begin(); it != sources.end(); ++it) {
		str += "\"";
		str += (*it);
		if (it != --sources.end())
			str += "\", ";
		else
			str += "\"";
	}
	str += " ]";
	return str;
}


UString ChannelParam::toStringDebug(const UString& newLinePadding) const
{
	UString str("ChannelParam(");
	str += toString(newLinePadding);
	str += "\")";
	return str;
}


} // End namespace Verso

