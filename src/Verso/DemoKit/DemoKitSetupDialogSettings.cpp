#include <Verso/DemoKit/DemoKitSetupDialogSettings.hpp>
#include <Verso/Math/ColorGenerator.hpp>
#include <Verso/System/JsonHelperDemoKit.hpp>

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui.h>
#include <imgui_internal.h>

namespace Verso {


DemoKitSetupDialogSettings::DemoKitSetupDialogSettings() :
	windowTitle(),
	logoFileName(),
	fontFileName(),
	backgroundColor(),
	startButtonText(),
	quitButtonText(),
	style()
{
}


DemoKitSetupDialogSettings::DemoKitSetupDialogSettings(const DemoKitSetupDialogSettings& original) :
	windowTitle(original.windowTitle),
	logoFileName(original.logoFileName),
	fontFileName(original.fontFileName),
	backgroundColor(original.backgroundColor),
	startButtonText(original.startButtonText),
	quitButtonText(original.quitButtonText),
	style(original.style)
{
}


DemoKitSetupDialogSettings::DemoKitSetupDialogSettings(DemoKitSetupDialogSettings&& original) :
	windowTitle(std::move(original.windowTitle)),
	logoFileName(std::move(original.logoFileName)),
	fontFileName(std::move(original.fontFileName)),
	backgroundColor(std::move(original.backgroundColor)),
	startButtonText(std::move(original.startButtonText)),
	quitButtonText(std::move(original.quitButtonText)),
	style(std::move(original.style))
{
	// Clear original
}


DemoKitSetupDialogSettings& DemoKitSetupDialogSettings::operator =(const DemoKitSetupDialogSettings& original)
{
	if (this != &original) {
		windowTitle = original.windowTitle;
		logoFileName = original.logoFileName;
		fontFileName = original.fontFileName;
		backgroundColor = original.backgroundColor;
		startButtonText = original.startButtonText;
		quitButtonText = original.quitButtonText;
		style = original.style;
	}
	return *this;
}


DemoKitSetupDialogSettings& DemoKitSetupDialogSettings::operator =(DemoKitSetupDialogSettings&& original)
{
	if (this != &original) {
		windowTitle = std::move(original.windowTitle);
		logoFileName = std::move(original.logoFileName);
		fontFileName = std::move(original.fontFileName);
		backgroundColor = std::move(original.backgroundColor);
		startButtonText = std::move(original.startButtonText);
		quitButtonText = std::move(original.quitButtonText);
		style = std::move(original.style);

		// Clear original
	}
	return *this;
}


DemoKitSetupDialogSettings::~DemoKitSetupDialogSettings()
= default;


void DemoKitSetupDialogSettings::importJson(const JsonPath& settings)
{
	windowTitle = settings.readString("windowTitle", false, "Demo Setup");
	logoFileName = settings.readString("logo", false, "splash.png");
	fontFileName = settings.readString("font", false, "RobotoCondensed-Regular.ttf");

	style.importJson(settings, "style", false);

	backgroundColor = settings.readColor("backgroundColor", false, ColorGenerator::getColor(ColorRgb::Black));
	startButtonText = settings.readString("startButtonText", false, "Start");
	quitButtonText = settings.readString("quitButtonText", false, "Cancel");
}


JSONValue* DemoKitSetupDialogSettings::exportJson() const
{
	JSONObject obj;

	obj[L"windowTitle"] = JsonPath::writeString(windowTitle);
	obj[L"logo"] = JsonPath::writeString(logoFileName);
	obj[L"font"] = JsonPath::writeString(fontFileName);
	obj[L"backgroundColor"] = JsonPath::writeColor(backgroundColor, false); // discard alpha
	obj[L"startButtonText"] = JsonPath::writeString(startButtonText);
	obj[L"quitButtonText"] = JsonPath::writeString(quitButtonText);
	obj[L"style"] = style.exportJson();

	return new JSONValue(obj);
}


UString DemoKitSetupDialogSettings::toString(const UString& newLinePadding) const
{
	UString str("{");
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += "\n" + newLinePadding2 + "\"windowTitle\": \"" + windowTitle + "\"";
		str += "\n" + newLinePadding2 + "\"logoFileName\": \"" + logoFileName + "\"";
		str += "\n" + newLinePadding2 + "\"fontFileName\": \"" + fontFileName + "\"";
		str += "\n" + newLinePadding2 + "\"backgroundColor\": \"" + backgroundColor.toString() + "\"";
		str += "\n" + newLinePadding2 + "\"startButtonText\": \"" + startButtonText + "\"";
		str += "\n" + newLinePadding2 + "\"quitButtonText\": \"" + quitButtonText + "\"";
	}
	str += "\n" + newLinePadding + "}";

	return str;
}


UString DemoKitSetupDialogSettings::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::DemoKitSetupDialogSettings(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso


