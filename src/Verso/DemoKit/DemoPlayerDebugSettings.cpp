#include <Verso/DemoKit/DemoPlayerDebugSettings.hpp>

namespace Verso {


const PlayMode defaultPlayMode = PlayMode::Development;
const DebugMode defaultDebugMode = DebugMode::Editor;
const float defaultFontSizeSmaller = 18.0f;
const float defaultFontSizeBigger = 28.0f;
const bool defaultPlaybackRangeEnabled = false;
const Ranged defaultPlaybackRange(0.0, 0.0);
const bool defaultMidiEnabled = false;


DemoPlayerDebugSettings::DemoPlayerDebugSettings() :
	playMode(defaultPlayMode),
	debugMode(defaultDebugMode),
	fontFileName(),
	fontSizeSmaller(defaultFontSizeSmaller),
	fontSizeBigger(defaultFontSizeBigger),
	playbackRangeEnabled(defaultPlaybackRangeEnabled),
	playbackRange(defaultPlaybackRange),
    midiEnabled(defaultMidiEnabled)
{
}


DemoPlayerDebugSettings::DemoPlayerDebugSettings(const DemoPlayerDebugSettings& original) :
	playMode(original.playMode),
	debugMode(original.debugMode),
	fontFileName(original.fontFileName),
	playbackRangeEnabled(original.playbackRangeEnabled),
	playbackRange(original.playbackRange),
    midiEnabled(original.midiEnabled)
{
}


DemoPlayerDebugSettings::DemoPlayerDebugSettings(DemoPlayerDebugSettings&& original) :
	playMode(std::move(original.playMode)),
	debugMode(std::move(original.debugMode)),
	fontFileName(std::move(original.fontFileName)),
	playbackRangeEnabled(std::move(original.playbackRangeEnabled)),
	playbackRange(std::move(original.playbackRange)),
    midiEnabled(std::move(original.midiEnabled))
{
}


DemoPlayerDebugSettings& DemoPlayerDebugSettings::operator =(const DemoPlayerDebugSettings& original)
{
	if (this != &original) {
		playMode = original.playMode;
		debugMode = original.debugMode;
		fontFileName = original.fontFileName;
		playbackRangeEnabled = original.playbackRangeEnabled;
		playbackRange = original.playbackRange;
        midiEnabled = original.midiEnabled;
	}
	return *this;
}


DemoPlayerDebugSettings& DemoPlayerDebugSettings::operator =(DemoPlayerDebugSettings&& original)
{
	if (this != &original) {
		playMode = std::move(original.playMode);
		debugMode = std::move(original.debugMode);
		fontFileName = std::move(original.fontFileName);
		playbackRangeEnabled = std::move(original.playbackRangeEnabled);
		playbackRange = std::move(original.playbackRange);
        midiEnabled = std::move(original.midiEnabled);

		// Clear the original
	}
	return *this;
}


DemoPlayerDebugSettings::~DemoPlayerDebugSettings()
= default;


void DemoPlayerDebugSettings::importJson(const JsonPath& parent)
{
	playMode = JsonHelper::readPlayMode(parent, "playmode", false, defaultPlayMode);

	debugMode = JsonHelper::readDebugMode(parent, "debugMode", false, defaultDebugMode);

	fontFileName = parent.readString("font", false, "RobotoCondensed-Regular.ttf");

	fontSizeSmaller = parent.readNumberf("fontSizeSmaller", false, defaultFontSizeSmaller);

	fontSizeBigger = parent.readNumberf("fontSizeBigger", false, defaultFontSizeBigger);

	playbackRange = parent.readRanged("playbackRange", false, defaultPlaybackRange);

	playbackRangeEnabled = defaultPlaybackRangeEnabled;
	if ((playbackRange.minValue != 0.0 || playbackRange.maxValue != 0.0) && playbackRange.minValue < playbackRange.maxValue) {
		playbackRangeEnabled = true;
	}

    midiEnabled = parent.readBool("midiEnabled", false, defaultMidiEnabled);
}


JSONValue* DemoPlayerDebugSettings::exportJson() const
{
	JSONObject obj;

	obj[L"playMode"] = JsonPath::writeString(playModeToString(playMode));
	obj[L"debugMode"] = JsonPath::writeString(debugModeToString(debugMode));
	obj[L"font"] = JsonPath::writeString(fontFileName);
	obj[L"fontSizeSmaller"] = JsonPath::writeNumberf(fontSizeSmaller);
	obj[L"fontSizeBigger"] = JsonPath::writeNumberf(fontSizeBigger);
	obj[L"playbackRange"] = JsonPath::writeRanged(playbackRange);
    obj[L"midiEnabled"] = JsonPath::writeBool(midiEnabled);

	return new JSONValue(obj);
}


UString DemoPlayerDebugSettings::toString(const UString& newLinePadding) const
{
	UString str("{ ");
	str += R"("playMode": ")" + playModeToString(playMode) + "\", ";
	str += R"("debugMode": ")" + debugModeToString(debugMode) + "\"";
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += ",\n" + newLinePadding2 + R"("fontFileName": ")" + fontFileName + "\"";
		str += ",\n" + newLinePadding2 + "\"fontSizeSmaller\": ";
		str.append2(fontSizeSmaller);
		str += ",\n" + newLinePadding2 + "\"fontSizeBigger\": ";
		str.append2(fontSizeBigger);
		str += ",\n" + newLinePadding2 + "\"playbackRangeEnabled\": ";
		if (playbackRangeEnabled) {
			str += "true";
		}
		else {
			str += "false";
		}
		str += ",\n" + newLinePadding2 + R"("playbackRange": ")" + playbackRange.toString() + "\"";
        str += ",\n" + newLinePadding2 + "\"midiEnabled\": ";
        if (midiEnabled) {
            str += "true";
        }
        else {
            str += "false";
        }
	}
	str += "\n" + newLinePadding + "}";

	return str;
}


UString DemoPlayerDebugSettings::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::DemoPlayerDebugSettings(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso
