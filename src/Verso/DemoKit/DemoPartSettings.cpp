#include <Verso/DemoKit/DemoPartSettings.hpp>

namespace Verso {


DemoPartSettings::DemoPartSettings(const JsonPath& demoPartJson, std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) :
	name(),
	type(),
	start(0),
	duration(0),
	priority(0),
	subsequentPriority(0), // needs to be set by using updateSubsequentPriority()
	duplicateLevel(0), // needs to be set by using updateSubsequentPriority()
	jsonOrder(jsonOrder),
	paramsJsonValue()
{
	importJson(demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
}


DemoPartSettings::DemoPartSettings(const UString& name, const UString& type,
	double start, double duration,
	std::int32_t priority, std::int32_t jsonOrder,
	const JSONValue& paramsJsonValue) :
	name(name),
	type(type),
	start(start),
	duration(duration),
	priority(priority),
	subsequentPriority(0), // needs to be set by using updateSubsequentPriority()
	duplicateLevel(0), // needs to be set by using updateSubsequentPriority()
	jsonOrder(jsonOrder),
	paramsJsonValue(paramsJsonValue)
{
}


DemoPartSettings::DemoPartSettings(DemoPartSettings&& original) :
	name(std::move(original.name)),
	type(std::move(original.type)),
	start(std::move(original.start)),
	duration(std::move(original.duration)),
	priority(std::move(original.priority)),
	subsequentPriority(std::move(original.subsequentPriority)),
	duplicateLevel(std::move(original.duplicateLevel)),
	jsonOrder(std::move(original.jsonOrder)),
	paramsJsonValue(std::move(original.paramsJsonValue))
{
}


DemoPartSettings& DemoPartSettings::operator =(DemoPartSettings&& original)
{
	if (this != &original) {
		name = std::move(original.name);
		type = std::move(original.type);
		start = std::move(original.start);
		duration = std::move(original.duration);
		priority = std::move(original.priority);
		subsequentPriority = std::move(original.subsequentPriority);
		duplicateLevel = std::move(original.duplicateLevel);
		jsonOrder = std::move(original.jsonOrder);
		paramsJsonValue = std::move(original.paramsJsonValue);

		// Clear the original
	}
	return *this;
}


DemoPartSettings::~DemoPartSettings()
{
}


void DemoPartSettings::importJson(const JsonPath& demoPartJson,
								  std::int32_t jsonOrder,
								  double startAccumulator,
								  int priorityAccumulator)
{
	this->name = demoPartJson.readString("name", false, "");
	this->type = demoPartJson.readString("type", true);
	this->start = demoPartJson.readNumberd("start", false, 0.0f) + startAccumulator;
	this->duration = demoPartJson.readNumberd("duration", false, 9999999.0f);
	this->priority = demoPartJson.readInt("priority", false, 0) + priorityAccumulator;
	this->jsonOrder = jsonOrder;

	if (demoPartJson.isObject("params")) {
		this->paramsJsonValue = JSONValue(demoPartJson.readValue("params"));
	}
	else {
		this->paramsJsonValue = JSONValue();
	}
}


JSONValue* DemoPartSettings::exportJson() const
{
	JSONObject obj;
	obj[L"name"] = new JSONValue(name.toUtf8Wstring());
	obj[L"type"] = new JSONValue(type.toUtf8Wstring());
	obj[L"start"] = new JSONValue(start);
	obj[L"duration"] = new JSONValue(duration);
	obj[L"priority"] = new JSONValue(priority);
	//obj[L"jsonOrder"] = new JSONValue(jsonOrder);
	obj[L"params"] = new JSONValue(paramsJsonValue);

	return new JSONValue(obj);
}


UString DemoPartSettings::toString(const UString& newLinePadding) const
{
	(void)newLinePadding;

	UString str("{ name=\"");
	str += name;
	str += ", type=\"";
	str += type;
	str += ",\n  start=";
	str.append2(start);
	str += ", duration=";
	str.append2(duration);
	str += ", priority=";
	str.append2(priority);
	str += ", jsonOrder=";
	str.append2(jsonOrder);
	str += ", params=";
	str += paramsJsonValue.Stringify(false);
	str += " }";
	return str;
}


UString DemoPartSettings::toStringDebug(const UString& newLinePadding) const
{
	UString str("DemoPartSettings(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

