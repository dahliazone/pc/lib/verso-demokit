#include <Verso/DemoKit/DemoPart.hpp>
#include <iomanip>

namespace Verso {


size_t DemoPart::nextInstanceId = 1;


DemoPart::DemoPart(
		const DemoPaths* demoPaths, const JsonPath& demoPartJson,
		std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) :
	demoPaths(demoPaths),
	instanceId(nextInstanceId),
	settings(new DemoPartSettings(demoPartJson, jsonOrder, startAccumulator, priorityAccumulator)),
	elapsedSeconds(0.0),
	surplusSeconds(0.0),
	accumulatorSeconds(0.0),
	incrementalRun(false),
	runMaxDeltaSeconds(0.0),
	incrementalRender(false),
	renderMaxDeltaSeconds(0.0)
{
	nextInstanceId++;
}


DemoPart::DemoPart(DemoPart&& original) :
	demoPaths(std::move(original.demoPaths)),
	instanceId(std::move(original.instanceId)),
	settings(std::move(original.settings)),
	elapsedSeconds(std::move(original.elapsedSeconds)),
	surplusSeconds(std::move(original.surplusSeconds)),
	accumulatorSeconds(std::move(original.accumulatorSeconds)),
	incrementalRun(std::move(original.incrementalRun)),
	runMaxDeltaSeconds(std::move(original.runMaxDeltaSeconds)),
	incrementalRender(std::move(original.incrementalRender)),
	renderMaxDeltaSeconds(std::move(original.renderMaxDeltaSeconds))
	//nextRenderDeltaSeconds(std::move(original.nextRenderDeltaSeconds)),
	//nextRenderSecondsElapsedDemo(std::move(original.nextRenderSecondsElapsedDemo))
{
	//UString message("move constructor(): ");
	//if (settings != nullptr)
	//	message += settings->name;
	//else
	//	message += "nullptr";
	//VERSO_LOG_DEBUG("verso-demokit", message.c_str());

	original.settings = nullptr;
	original.instanceId = 0;
}


DemoPart::~DemoPart()
{
	//UString message("move destructor(): ");
	//if (settings != nullptr)
	//	message += settings->name;
	//else
	//	message += "nullptr";
	//VERSO_LOG_DEBUG("verso-demokit", message.c_str());

	if (settings != nullptr) {
		delete settings;
		settings = nullptr;
	}
}


DemoPart& DemoPart::operator =(DemoPart&& original)
{
	//UString message("operator =: ");
	//if (settings != nullptr)
	//	message += settings->name;
	//else
	//	message += "nullptr";
	//VERSO_LOG_DEBUG("verso-demokit", message.c_str());

	if (this != &original) {
		demoPaths = std::move(original.demoPaths);
		instanceId = std::move(original.instanceId);
		settings = std::move(original.settings);
		elapsedSeconds = std::move(original.elapsedSeconds);
		surplusSeconds = std::move(original.surplusSeconds);
		accumulatorSeconds = std::move(original.accumulatorSeconds);
		incrementalRun = std::move(original.incrementalRun);
		runMaxDeltaSeconds = std::move(original.runMaxDeltaSeconds);
		incrementalRender = std::move(original.incrementalRender);
		renderMaxDeltaSeconds = std::move(original.renderMaxDeltaSeconds);
		//nextRenderDeltaSeconds = std::move(original.nextRenderDeltaSeconds);
		//nextRenderSecondsElapsedDemo = std::move(original.nextRenderSecondsElapsedDemo);

		original.settings = nullptr;
	}
	return *this;
}


void DemoPart::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window; (void)time;
	// by default you don't need to implement this unless you want to process input before render()
}


void DemoPart::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
	(void)window; (void)time; (void)event;
	// by default you don't need to implement this unless you want to process events before render()
}


bool DemoPart::isQuitting() const
{
	// not supported for demoparts
	return false;
}


void DemoPart::quit()
{
	// not supported for demoparts
}


void DemoPart::update(IWindowOpengl& window, const FrameTimestamp& time) //double deltaSeconds, double secondsElapsed)
{
	double newDeltaSeconds = time.getDt().asSeconds();

	double newSecondsPassedDemo = time.getElapsed().asSeconds() - time.getDt().asSeconds();
	if (surplusSeconds != 0.0) {
		newDeltaSeconds = surplusSeconds;
	}

	if (isIncrementalRun() == false) {
		elapsedSeconds += newDeltaSeconds;

		FrameTimestamp newTime(time.getUpdateFrame(),
							   time.getRenderFrame(),
							   Timestamp::seconds(newDeltaSeconds),
							   Timestamp::seconds(elapsedSeconds),
							   time.getTargetDt(),
							   time.getLastAverageDt());

		handleInput(window, newTime);
		//update3(newDeltaSeconds, elapsedSeconds, newSecondsPassedDemo+newDeltaSeconds);
	}

	// surplusSeconds = oma juttunsa
	// accumulator = mahdollistaa tasa-FPS:n
	//
	// if (tasa-fps saavutettu) {
	// 	getInput();
	// 	calculatePhysics(time.getDt().asSeconds(), elapsedSeconds);
	// }
	//
	// Render(time.getDt().asSeconds(), elapsedSeconds)
	//
	// --------
	//
	// |-----|-----|-----|-----|
	// C1    C2  +
	//
	// C2 + speed*interpolation
	//
	// "Absolute"Object (only x,y,z-vector)
	// - no need for interpolation. location will be calculated on render
	// "Incremental"Object (x,y,z-vector delta/speed value vector)
	// - interpolation is calculated using delta/speed values
	//
	// nykynen bugi: koska demopartin ajoaika ei ole step:n monikerta j
	//   viimeinen <step arvo kummittelemaan eik tmn vuoksi demopartti edes lopu

	else {
		VERSO_LOG_DEBUG("verso-demokit", "###"<<getName()<<"#############("<<time.getDt().asSeconds()<<")#################################");

		//VERSO_LOG_DEBUG("verso-demokit", "   Added " << accumulatorSeconds << "s to deltaSeconds, so it's now " << time.getDt().asSeconds() + accumulatorSeconds);
		newDeltaSeconds += accumulatorSeconds;

		while (newDeltaSeconds >= runMaxDeltaSeconds) {
			//VERSO_LOG_DEBUG("verso-demokit", "### 1X " << runMaxDeltaSeconds << "s ");
			elapsedSeconds += runMaxDeltaSeconds;
			newSecondsPassedDemo += runMaxDeltaSeconds;
			///VERSO_LOG_DEBUG("verso-demokit", "##### " << runMaxDeltaSeconds << ", " << elapsedSeconds << ", " << newSecondsPassedDemo);

			FrameTimestamp newTime(time.getUpdateFrame(),
								   time.getRenderFrame(),
								   Timestamp::seconds(runMaxDeltaSeconds),
								   Timestamp::seconds(elapsedSeconds),
								   time.getTargetDt(),
								   time.getLastAverageDt());

			handleInput(window, newTime);
			//update3(runMaxDeltaSeconds, elapsedSeconds, newSecondsPassedDemo);
			newDeltaSeconds -= runMaxDeltaSeconds;
		}

		//VERSO_LOG_DEBUG("verso-demokit", "   Put " << newDeltaSeconds << "s to accumulatorSeconds");
		accumulatorSeconds = newDeltaSeconds;
//		elapsedSeconds += newDeltaSeconds;
//		newSecondsPassedDemo += newDeltaSeconds;
//		VERSO_LOG_DEBUG("verso-demokit", "##### " << newDeltaSeconds << ", " << elapsedSeconds << ", " << newSecondsPassedDemo);
//		update(newDeltaSeconds, elapsedSeconds, newSecondsPassedDemo);
	}

	if (surplusSeconds == 0.0) {
		//nextRenderDeltaSeconds = time.getDt().asSeconds();
		//nextRenderSecondsElapsedDemo = secondsElapsedDemo;
	}
	else { // surplusSeconds != 0.0f
		//nextRenderDeltaSeconds = surplusSeconds;
		//nextRenderSecondsElapsedDemo = secondsElapsedDemo - time.getDt().asSeconds() + surplusSeconds;
		surplusSeconds = 0.0;
	}
}


DemoPartSettings& DemoPart::getSettings() const
{
	if (settings == nullptr) {
		VERSO_ERROR("verso-demokit", "Internal variable 'settings' isn't initialized properly!", "");
	}
	return *settings;
}


bool DemoPart::hasStarted(double secondsElapsedDemo) const
{
	if (secondsElapsedDemo >= settings->start) {
		return true;
	}
	else {
		return false;
	}
}


bool DemoPart::hasEnded(double secondsElapsedDemo) const
{
	if (secondsElapsedDemo >= settings->start+settings->duration) {
		return true;
	}
	else {
		return false;
	}
}


bool DemoPart::isActive(double secondsElapsedDemo) const
{
	if (secondsElapsedDemo >= settings->start &&
			secondsElapsedDemo < settings->start+settings->duration) {
		return true;
	}
	else {
		return false;
	}
}


bool DemoPart::collidesWithPriority(DemoPart* demoPart) const
{
	const DemoPartSettings* a = settings;
	const DemoPartSettings* b = demoPart->settings;
	if (a->priority != b->priority) {
		return false;
	}

	return collidesWithoutPriority(demoPart);
}


bool DemoPart::collidesWithoutPriority(DemoPart* demoPart) const
{
	const DemoPartSettings* a = settings;
	const DemoPartSettings* b = demoPart->settings;

	const double aStart = a->start;
	const double aEnd = a->start + a->duration;
	const double bStart = b->start;
	const double bEnd = b->start + b->duration;

	// A starts inside B
	if (aStart > bStart && aStart < bEnd) {
		return true;
	}

	// A ends inside B
	if (aEnd > bStart && aEnd < bEnd) {
		return true;
	}

	// B starts inside A
	if (bStart > aStart && bStart < aEnd) {
		return true;
	}

	// B ends inside A
	if (bEnd > aStart && bEnd < aEnd) {
		return true;
	}

	return false;
}


size_t DemoPart::getInstanceId() const
{
	return instanceId;
}


UString DemoPart::getInstanceStr() const
{
	std::stringstream ss;
	ss << std::setfill('0') << std::setw(5) << instanceId;
	return ss.str();
}


UString DemoPart::getName() const
{
	return settings->name;
}


double DemoPart::getStartSeconds() const
{
	return settings->start;
}


double DemoPart::getEndSeconds() const
{
	return settings->start + settings->duration;
}


double DemoPart::getDurationSeconds() const
{
	return settings->duration;
}


double DemoPart::getElapsedSeconds() const
{
	return elapsedSeconds;
}


void DemoPart::setElapsedSeconds(double elapsedSeconds)
{
	// Reset surplus time
	surplusSeconds = 0.0;

	// Reset new elapsed time
	this->elapsedSeconds = elapsedSeconds;
}


void DemoPart::addSurplusSeconds(double surplusSeconds)
{
	this->surplusSeconds += surplusSeconds;
}


bool DemoPart::isIncrementalRun() const
{
	return incrementalRun;
}


bool DemoPart::isIncrementalRender() const
{
	return incrementalRender;
}

void DemoPart::enableIncrementalRun(double minFramesPerSecond, double maxDeltaSeconds)
{
	if (minFramesPerSecond > 0.0) {
		incrementalRun = true;
		runMaxDeltaSeconds = 1.0 / minFramesPerSecond;
	}

	else if (maxDeltaSeconds > 0.0) {
		incrementalRun = true;
		runMaxDeltaSeconds = maxDeltaSeconds;
	}
}


void DemoPart::disableIncrementalRun()
{
	incrementalRun = false;
	runMaxDeltaSeconds = 0.0;
}


void DemoPart::enableIncrementalRender(double minFramesPerSecond, double maxDeltaSeconds)
{
	if (minFramesPerSecond > 0.0) {
		incrementalRender = true;
		renderMaxDeltaSeconds = 1.0 / minFramesPerSecond;
	}

	else if (maxDeltaSeconds > 0.0) {
		incrementalRender = true;
		renderMaxDeltaSeconds = maxDeltaSeconds;
	}
}


void DemoPart::disableIncrementalRender()
{
	incrementalRender = false;
	renderMaxDeltaSeconds = 0.0;
}


///////////////////////////////////////////////////////////////////////////////////////////
// comparison
///////////////////////////////////////////////////////////////////////////////////////////

bool DemoPart::operator < (const DemoPart& rhs) const
{
	return settings->start < rhs.settings->start;
}


bool DemoPart::operator <= (const DemoPart& rhs) const
{
	return settings->start <= rhs.settings->start;
}


bool DemoPart::operator > (const DemoPart& rhs) const
{
	return settings->start > rhs.settings->start;
}


bool DemoPart::operator >= (const DemoPart& rhs) const
{
	return settings->start >= rhs.settings->start;
}


///////////////////////////////////////////////////////////////////////////////////////////
// toString & hash
///////////////////////////////////////////////////////////////////////////////////////////

UString DemoPart::toString(const UString& newLinePadding) const
{
	if (settings != nullptr) {
		return settings->toString(newLinePadding);
	}
	else {
		return "{ }";
	}
}


UString DemoPart::toStringDebug(const UString& newLinePadding) const
{
	UString str("DemoPart(");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

