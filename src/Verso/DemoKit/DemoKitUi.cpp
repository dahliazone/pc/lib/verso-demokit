#include <Verso/DemoKit/DemoKitUi.hpp>
#include <Verso/Gui/ImGuiVersoImpl.hpp>
#include <Verso/Input/InputController.hpp>
#include <Verso/Input/InputManager.hpp>
#include <Verso/Gui/ImGuiStyleVerso.hpp>
#include <Verso/Gui/ImGuiTimeLine.hpp>
#include <Verso/Render/RenderStats.hpp>
#include <Verso/Render/Render.hpp>
#include <Verso/Math/RefScale.hpp>

namespace Verso {


DemoKitUi::DemoKitUi(IDemoKitApp& demoKitApp, DemoPlayer& demoPlayer) :
	created(false),
	imgui(),
	inputController(nullptr),
	demoKitApp(demoKitApp),
	demoPlayer(demoPlayer),
	smallerFont(nullptr),
	biggerFont(nullptr),
	icons(),
	selectedIconSet(),
	editorBgColor(),
	midi(),
	a1(0),
	a2(0),
	a3(0),
	a4(0),
	buttonPlayPause(0),
	buttonRewindToStart(0),
	buttonToggleFullscreen(0),
	buttonToggleEditor(0),
	b1(0),
	b2(0),
	b3(0),
	b4(0),
	b5(0),
	b6(0),
	b7(0),
	b8(0),
	b9(0),
	b10(0),
	b11(0),
	b12(0),
	b13(0),
	style(),

	// state
	enableUi(true),
	viewFullscreen2(true),
	viewPanelsToggle(true),
	viewMainControls(true),
	viewLeftPanel(false),
	viewRightPanel(true),
	viewHorizontalPanel(true),
	viewImGuiDemoWindow(false),
	viewTestWindow(true),
	mainMenuHeight(0),
	selectedDemoPart(nullptr),
	renderTexture(nullptr)
{
}


DemoKitUi::DemoKitUi(DemoKitUi&& original) noexcept :
	created(std::move(original.created)),
	imgui(std::move(original.imgui)),
	inputController(std::move(original.inputController)),
	demoKitApp(original.demoKitApp),
	demoPlayer(original.demoPlayer),
	smallerFont(std::move(original.smallerFont)),
	biggerFont(std::move(original.biggerFont)),
	icons(std::move(original.icons)),
	selectedIconSet(std::move(original.selectedIconSet)),
	editorBgColor(std::move(original.editorBgColor)),
	midi(std::move(original.midi)),
	a1(std::move(original.a1)),
	a2(std::move(original.a2)),
	a3(std::move(original.a3)),
	a4(std::move(original.a4)),
	buttonPlayPause(std::move(original.buttonPlayPause)),
	buttonRewindToStart(std::move(original.buttonRewindToStart)),
	buttonToggleFullscreen(std::move(original.buttonToggleFullscreen)),
	buttonToggleEditor(std::move(original.buttonToggleEditor)),
	b1(std::move(original.b1)),
	b2(std::move(original.b2)),
	b3(std::move(original.b3)),
	b4(std::move(original.b4)),
	b5(std::move(original.b5)),
	b6(std::move(original.b6)),
	b7(std::move(original.b7)),
	b8(std::move(original.b8)),
	b9(std::move(original.b9)),
	b10(std::move(original.b10)),
	b11(std::move(original.b11)),
	b12(std::move(original.b12)),
	b13(std::move(original.b13)),
	style(std::move(original.style)),

	// state
	enableUi(std::move(original.enableUi)),
	viewFullscreen2(std::move(original.viewFullscreen2)),
	viewPanelsToggle(std::move(original.viewPanelsToggle)),
	viewMainControls(std::move(original.viewMainControls)),
	viewLeftPanel(std::move(original.viewLeftPanel)),
	viewRightPanel(std::move(original.viewRightPanel)),
	viewHorizontalPanel(std::move(original.viewHorizontalPanel)),
	viewImGuiDemoWindow(std::move(original.viewImGuiDemoWindow)),
	viewTestWindow(std::move(original.viewTestWindow)),
	mainMenuHeight(std::move(original.mainMenuHeight)),
	selectedDemoPart(std::move(original.selectedDemoPart)),
	renderTexture(std::move(original.renderTexture))
{
	// Clear the original
	original.created = false;
	original.inputController = nullptr;
	original.smallerFont = nullptr;
	original.biggerFont = nullptr;
	original.selectedDemoPart = nullptr;
	original.renderTexture = nullptr;
}


DemoKitUi& DemoKitUi::operator =(DemoKitUi&& original) noexcept
{
	if (this != &original) {
		created = std::move(original.created);
		imgui = std::move(original.imgui);
		inputController = std::move(original.inputController);
		demoKitApp = std::move(original.demoKitApp);
		demoPlayer = std::move(original.demoPlayer);
		smallerFont = std::move(original.smallerFont);
		biggerFont = std::move(original.biggerFont);
		icons = std::move(original.icons);
		selectedIconSet = std::move(original.selectedIconSet);
		editorBgColor = std::move(original.editorBgColor);
		midi = std::move(original.midi);
		a1 = std::move(original.a1);
		a2 = std::move(original.a2);
		a3 = std::move(original.a3);
		a4 = std::move(original.a4);
		buttonPlayPause = std::move(original.buttonPlayPause);
		buttonRewindToStart = std::move(original.buttonRewindToStart);
		buttonToggleFullscreen = std::move(original.buttonToggleFullscreen);
		buttonToggleEditor = std::move(original.buttonToggleEditor);
		b1 = std::move(original.b1);
		b2 = std::move(original.b2);
		b3 = std::move(original.b3);
		b4 = std::move(original.b4);
		b5 = std::move(original.b5);
		b6 = std::move(original.b6);
		b7 = std::move(original.b7);
		b8 = std::move(original.b8);
		b9 = std::move(original.b9);
		b10 = std::move(original.b10);
		b11 = std::move(original.b11);
		b12 = std::move(original.b12);
		b13 = std::move(original.b13);
		style = std::move(original.style);

		// state
		enableUi = std::move(original.enableUi);
		viewFullscreen2 = std::move(original.viewFullscreen2);
		viewPanelsToggle = std::move(original.viewPanelsToggle);
		viewMainControls = std::move(original.viewMainControls);
		viewLeftPanel = std::move(original.viewLeftPanel);
		viewRightPanel = std::move(original.viewRightPanel);
		viewHorizontalPanel = std::move(original.viewHorizontalPanel);
		viewImGuiDemoWindow = std::move(original.viewImGuiDemoWindow);
		viewTestWindow = std::move(original.viewTestWindow);
		mainMenuHeight = std::move(original.mainMenuHeight);
		selectedDemoPart = std::move(original.selectedDemoPart);
		renderTexture = std::move(original.renderTexture);

		// Clear the original
		original.created = false;
		original.inputController = nullptr;
		original.smallerFont = nullptr;
		original.biggerFont = nullptr;
		original.selectedDemoPart = nullptr;
		original.renderTexture = nullptr;
	}
	return *this;
}


DemoKitUi::~DemoKitUi()
{
	destroy();
}


bool DemoKitUi::getEnableUi() const
{
	return enableUi;
}


void DemoKitUi::updateRenderTexture(Texture* renderTexture)
{
	this->renderTexture = renderTexture;
}


void DemoKitUi::create(IWindowOpengl& window)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");

	// Setup ImGui binding
	imgui.create(window);
	ImGui::SetCurrentContext(imgui.getContext());

	UString ttfFontFileName(demoPlayer.settings.demoPaths.pathFonts()+demoPlayer.settings.debug.fontFileName);
	ImGuiIO& io = ImGui::GetIO();
	smallerFont = io.Fonts->AddFontFromFileTTF(ttfFontFileName.c_str(), demoPlayer.settings.debug.fontSizeSmaller);
	IM_ASSERT(smallerFont != NULL);
	biggerFont = io.Fonts->AddFontFromFileTTF(ttfFontFileName.c_str(), demoPlayer.settings.debug.fontSizeBigger);
	IM_ASSERT(biggerFont != NULL);

	if (demoPlayer.settings.debug.midiEnabled) {
		midi.printMidiDevices();
		midi.createAll();
		midi.printInitializedInputDevices();
		midi.printInitializedOutputDevices();
	}

	InputManager::instance().enableJoysticksAndGameControllers();
	VERSO_LOG_INFO("verso-demokit", InputManager::instance().getJoystickInfo());
	inputController = InputManager::instance().createInputController("joystick");

	buttonPlayPause = inputController->mapKeyboardToButton("playPause", KeyCode::Space);
	buttonRewindToStart = inputController->mapKeyboardToButton("rewindToStart", KeyCode::Backspace);
	buttonToggleFullscreen = inputController->mapKeyboardToButton("toggleFullscreen", KeyCode::F11);
	buttonToggleEditor = inputController->mapKeyboardToButton("toggleEditor", KeyCode::F12);

	a1 = inputController->mapJoystickAxisToAxis("a1", JoystickAxis::Axis1);
	a2 = inputController->mapJoystickAxisToAxis("a2", JoystickAxis::Axis2);
	a3 = inputController->mapJoystickAxisToAxis("a3", JoystickAxis::Axis3);
	a4 = inputController->mapJoystickAxisToAxis("a4", JoystickAxis::Axis4);
	b1 = inputController->mapJoystickButtonToButton("b1", JoystickButton::Button1);
	b2 = inputController->mapJoystickButtonToButton("b2", JoystickButton::Button2);
	b3 = inputController->mapJoystickButtonToButton("b3", JoystickButton::Button3);
	b4 = inputController->mapJoystickButtonToButton("b4", JoystickButton::Button4);
	b5 = inputController->mapJoystickButtonToButton("b5", JoystickButton::Button5);
	b6 = inputController->mapJoystickButtonToButton("b6", JoystickButton::Button6);
	b7 = inputController->mapJoystickButtonToButton("b7", JoystickButton::Button7);
	b8 = inputController->mapJoystickButtonToButton("b8", JoystickButton::Button8);
	b9 = inputController->mapJoystickButtonToButton("b9", JoystickButton::Button9);
	b10 = inputController->mapJoystickButtonToButton("b10", JoystickButton::Button10);
	b11 = inputController->mapJoystickButtonToButton("b11", JoystickButton::Button11);
	b12 = inputController->mapJoystickButtonToButton("b12", JoystickButton::Button12);
	b13 = inputController->mapJoystickButtonToButton("b13", JoystickButton::Button13);

	// Load icons
	icons.loadTextureCssJson(window, demoPlayer.settings.demoPaths.pathGuiIcons()+"icon-av-grey600.json");
	icons.loadTextureCssJson(window, demoPlayer.settings.demoPaths.pathGuiIcons()+"icon-av-white.json");
	icons.loadTextureCssJson(window, demoPlayer.settings.demoPaths.pathGuiIcons()+"icon-image-grey600.json");
	icons.loadTextureCssJson(window, demoPlayer.settings.demoPaths.pathGuiIcons()+"icon-image-white.json");
	selectedIconSet = "white";
	//VERSO_LOG_DEBUG("verso-demokit", icons.toString());

	editorBgColor = ColorGenerator::getColor(ColorRgb::BlenderBackground);

	//style.loadFromFile(demoPlayer.settings.demoPaths.pathGuiStyles()+"blue-and-black.json");
	style.loadFromFile(demoPlayer.settings.demoPaths.pathGuiStyles()+"blue-and-white.json");

	created = true;

	reset(window);
}


void DemoKitUi::reset(IWindowOpengl& window)
{
	(void)window;
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == true, "DemoPart must be created before calling reset()");

	// Defaults
	enableUi = (demoPlayer.settings.debug.playMode == PlayMode::Development && demoPlayer.settings.debug.debugMode != DebugMode::Fullscreen);
	if (demoPlayer.settings.debug.playMode == PlayMode::Development) {
		window.setCursorShown(true);
	}
	else {
		window.setCursorShown(enableUi);
	}

	viewFullscreen2 = true;
	viewPanelsToggle = (demoPlayer.settings.debug.debugMode == DebugMode::Editor);
	viewMainControls = true;
	viewLeftPanel = false;
	viewRightPanel = true;
	viewHorizontalPanel = true;
	viewImGuiDemoWindow = false;
	viewTestWindow = true;
	mainMenuHeight = 0;
	selectedDemoPart = nullptr;
	renderTexture = nullptr;
}


void DemoKitUi::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	inputController = nullptr; // managed by InputController
	smallerFont = nullptr; // managed by IMGUI
	biggerFont = nullptr; // managed by IMGUI

	icons.destroy();

    if (demoPlayer.settings.debug.midiEnabled) {
		midi.destroy();
	}

	selectedDemoPart = nullptr;

	renderTexture = nullptr;

	imgui.destroy();

	created = false;
}


bool DemoKitUi::isCreated() const
{
	return created;
}


void DemoKitUi::handleInput(IWindowOpengl& window, const FrameTimestamp& time)
{
	(void)window; (void)time;
	if (enableUi == false && demoPlayer.settings.debug.playMode == PlayMode::Production) {
		inputController->resetBoth();
		return;
	}

	if (inputController->getButtonState(b1) == ButtonState::Pressed) {
		VERSO_LOG_DEBUG("verso-demokit", "b1");
	}
	if (inputController->getButtonState(b2) == ButtonState::Pressed) {
		VERSO_LOG_DEBUG("verso-demokit", "b2");
	}
	if (inputController->getButtonState(b3) == ButtonState::Pressed) {
		VERSO_LOG_DEBUG("verso-demokit", "b3");
	}
	if (inputController->getButtonState(b4) == ButtonState::Pressed) {
		VERSO_LOG_DEBUG("verso-demokit", "b4");
	}
	if (inputController->getButtonState(b5) == ButtonState::Pressed) {
		VERSO_LOG_DEBUG("verso-demokit", "b5");
	}
	if (inputController->getButtonState(b6) == ButtonState::Pressed) {
		VERSO_LOG_DEBUG("verso-demokit", "b6");
	}
	if (inputController->getButtonState(b7) == ButtonState::Pressed) {
		VERSO_LOG_DEBUG("verso-demokit", "b7");
	}
	if (inputController->getButtonState(b8) == ButtonState::Pressed) {
		VERSO_LOG_DEBUG("verso-demokit", "b8");
	}
	if (inputController->getButtonState(b9) == ButtonState::Pressed) {
		VERSO_LOG_DEBUG("verso-demokit", "b9");
	}
	if (inputController->getButtonState(b10) == ButtonState::Pressed) {
		VERSO_LOG_DEBUG("verso-demokit", "b10");
	}

	if (inputController->getButtonPressedDown(buttonPlayPause)) {
		actionPlayPause(window);
	}

	if (inputController->getButtonPressedDown(buttonRewindToStart)) {
		actionRewindToStart(window);
	}

	if (inputController->getButtonPressedDown(buttonToggleFullscreen)) {
		actionToggleFullscreen(window);
	}

	if (inputController->getButtonPressedDown(buttonToggleEditor)) {
		actionToggleEditor(window);
	}

	//if (keyboard.state == ButtonState::Released && keyboard.key.scancode == KeyScancode::F5) {
	//	demoPlayer.settings.saveToJson("test.json");
	//}

//	UString axis("[ ");
//	axis.append2(inputController->getAxisState(a1));
//	axis += ", ";
//	axis.append2(inputController->getAxisState(a2));
//	axis += " ] [";
//	axis.append2(inputController->getAxisState(a3));
//	axis += ", ";
//	axis.append2(inputController->getAxisState(a4));
//	axis += " ]";

	inputController->resetBoth();
}


void DemoKitUi::handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event)
{
//	switch (event.type) {
//	case EventType::Keyboard:
//	{
//		const KeyboardEvent& keyboard = event.keyboard;
//		if (keyboard.type == KeyboardEventType::Key) {
//		}
//		break;
//	}
//	default:
//		break;
//	}

	imgui.handleEvent(window, time, event);
}


void DemoKitUi::render(IWindowOpengl& window, const FrameTimestamp& time)
{
	if (enableUi == false) {
		return;
	}

	Render::clearScreen(ClearFlag::SolidColor, editorBgColor);

	imgui.newFrame(window, time, false);
	ImGuiStyle& style = ImGui::GetStyle();
	style.WindowRounding = 0;

	//	ImGui::PushFont(smallerFont);
	//	ImGui::PushFont(biggerFont);
	//	ImGui::PopFont();

	// Main menu
	menuBar(window);

	// Dear Imgui examples
	if (viewImGuiDemoWindow) {
		ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiCond_Once);
		ImGui::ShowDemoWindow(&viewImGuiDemoWindow);
	}

	//showExampleAppConstrainedResize(&viewTestWindow);
	//showExampleAppPropertyEditor(&viewTestWindow);

	Vector2i drawableResolution(window.getDrawableResolutioni());
	int leftPanelWidth = drawableResolution.x / 5;
	int leftPanelHeight = drawableResolution.y - mainMenuHeight;
	int rightPanelWidth = drawableResolution.x / 3;
	int horizontalPanelWidth = drawableResolution.x;
	if (viewPanelsToggle && viewRightPanel) {
		horizontalPanelWidth -= rightPanelWidth;
	}
	int horizontalPanelHeight = drawableResolution.y / 3;
	int mainControlsHeight = 48;
	int mainViewTopLeftX = 0;
	int mainViewWidth = drawableResolution.x;
	int mainViewHeight = drawableResolution.y;

	mainViewHeight -= mainMenuHeight;
	mainViewHeight -= mainControlsHeight;

	if (viewPanelsToggle && viewLeftPanel) {
		mainViewTopLeftX = leftPanelWidth;
		mainViewWidth -= leftPanelWidth;
	}
	if (viewPanelsToggle && viewRightPanel) {
		mainViewWidth -= rightPanelWidth;
	}
	if (viewPanelsToggle && viewHorizontalPanel) {
		mainViewHeight -= horizontalPanelHeight;
		leftPanelHeight -= horizontalPanelHeight;
	}

	// Main view
	panelMainView(window,
				  ImVec2(static_cast<float>(mainViewTopLeftX), static_cast<float>(mainMenuHeight)),
				  ImVec2(static_cast<float>(mainViewWidth), static_cast<float>(mainViewHeight)));

	// Main controls
	if (viewMainControls) {
		ImGui::SetNextWindowPos(ImVec2(static_cast<float>(mainViewTopLeftX), static_cast<float>(mainMenuHeight + mainViewHeight)), ImGuiCond_Always);
		ImGui::SetNextWindowSize(ImVec2(static_cast<float>(mainViewWidth), static_cast<float>(mainControlsHeight)), ImGuiCond_Always);

		ImGui::Begin("Main controls", nullptr,
					 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
					 ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse |
					 ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);

		panelMainControls(window);

		ImGui::End();
	}

	// Left panel
	if (viewPanelsToggle && viewLeftPanel) {
		ImGui::SetNextWindowPos(ImVec2(0.0f, static_cast<float>(mainMenuHeight)), ImGuiCond_Always);
		ImGui::SetNextWindowSize(ImVec2(static_cast<float>(leftPanelWidth), static_cast<float>(leftPanelHeight)), ImGuiCond_Always);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));

		ImGui::Begin("Left panel", nullptr,
					 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
					 ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);

		panelLeft(window);
		ImGui::End();

		ImGui::PopStyleVar();
	}

	// Right panel
	if (viewPanelsToggle && viewRightPanel) {
		ImGui::SetNextWindowPos(
					ImVec2(static_cast<float>(drawableResolution.x - rightPanelWidth),
						   static_cast<float>(mainMenuHeight)),
					ImGuiCond_Always);
		ImGui::SetNextWindowSize(ImVec2(
									 static_cast<float>(rightPanelWidth),
									 static_cast<float>(drawableResolution.y - mainMenuHeight)),
								 ImGuiCond_Always);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));

		ImGui::Begin("Right panel", nullptr,
					 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
					 ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);

		panelRight(window);

		ImGui::End();
		ImGui::PopStyleVar();
	}

	// Horizontal panel
	if (viewPanelsToggle && viewHorizontalPanel) {
		ImGui::SetNextWindowPos(ImVec2(
									0.0f,
									static_cast<float>(drawableResolution.y - horizontalPanelHeight)),
								ImGuiCond_Always);
		ImGui::SetNextWindowSize(ImVec2(static_cast<float>(horizontalPanelWidth), static_cast<float>(horizontalPanelHeight)), ImGuiCond_Always);
		ImGui::SetNextWindowContentSize(ImVec2(static_cast<float>(horizontalPanelWidth), static_cast<float>(horizontalPanelHeight)));
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));

		ImGui::Begin("DemoKit", nullptr,
					 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
					 ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);

		panelHorizontal(window);

		ImGui::End();
		ImGui::PopStyleVar();
	}

	imgui.render(window, window.getDrawableViewporti());
}


bool DemoKitUi::isQuitting() const
{
	// not supported for DemoKitUi
	return false;
}


void DemoKitUi::quit()
{
	// not supported for DemoKitUi
}


bool DemoKitUi::imguiImageButton(const UString& subAtlasName, const UString& iconName, const UString& id,
									int framePadding, const RgbaColorf& bgColor, const RgbaColorf& tintColor)
{
	TextureArea texArea = icons.getTextureArea(subAtlasName, iconName);
	Rectf a = texArea.getArea();
	Vector2f s = texArea.getTexture()->getResolutionf();
	ImGui::PushID(id.c_str());
	bool returnValue = ImGui::ImageButton(reinterpret_cast<ImTextureID>(static_cast<int64_t>(texArea.getTexture()->getHandle())),
										  ImVec2(a.size.x, a.size.y),
										  ImVec2(a.getX1()/ s.x, a.getY1() / s.y),
										  ImVec2(a.getX2()/s.x, a.getY2()/s.y),
										  framePadding,
										  ImVec4(bgColor.r, bgColor.g, bgColor.b, bgColor.a),
										  ImVec4(tintColor.r, tintColor.g, tintColor.b, tintColor.a));
	ImGui::PopID();
	return returnValue;
}


void DemoKitUi::menuBar(const IWindowOpengl& window)
{
	Vector2i drawableResolution(window.getDrawableResolutioni());

	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			menuBarFile(window);
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Edit"))
		{
			menuBarEdit(window);
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("View"))
		{
			menuBarView(window);
			ImGui::EndMenu();
		}

		char temp[256] = "";
		snprintf(temp, 256, "%.2f ms (%.1f FPS)", static_cast<double>(1000.0f / ImGui::GetIO().Framerate), static_cast<double>(ImGui::GetIO().Framerate));
		float fpsTextSize = ImGui::CalcTextSize(temp).x;
		ImGui::SameLine(drawableResolution.x - fpsTextSize - 2 * ImGui::GetStyle().ItemSpacing.x, 0);
		ImGui::Text("%s", temp);

		temp[0] = '\0';
		snprintf(temp, 256, "Stats: %s", RenderStats::toString().c_str());
		ImGui::SameLine(drawableResolution.x - fpsTextSize - ImGui::CalcTextSize(temp).x - 8 * ImGui::GetStyle().ItemSpacing.x, 0);
		ImGui::Text("%s", temp);

		mainMenuHeight = static_cast<int>(ImGui::GetWindowSize().y);

		ImGui::EndMainMenuBar();
	}
}


void DemoKitUi::menuBarFile(const IWindowOpengl& window)
{
	(void)window;

	if (ImGui::MenuItem("Screenshot"))
	{
		static int frameCount = 1;
		UString fileName("frame-");
		std::ostringstream ss;
		ss << std::setw(8) << std::setfill('0') << frameCount;
		fileName.append(ss.str());
		fileName.append("." + imageSaveFormatToFileExtension(ImageSaveFormat::Png));
		demoKitApp.saveRenderFboToFile(fileName, ImageSaveFormat::Png);
		UString out("Saved screenshot to \"");
		out += fileName;
		out += "\".";
		VERSO_LOG_INFO("verso-demokit", out.c_str());
		frameCount++;
	}

	ImGui::MenuItem("(dummy menu)", nullptr, false, false);
	if (ImGui::MenuItem("New")) {}
	if (ImGui::MenuItem("Open", "Ctrl+O")) {
	}
	if (ImGui::BeginMenu("Open Recent"))
	{
		ImGui::MenuItem("fish_hat.c");
		ImGui::MenuItem("fish_hat.inl");
		ImGui::MenuItem("fish_hat.h");
		if (ImGui::BeginMenu("More.."))
		{
			ImGui::MenuItem("Hello");
			ImGui::MenuItem("Sailor");
			ImGui::EndMenu();
		}
		ImGui::EndMenu();
	}
	if (ImGui::MenuItem("Save", "Ctrl+S")) {}
	if (ImGui::MenuItem("Save As..")) {}
	ImGui::Separator();
	if (ImGui::BeginMenu("Disabled", false)) // Disabled
	{
		IM_ASSERT(0);
	}
	if (ImGui::MenuItem("Quit", "Alt+F4")) {
		demoKitApp.quit();
	}
}


void DemoKitUi::menuBarEdit(const IWindowOpengl& window)
{
	(void)window;

	if (ImGui::MenuItem("Undo", "CTRL+Z")) {}
	if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) {}  // Disabled item
	ImGui::Separator();
	if (ImGui::MenuItem("Cut", "CTRL+X")) {}
	if (ImGui::MenuItem("Copy", "CTRL+C")) {}
	if (ImGui::MenuItem("Paste", "CTRL+V")) {}
}


void DemoKitUi::menuBarView(const IWindowOpengl& window)
{
	(void)window;

	if (ImGui::MenuItem("Fullscreen", "F11", &enableUi, true)) {}
	if (ImGui::MenuItem("Toggle panels", "F12", &viewPanelsToggle, true)) {}
	if (ImGui::MenuItem("Left panel", nullptr, &viewLeftPanel, true)) {}
	if (ImGui::MenuItem("Right panel", nullptr, &viewRightPanel, true)) {}
	if (ImGui::MenuItem("Horizontal panel", nullptr, &viewHorizontalPanel, true)) {}
	if (ImGui::MenuItem("ImGui Demo", nullptr, &viewImGuiDemoWindow, true)) {}
}


void DemoKitUi::panelMainView(const IWindowOpengl& window, const ImVec2& windowPos, const ImVec2& windowSize)
{
	(void)window;

	ImGui::SetNextWindowPos(windowPos, ImGuiCond_Always);
	ImGui::SetNextWindowSize(windowSize, ImGuiCond_Always);

	Vector2f size;
	ImVec2 padding;
	if (this->renderTexture != nullptr) {
		float aspectRatio = this->renderTexture->getAspectRatioTarget().value;
		if (demoPlayer.getDemoPlayerSettings().general.contentAspectRatio.value != 0) {
			aspectRatio = demoPlayer.getDemoPlayerSettings().general.contentAspectRatio.value;
		}

		size = calculateRefScaleSize(
					RefScale::ViewportSize_KeepAspectRatio_FitRect,
					Vector2f(1, 1),
					this->renderTexture->getResolutionf(),
					aspectRatio,
					Vector2i(static_cast<int>(windowSize.x), static_cast<int>(windowSize.y)));

		padding = ImVec2((windowSize.x - size.x) / 2.0f,
						 (windowSize.y - size.y) / 2.0f);
	}

	RgbaColorf c(demoPlayer.getDemoPlayerSettings().general.borderColor);
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(c.r, c.g, c.b, c.a));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, padding);
	ImGui::Begin("Main view", nullptr,
				 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
				 ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse |
				 ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);

	if (this->renderTexture != nullptr) {
		ImGui::Image(reinterpret_cast<ImTextureID>(static_cast<int64_t>(this->renderTexture->getHandle())),
					 ImVec2(size.x, size.y), ImVec2(0, 1), ImVec2(1, 0));
	}

	ImGui::End();
	ImGui::PopStyleVar();
	ImGui::PopStyleColor();
}


void DemoKitUi::panelMainControls(IWindowOpengl& window)
{
	ImGui::BeginGroup();
	ImGui::PushID("demoKitMainControls");
	{
		if (imguiImageButton("icon-av-"+selectedIconSet, "icon-ic_fast_rewind", "fast_rewind", 0)) {
			VERSO_LOG_DEBUG("verso-demokit", "Fast rewind pressed");
		}
		ImGui::SameLine();

		if (imguiImageButton("icon-av-"+selectedIconSet, "icon-ic_fast_forward", "fast_forward", 0)) {
			VERSO_LOG_DEBUG("verso-demokit", "Fast forward pressed");
		}
		ImGui::SameLine();

		if (imguiImageButton("icon-av-"+selectedIconSet, "icon-ic_skip_previous", "skip_previous", 0)) {
			VERSO_LOG_DEBUG("verso-demokit", "Skip previous pressed");
		}
		ImGui::SameLine();

		if (imguiImageButton("icon-av-"+selectedIconSet, "icon-ic_skip_next", "skip_next", 0)) {
			VERSO_LOG_DEBUG("verso-demokit", "Skip next pressed");
		}
		ImGui::SameLine();

		if (imguiImageButton("icon-image-"+selectedIconSet, "icon-ic_navigate_before", "navigate_before", 0)) {
			VERSO_LOG_DEBUG("verso-demokit", "Navigate previous pressed");
		}
		ImGui::SameLine();

		//const bool browseButtonPressed =
		ImGui::Button("Open");
		{
			// Inside a ImGui window:
			//static ImGuiFs::Dialog dlg; // one per dialog (and must be static)
			//const char* chosenPath = dlg.chooseFileDialog(browseButtonPressed); // see other dialog types and the full list of arguments for advanced usage
			//if (strlen(chosenPath)>0) {
			//	// A path (chosenPath) has been chosen RIGHT NOW. However we can retrieve it later more comfortably using: dlg.getChosenPath()
			//}
			//if (strlen(dlg.getChosenPath())>0) {
			//	ImGui::Text("Chosen file: \"%s\"",dlg.getChosenPath());
			//}
			//
			// If you want to copy the (valid) returned path somewhere, you can use something like:
			//static char myPath[ImGuiFs::MAX_PATH_BYTES];
			//if (strlen(dlg.getChosenPath())>0) {
			//	strcpy(myPath,dlg.getChosenPath());
			//}
		}

		ImGui::SameLine();

		if (demoPlayer.getPlayState() == PlayState::Paused) {
			if (imguiImageButton("icon-av-"+selectedIconSet, "icon-ic_play_circle_outline", "play", 0)) {
				VERSO_LOG_DEBUG("verso-demokit", "Play pressed");
				actionPlayPause(window);
			}
		}
		else {
			if (imguiImageButton("icon-av-"+selectedIconSet, "icon-ic_pause_circle_outline", "pause", 0)) {
				VERSO_LOG_DEBUG("verso-demokit", "Pause pressed");
				actionPlayPause(window);
			}
		}
		ImGui::SameLine();

		if (imguiImageButton("icon-image-"+selectedIconSet, "icon-ic_navigate_next", "navigate_next", 0)) {
			VERSO_LOG_DEBUG("verso-demokit", "Navigate next pressed");
		}
	}
	ImGui::PopID();
	ImGui::EndGroup();

	ImGui::SameLine();

	ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x * 0.6f);
	ImGui::BeginGroup();
	{
		ImGui::SetCursorPosY(17);
		UString elapsedStr = Timestamp::seconds(demoPlayer.getElapsedSeconds()).asDigitalClockString(true);
		ImGui::Text("%s", elapsedStr.c_str());
		ImGui::SameLine();

		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 2));

		ImGui::SetCursorPosY(15);
		float secondsNow = static_cast<float>(demoPlayer.getElapsedSeconds());
		UString durationStr = Timestamp::seconds(demoPlayer.settings.general.duration).asDigitalClockString(true);
		if (ImGui::SliderFloat(durationStr.c_str(), &secondsNow, 0.0f, static_cast<float>(demoPlayer.settings.general.duration), "")) {
			demoPlayer.gotoAbsolute(window, static_cast<double>(secondsNow));
		}
		ImGui::PopStyleVar();
	}
	ImGui::EndGroup();
}


void DemoKitUi::panelLeft(const IWindowOpengl& window)
{
	(void)window;

	ImGui::Text("Right panel");
}


void DemoKitUi::panelRight(const IWindowOpengl& window)
{
	(void)window;

	ImGuiTabBarFlags tbFlags = 0;
//    ImGuiTabBarFlags_None                           = 0,
//    ImGuiTabBarFlags_Reorderable                    = 1 << 0,   // Allow manually dragging tabs to re-order them + New tabs are appended at the end of list
//    ImGuiTabBarFlags_AutoSelectNewTabs              = 1 << 1,   // Automatically select new tabs when they appear
//    ImGuiTabBarFlags_TabListPopupButton             = 1 << 2,   // Disable buttons to open the tab list popup
//    ImGuiTabBarFlags_NoCloseWithMiddleMouseButton   = 1 << 3,   // Disable behavior of closing tabs (that are submitted with p_open != NULL) with middle mouse button. You can still repro this behavior on user's side with if (IsItemHovered() && IsMouseClicked(2)) *p_open = false.
//    ImGuiTabBarFlags_NoTabListScrollingButtons      = 1 << 4,   // Disable scrolling buttons (apply when fitting policy is ImGuiTabBarFlags_FittingPolicyScroll)
//    ImGuiTabBarFlags_NoTooltip                      = 1 << 5,   // Disable tooltips when hovering a tab
//    ImGuiTabBarFlags_FittingPolicyResizeDown        = 1 << 6,   // Resize tabs when they don't fit
//    ImGuiTabBarFlags_FittingPolicyScroll            = 1 << 7,   // Add scroll buttons when tabs don't fit
//    ImGuiTabBarFlags_FittingPolicyMask_             = ImGuiTabBarFlags_FittingPolicyResizeDown | ImGuiTabBarFlags_FittingPolicyScroll,
//    ImGuiTabBarFlags_FittingPolicyDefault_          = ImGuiTabBarFlags_FittingPolicyResizeDown
	ImGui::BeginTabBar("#rightPanelTabs", tbFlags);

	ImGuiTabItemFlags tbiFlags = 0;
//    ImGuiTabItemFlags_None                          = 0,
//    ImGuiTabItemFlags_UnsavedDocument               = 1 << 0,   // Append '*' to title without affecting the ID, as a convenience to avoid using the ### operator. Also: tab is selected on closure and closure is deferred by one frame to allow code to undo it without flicker.
//    ImGuiTabItemFlags_SetSelected                   = 1 << 1,   // Trigger flag to programmatically make the tab selected when calling BeginTabItem()
//    ImGuiTabItemFlags_NoCloseWithMiddleMouseButton  = 1 << 2,   // Disable behavior of closing tabs (that are submitted with p_open != NULL) with middle mouse button. You can still repro this behavior on user's side with if (IsItemHovered() && IsMouseClicked(2)) *p_open = false.
//    ImGuiTabItemFlags_NoPushId                      = 1 << 3    // Don't call PushID(tab->ID)/PopID() on BeginTabItem()/EndTabItem()
	// bool* p_open = NULL
	if (ImGui::BeginTabItem("DemoPart", nullptr, tbiFlags)) {
			tabDemoPart(window);
			ImGui::EndTabItem();
	}
	if (ImGui::BeginTabItem("Material", nullptr, tbiFlags)) {
		tabMaterial(window);
		ImGui::EndTabItem();
	}
	if (ImGui::BeginTabItem("Postproc", nullptr, tbiFlags)) {
		tabPostProc(window);
		ImGui::EndTabItem();
	}
	if (ImGui::BeginTabItem("Demo", nullptr, tbiFlags)) {
		tabDemo(window);
		ImGui::EndTabItem();
	}
	ImGui::EndTabBar();
}


void DemoKitUi::tabDemoPart(const IWindowOpengl& window)
{
	(void)window;

	/*
	static bool fullscreen = true;
	if(ImGui::Checkbox("Fullscreen Mode",&fullscreen)){
		fullscreen = !fullscreen;
	}
	static bool enableMultisampling = true;
	if(ImGui::Checkbox("Enable Multisampling",&enableMultisampling)){
		enableMultisampling = !enableMultisampling;
	}

	int multisampleCount = 1;
	ImGui::SliderInt("MSAA Count",reinterpret_cast<int*>(&multisampleCount), 2, 8);

	ImGui::Text("Camera");
	*/
	if (selectedDemoPart == nullptr) {
		ImGui::Text("No DemoPart selected");
		return;
	}

	char buffer64[64] = "";
	//char buffer256[256] = "";
	std::int32_t v1[2] = { 0, 0 };
	float v2[2] = { 0, 0 };

	ImGui::Text("Info");
	{
		strcpy(buffer64, selectedDemoPart->getSettings().name.c_str());
		if (ImGui::InputText("Name", buffer64, IM_ARRAYSIZE(buffer64))) {
			selectedDemoPart->getSettings().name = buffer64;
		}

		strcpy(buffer64, selectedDemoPart->getSettings().type.c_str());
		if (ImGui::InputText("Type", buffer64, IM_ARRAYSIZE(buffer64))) {
			selectedDemoPart->getSettings().type = buffer64;
		}

		v2[0] = static_cast<float>(selectedDemoPart->getSettings().start);
		if (ImGui::InputFloat("Start", &v2[0], 0.25f, 1.0f, "%.3f")) {
			demoPlayer.settings.moveDemoPart(selectedDemoPart, static_cast<double>(v2[0]));
			demoPlayer.settings.updateSubsequentPriorities();
		}

		v2[0] = static_cast<float>(selectedDemoPart->getSettings().duration);
		if (ImGui::InputFloat("Duration", &v2[0], 0.25f, 1.0f, "%.3f")) {
			demoPlayer.settings.resizeDemoPart(selectedDemoPart, static_cast<double>(v2[0]));
			demoPlayer.settings.updateSubsequentPriorities();
		}

		v1[0] = selectedDemoPart->getSettings().priority;
		if (ImGui::InputInt("Priority", &v1[0], 1, 5)) {
			selectedDemoPart->getSettings().priority = v1[0];
			demoPlayer.settings.updateSubsequentPriorities();
		}

		v1[0] = selectedDemoPart->getSettings().jsonOrder;
		if (ImGui::InputInt("Order", &v1[0], 1, 5)) {
			selectedDemoPart->getSettings().jsonOrder = v1[0];
			demoPlayer.settings.updateSubsequentPriorities();
		}

	}

	ImGui::Text("Parameters");
	{
		//const JSONObject& paramsJson;
	}
}


void DemoKitUi::tabMaterial(const IWindowOpengl& window)
{
	(void)window;

	ImGui::Text("- Textures");
	ImGui::Text("- Shaders");
	ImGui::Text("...");
}


void DemoKitUi::tabPostProc(const IWindowOpengl& window)
{
	(void)window;

	ImGui::Text("...");
}


void DemoKitUi::tabDemo(const IWindowOpengl& window)
{
	(void)window;

	ImGui::PushID("RightPanel/Demo");

	char buffer64[64] = "";
	char buffer256[256] = "";
	float v2[2] = { 0, 0 };

	ImGui::Text("Info");
	{
		strcpy(buffer64, demoPlayer.settings.general.name.c_str());
		if (ImGui::InputText("Name", buffer64, IM_ARRAYSIZE(buffer64))) {
			demoPlayer.settings.general.name = buffer64;
		}

		strcpy(buffer64, demoPlayer.settings.general.version.c_str());
		if (ImGui::InputText("Version", buffer64, IM_ARRAYSIZE(buffer64))) {
			demoPlayer.settings.general.version = buffer64;
		}

		strcpy(buffer64, demoPlayer.settings.general.authors.c_str());
		if (ImGui::InputText("Author(s)", buffer64, IM_ARRAYSIZE(buffer64))) {
			demoPlayer.settings.general.authors = buffer64;
		}
	}

	ImGui::Text("Playback");
	{
		int currentItem = (demoPlayer.settings.debug.playMode == PlayMode::Production ? 0 : 1);
		if (ImGui::Combo("Play mode", &currentItem, "Production\0Development\0\0")) {
			demoPlayer.settings.debug.playMode = (currentItem == 0 ? PlayMode::Production : PlayMode::Development);
		}

		v2[0] = static_cast<float>(demoPlayer.settings.general.duration);
		if (ImGui::InputFloat("Duration", &v2[0], 1.0f, 5.0f, "%.3f")) {
			demoPlayer.settings.general.duration = static_cast<double>(v2[0]);
		}

		ImGui::Checkbox("Loop", &demoPlayer.settings.general.loop);
	}

	ImGui::Text("Debug playback range");
	{
		v2[0] = static_cast<float>(demoPlayer.settings.debug.playbackRange.minValue);
		if (ImGui::SliderFloat("Start", &v2[0], 0.0f, static_cast<float>(demoPlayer.settings.debug.playbackRange.maxValue) - 0.5f)) {
			demoPlayer.settings.debug.playbackRange.minValue = static_cast<double>(v2[0]);
		}

		v2[0] = static_cast<float>(demoPlayer.settings.debug.playbackRange.maxValue);
		if (ImGui::SliderFloat("End", &v2[0], static_cast<float>(demoPlayer.settings.debug.playbackRange.minValue) + 0.5f, static_cast<float>(demoPlayer.settings.general.duration))) {
			demoPlayer.settings.debug.playbackRange.maxValue = static_cast<double>(v2[0]);
		}
	}

	ImGui::Text("Target FPS");
	v2[0] = static_cast<float>(demoPlayer.settings.general.targetFps);
	if (ImGui::SliderFloat("Target FPS", &v2[0], 5, 100)) {
		demoPlayer.settings.general.targetFps = static_cast<double>(v2[0]);
	}

	ImGui::Text("Music");
	{
		strcpy(buffer256, demoPlayer.settings.general.musicFileName.c_str());
		if (ImGui::InputText("File name", buffer256, IM_ARRAYSIZE(buffer256))) {
			demoPlayer.settings.general.musicFileName = buffer256;
		}
	}

	ImGui::Text("Paths");
	strcpy(buffer256, demoPlayer.settings.demoPaths.models.c_str());
	if (ImGui::InputText("Models", buffer256, IM_ARRAYSIZE(buffer256))) {
		demoPlayer.settings.demoPaths.models = buffer256;
	}

	strcpy(buffer256, demoPlayer.settings.demoPaths.shaders.c_str());
	if (ImGui::InputText("Shaders", buffer256, IM_ARRAYSIZE(buffer256))) {
		demoPlayer.settings.demoPaths.shaders = buffer256;
	}

	strcpy(buffer256, demoPlayer.settings.demoPaths.music.c_str());
	if (ImGui::InputText("Music##Paths", buffer256, IM_ARRAYSIZE(buffer256))) {
		demoPlayer.settings.demoPaths.music = buffer256;
	}

	strcpy(buffer256, demoPlayer.settings.demoPaths.textures.c_str());
	if (ImGui::InputText("Textures", buffer256, IM_ARRAYSIZE(buffer256))) {
		demoPlayer.settings.demoPaths.textures = buffer256;
	}

	strcpy(buffer256, demoPlayer.settings.demoPaths.fonts.c_str());
	if (ImGui::InputText("Fonts", buffer256, IM_ARRAYSIZE(buffer256))) {
		demoPlayer.settings.demoPaths.fonts = buffer256;
	}

	strcpy(buffer256, demoPlayer.settings.demoPaths.particles.c_str());
	if (ImGui::InputText("Particles", buffer256, IM_ARRAYSIZE(buffer256))) {
		demoPlayer.settings.demoPaths.particles = buffer256;
	}

	strcpy(buffer256, demoPlayer.settings.demoPaths.gui.c_str());
	if (ImGui::InputText("GUI", buffer256, IM_ARRAYSIZE(buffer256))) {
		demoPlayer.settings.demoPaths.gui = buffer256;
	}

	ImGui::PopID();
}


void DemoKitUi::panelHorizontal(IWindowOpengl& window)
{
	ImGuiTabBarFlags tbFlags = 0;
//    ImGuiTabBarFlags_None                           = 0,
//    ImGuiTabBarFlags_Reorderable                    = 1 << 0,   // Allow manually dragging tabs to re-order them + New tabs are appended at the end of list
//    ImGuiTabBarFlags_AutoSelectNewTabs              = 1 << 1,   // Automatically select new tabs when they appear
//    ImGuiTabBarFlags_TabListPopupButton             = 1 << 2,   // Disable buttons to open the tab list popup
//    ImGuiTabBarFlags_NoCloseWithMiddleMouseButton   = 1 << 3,   // Disable behavior of closing tabs (that are submitted with p_open != NULL) with middle mouse button. You can still repro this behavior on user's side with if (IsItemHovered() && IsMouseClicked(2)) *p_open = false.
//    ImGuiTabBarFlags_NoTabListScrollingButtons      = 1 << 4,   // Disable scrolling buttons (apply when fitting policy is ImGuiTabBarFlags_FittingPolicyScroll)
//    ImGuiTabBarFlags_NoTooltip                      = 1 << 5,   // Disable tooltips when hovering a tab
//    ImGuiTabBarFlags_FittingPolicyResizeDown        = 1 << 6,   // Resize tabs when they don't fit
//    ImGuiTabBarFlags_FittingPolicyScroll            = 1 << 7,   // Add scroll buttons when tabs don't fit
//    ImGuiTabBarFlags_FittingPolicyMask_             = ImGuiTabBarFlags_FittingPolicyResizeDown | ImGuiTabBarFlags_FittingPolicyScroll,
//    ImGuiTabBarFlags_FittingPolicyDefault_          = ImGuiTabBarFlags_FittingPolicyResizeDown
	ImGui::BeginTabBar("#horizontalPanelTabs", tbFlags);

	ImGuiTabItemFlags tbiFlags = 0;
//    ImGuiTabItemFlags_None                          = 0,
//    ImGuiTabItemFlags_UnsavedDocument               = 1 << 0,   // Append '*' to title without affecting the ID, as a convenience to avoid using the ### operator. Also: tab is selected on closure and closure is deferred by one frame to allow code to undo it without flicker.
//    ImGuiTabItemFlags_SetSelected                   = 1 << 1,   // Trigger flag to programmatically make the tab selected when calling BeginTabItem()
//    ImGuiTabItemFlags_NoCloseWithMiddleMouseButton  = 1 << 2,   // Disable behavior of closing tabs (that are submitted with p_open != NULL) with middle mouse button. You can still repro this behavior on user's side with if (IsItemHovered() && IsMouseClicked(2)) *p_open = false.
//    ImGuiTabItemFlags_NoPushId                      = 1 << 3    // Don't call PushID(tab->ID)/PopID() on BeginTabItem()/EndTabItem()
	// bool* p_open = NULL
	if (ImGui::BeginTabItem("Timeline", nullptr, tbiFlags)) {
		ImGui::PushID("HorizontalPanel/Timeline");
		//ImGui::Unindent(ImGui::GetStyle().WindowPadding.x);
		tabTimeline(window);

		ImGui::PopID();
		ImGui::EndTabItem();
	}

	if (ImGui::BeginTabItem("Console", nullptr, tbiFlags)) {
		ImGui::PushID("HorizontalPanel/Console");

		ImGui::PopID();
		ImGui::EndTabItem();
	}
	ImGui::EndTabBar();
}


void DemoKitUi::tabTimeline(IWindowOpengl& window)
{
	float timeNowSeconds = static_cast<float>(demoPlayer.getElapsedSeconds());
	static float scrollPosSeconds = 0.0f;
	static float viewWidthSeconds = static_cast<float>(demoPlayer.settings.general.duration * 1.15);

	ImGui::PushFont(smallerFont);
	if (demoTimeLine(demoPlayer.settings, &timeNowSeconds, &scrollPosSeconds, &viewWidthSeconds, &selectedDemoPart) == true) {
		demoPlayer.gotoAbsolute(window, static_cast<double>(timeNowSeconds));
	}
	ImGui::PopFont();
}


void DemoKitUi::actionPlayPause(IWindowOpengl& window)
{
	(void)window;

	if (demoPlayer.getPlayState() == PlayState::Paused) {
		//VERSO_LOG_DEBUG("verso-demokit", "Play pressed");
		demoPlayer.setPlayState(PlayState::Playing);
	}
	else {
		//VERSO_LOG_DEBUG("verso-demokit", "Pause pressed");
		demoPlayer.setPlayState(PlayState::Paused);
	}
}


void DemoKitUi::actionRewindToStart(IWindowOpengl& window)
{
	if (demoPlayer.settings.debug.playbackRangeEnabled) {
		//VERSO_LOG_DEBUG("verso-demokit", "Rewind to start (playback range)");
		demoPlayer.gotoAbsolute(window, demoPlayer.settings.debug.playbackRange.minValue);
	}
	else {
		//VERSO_LOG_DEBUG("verso-demokit", "Rewind to start");
		demoPlayer.gotoAbsolute(window, 0.0f);
	}
}


void DemoKitUi::actionToggleFullscreen(IWindowOpengl& window)
{
	if (enableUi == false) {
		enableUi = true;
		viewPanelsToggle = false;
	}
	else if (viewPanelsToggle == true) {
		viewPanelsToggle = false;
	}
	else {
		enableUi = false;
	}

	if (demoPlayer.settings.debug.playMode == PlayMode::Development) {
		window.setCursorShown(true);
	}
	else {
		window.setCursorShown(enableUi);
	}
}


void DemoKitUi::actionToggleEditor(IWindowOpengl& window)
{
	(void)window;

	if (enableUi == false) {
		enableUi = true;
		viewPanelsToggle = true;
	}
	else if (viewPanelsToggle == false) {
		viewPanelsToggle = true;
	}
	else {
		enableUi = false;
	}
}


} // End namespace Verso

