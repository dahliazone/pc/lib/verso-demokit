#include <Verso/DemoKit/DemoPartFactory.hpp>
#include <Verso/DemoKit/DemoParts/ClearScreen.hpp>
#include <Verso/DemoKit/DemoParts/Heightmap.hpp>
#include <Verso/DemoKit/DemoParts/ImageViewer.hpp>
#include <Verso/DemoKit/DemoParts/ImGuiTest.hpp>
#include <Verso/DemoKit/DemoParts/Particles.hpp>
#include <Verso/DemoKit/DemoParts/RandomBackground.hpp>
#include <Verso/DemoKit/DemoParts/Shadertoy.hpp>
#include <Verso/DemoKit/DemoParts/SimpleScroller.hpp>
#include <Verso/DemoKit/DemoParts/Sprites3d.hpp>
#include <Verso/DemoKit/DemoParts/Text.hpp>

namespace Verso {


DemoPartFactory::DemoPartFactory()
{
}


DemoPartFactory::~DemoPartFactory()
{
}


DemoPart* DemoPartFactory::instance(
		const DemoPaths* demoPaths, const JsonPath& demoPartJson,
		std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) const
{
	UString type = demoPartJson.readString("type", true);

	if (type == "verso.clearscreen") {
		return new ClearScreen(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
	}

	else if (type == "verso.heightmap") {
		return new Heightmap(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
	}

	else if (type == "verso.imageviewer") {
		return new ImageViewer(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
	}

	else if (type == "verso.imguitest") {
		return new ImGuiTest(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
	}

	else if (type == "verso.particles") {
		return new Particles(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
	}

	else if (type == "verso.randombackground") {
		return new RandomBackground(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
	}

	else if (type == "verso.shadertoy") {
		return new Shadertoy(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
	}

	else if (type == "verso.simplescroller") {
		return new SimpleScroller(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
	}

	else if (type == "verso.sprites3d") {
		return new Sprites3d(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
	}

	else if (type == "verso.text") {
		return new Text(demoPaths, demoPartJson, jsonOrder, startAccumulator, priorityAccumulator);
	}

	else {
		UString error("Cannot find DemoPart with given type!");
		UString typeInfo("type = \""+type+"\"");
		VERSO_OBJECTNOTFOUND("verso-demokit", error.c_str(), typeInfo.c_str());
	}
}


} // End namespace Verso

