#include <Verso/DemoKit/DemoPlayerSettings.hpp>
#include <Verso/System/JsonHelperDemoKit.hpp>

namespace Verso {


UString DemoPlayerSettings::supportedJsonFormat("DemoKit04");


DemoPlayerSettings::DemoPlayerSettings() :
	sourceFileName(),

	// data
	format(),
	general(),
	demoPaths(),
	setupDialog(),
	debug(),
	demoParts(),

	// private
	demoPartFactory(nullptr),
	rootPath(),
	created(false)
{
}


DemoPlayerSettings::DemoPlayerSettings(DemoPlayerSettings&& original) :
	// Demo model
	sourceFileName(std::move(original.sourceFileName)),

	// data
	format(std::move(original.format)),
	general(std::move(original.general)),
	demoPaths(std::move(original.demoPaths)),
	setupDialog(std::move(original.setupDialog)),
	debug(std::move(original.debug)),
	demoParts(std::move(original.demoParts)),

	// private
	demoPartFactory(std::move(original.demoPartFactory)),
	rootPath(std::move(original.rootPath)),
	created(std::move(original.created))
{
	original.demoParts.clear();
	original.demoPartFactory = nullptr;
	original.created = false;
}


DemoPlayerSettings& DemoPlayerSettings::operator =(DemoPlayerSettings&& original)
{
	if (this != &original) {
		// Demo model
		sourceFileName = std::move(original.sourceFileName);

		// data
		format = std::move(original.format);
		general = std::move(original.general);
		demoPaths = std::move(original.demoPaths);
		setupDialog = std::move(original.setupDialog);
		debug = std::move(original.debug);
		demoParts = std::move(original.demoParts);

		// private
		demoPartFactory = std::move(original.demoPartFactory);
		rootPath = std::move(original.rootPath);
		created = std::move(original.created);

		// Clear the original
		original.demoParts.clear();
		original.demoPartFactory = nullptr;
		original.created = false;
	}
	return *this;
}


DemoPlayerSettings::~DemoPlayerSettings()
{
	destroy();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Demo model
///////////////////////////////////////////////////////////////////////////////////////////

void DemoPlayerSettings::createFromFile(const IDemoPartFactory* demoPartFactory, const UString& fileName)
{
	VERSO_ASSERT_MSG("verso-demokit", isCreated() == false, "Already created!");

	this->demoPartFactory = demoPartFactory;

	loadFromJson(fileName);

	created = true;
}


void DemoPlayerSettings::destroy()
{
	if (isCreated() == false) {
		return;
	}

	destroyDemoParts();

	this->demoPartFactory = nullptr;

	if (rootPath.value != nullptr) {
		JsonHelper::free(rootPath);
		rootPath.value = nullptr;
	}

	created = false;
}


bool DemoPlayerSettings::isCreated() const
{
	return created;
}


void DemoPlayerSettings::createDemoParts(IWindowOpengl& window)
{
	if (isCreated() == false) {
		VERSO_ERROR("verso-demokit", "createFromFile() must be called before this method.", "");
	}

	for (auto& demoPart : demoParts) {
		demoPart->create(window);
	}
}


void DemoPlayerSettings::destroyDemoParts() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	// Clear & destroy demoParts
	for (auto& demoPart : demoParts) {
		demoPart->destroy();
		if (demoPart != nullptr) {
			delete demoPart;
		}
	}
	demoParts.clear();
}


void DemoPlayerSettings::loadFromJson(const UString& fileName)
{
	sourceFileName = fileName;
	if (this->rootPath.value != nullptr) {
		JsonHelper::free(this->rootPath);
		this->rootPath.value = nullptr;
	}

	this->rootPath = JsonHelper::loadAndParse(File::getBasePath() + sourceFileName);
	importJson(this->rootPath);
}


void DemoPlayerSettings::saveToJson(const UString& fileName)
{
	JSONValue* root = exportJson();
	JsonHelper::saveToFile(fileName, root, true);
	//delete root;
}


std::int32_t DemoPlayerSettings::processDemoPartsRecursive(const JsonPath& currentDemoPartJson, std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator)
{
	if (!currentDemoPartJson.isArray("demoparts")) {
		UString error("Expected array of demoparts at \"");
		error += currentDemoPartJson.currentPath;
		error += ".demoparts\"!";
		VERSO_ILLEGALFORMAT("verso-demokit", error.c_str(), currentDemoPartJson.fileName.c_str());
	}
	else {
		JsonPathArray demoPartsJson = currentDemoPartJson.asArray("demoparts");

		if (demoPartsJson.array.empty()) {
			VERSO_ILLEGALFORMAT("verso-demokit", "demoparts array is empty!", currentDemoPartJson.fileName.c_str());
		}

		for (auto& demoPartPath : demoPartsJson.array) {
			if (demoPartPath.isObject()) {
				// Special group for DemoParts
				if (demoPartPath.readString("type", true) == "verso.group") {
					double nextStart = startAccumulator + demoPartPath.readNumberd("start", false, 0.0);
					int nextPriority = priorityAccumulator + demoPartPath.readNumberi("priority", false, 0);
					jsonOrder = processDemoPartsRecursive(demoPartPath, jsonOrder, nextStart, nextPriority);
				}

				// Normal DemoPart
				else {
					DemoPart* part = demoPartFactory->instance(&demoPaths, demoPartPath, jsonOrder, startAccumulator, priorityAccumulator);
					demoParts.push_back(part);
					jsonOrder++;
				}
			}
			else {
				VERSO_LOG_INFO_VARIABLE("verso-demokit", "demopart", JsonPath::jsonValueToString(demoPartPath.value, "").c_str());
				UString error(R"("demoparts" array contains other than objects at ")" + demoPartPath.currentPath + "\"");
				VERSO_ILLEGALFORMAT("verso-demokit", error.c_str(), currentDemoPartJson.fileName.c_str());
			}
		}
	}
	return jsonOrder;
}


void DemoPlayerSettings::importJson(const JsonPath& parent)
{
	// Format check
	format = parent.readString("format", true);
	if (!format.equals(supportedJsonFormat)) {
		UString error("Unsupported DemoKit file format version ('"+format+"')! Supported formats: "+supportedJsonFormat);
		VERSO_ILLEGALFORMAT("verso-demokit", error.c_str(), parent.fileName.c_str());
	}

	// General
	general.importJson(parent.getAttribute("general", false));

	// Paths
	demoPaths.importJson(parent.getAttribute("paths", false), parent.fileName);

	// Setup dialog
	setupDialog.importJson(parent.getAttribute("setupDialog", false));

	// Debug
	debug.importJson(parent.getAttribute("debug", false));

	// Demoparts
	processDemoPartsRecursive(parent, 0, 0.0, 0);

	updateSubsequentPriorities();
}


JSONValue* DemoPlayerSettings::exportJson() const
{
	JSONObject obj;

	obj[L"format"] = new JSONValue(supportedJsonFormat.toUtf8Wstring());

	// - general
	obj[L"general"] = general.exportJson();

	// - paths
	obj[L"paths"] = demoPaths.exportJson();

	// - Setup dialog
	obj[L"setupDialog"] = setupDialog.exportJson();

	// - debug
	obj[L"debug"] = debug.exportJson();

	// - demoparts
	{
		JSONArray demoPartsArray;

		std::multimap<std::int32_t, DemoPart*> demoPartPriorities;
		for (auto& i : demoParts) {
			i->getSettings().duplicateLevel = 0;
			demoPartPriorities.insert(std::pair<std::int32_t, DemoPart*>(i->getSettings().priority, i));
		}

		for (auto & demoPartPriority : demoPartPriorities) {
			JSONValue* v = demoPartPriority.second->getSettings().exportJson();
			demoPartsArray.push_back(v);
		}

		obj[L"demoparts"] = new JSONValue(demoPartsArray);
	}

	return new JSONValue(obj);
}


std::int32_t DemoPlayerSettings::findDemoPart(DemoPart* demoPart)
{
	if (demoPart == nullptr) {
		return false;
	}

	for (size_t i=0; i<demoParts.size(); ++i) {
		if (demoParts[i] == demoPart) {
			return static_cast<std::int32_t>(i);
		}
	}

	return -1;
}


bool DemoPlayerSettings::moveDemoPart(DemoPart* demoPart, double newStart)
{
	if (findDemoPart(demoPart) < 0) {
		return false;
	}
	if (newStart < 0.0) {
		newStart = 0.0;
	}

	double oldStart = demoPart->getSettings().start;
	demoPart->getSettings().start = newStart;
	for (auto& loopDemoPart : demoParts) {
		if (loopDemoPart != demoPart && loopDemoPart->collidesWithPriority(demoPart)) {
			demoPart->getSettings().start = oldStart;
			return false;
		}
	}

	return true;
}


bool DemoPlayerSettings::resizeDemoPart(DemoPart* demoPart, double newDuration)
{
	if (findDemoPart(demoPart) < 0) {
		return false;
	}
	if (newDuration < 0.01) {
		newDuration = 0.01;
	}

	double oldDuration = demoPart->getSettings().duration;

	demoPart->getSettings().duration = newDuration;
	for (auto& loopDemoPart : demoParts) {
		if (loopDemoPart != demoPart && loopDemoPart->collidesWithPriority(demoPart)) {
			demoPart->getSettings().duration = oldDuration;
			return false;
		}
	}

	return true;
}


void DemoPlayerSettings::updateSubsequentPriorities()
{
	std::multimap<std::int32_t, DemoPart*> demoPartPriorities;

	// Insert demoparts into the multimap by priority as key
	for (auto& demoPart : demoParts) {
		demoPart->getSettings().duplicateLevel = 0;
		demoPartPriorities.insert(std::pair<std::int32_t, DemoPart*>(demoPart->getSettings().priority, demoPart));
	}

	// Set initial subsequentPriorities
	std::int32_t priorityCounter = 0;
	std::int32_t lastPriority = demoPartPriorities.begin()->second->getSettings().priority;
	for (auto& demoPartPriority : demoPartPriorities) {
		auto& settings = demoPartPriority.second->getSettings();
		if (lastPriority != settings.priority) {
			lastPriority = settings.priority;
			priorityCounter++;
		}
		settings.subsequentPriority = priorityCounter;
	}

	// Calculate duplicate priority levels for colliding demoparts and update subsequentPriorities
	std::map<std::int32_t, std::int32_t> maxDuplicateLevelForPriority;
	for (auto itA = demoPartPriorities.begin(); itA != demoPartPriorities.end(); ++itA) {
		for (auto j = itA; j != demoPartPriorities.end(); ++j) {
			DemoPart* a = (*itA).second;
			DemoPart* b = (*j).second;

			if (a != b && a->collidesWithPriority(b) == true) {
				if (a->getSettings().start <= b->getSettings().start) {
					b->getSettings().duplicateLevel++;
					maxDuplicateLevelForPriority[b->getSettings().priority] = b->getSettings().duplicateLevel;
				}
				else {
					a->getSettings().duplicateLevel++;
					maxDuplicateLevelForPriority[a->getSettings().priority] = a->getSettings().duplicateLevel;
				}
			}
		}
	}

	// Update subsequentPriorities based in duplicate levels
	for (auto& demoPart : demoParts) {
		demoPart->getSettings().subsequentPriority += demoPart->getSettings().duplicateLevel;
	}

	// Apply extra "layers" for subsequentPriorities based on duplicate levels
	for (auto& it : maxDuplicateLevelForPriority) {
		std::int32_t priority = it.first;
		std::int32_t maxPriority = it.second;
		for (auto& j : demoParts) {
			auto& settings = j->getSettings();
			if (settings.priority > priority) {
				settings.subsequentPriority += maxPriority;
			}
		}
	}
}


UString DemoPlayerSettings::toString(const UString& newLinePadding) const
{
	UString str;

	str += R"({ "sourceFileName": ")" + sourceFileName + "\"";
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += ",\n" + newLinePadding2 + "\"format\": \"" + format + "\"";

		str += ",\n" + newLinePadding2 + "\"general\": ";
		str += general.toString(newLinePadding2 + "  ");

		str += ",\n" + newLinePadding2 + "\"paths\": ";
		str += demoPaths.toString(newLinePadding2 + "  ");

		str += ",\n" + newLinePadding2 + "\"setupDialog\": ";
		str += setupDialog.toString(newLinePadding2 + "  ");

		str += ",\n" + newLinePadding2 + "\"debug\": ";
		str += debug.toString(newLinePadding2 + "  ");

		str += ",\n" + newLinePadding2 + "\"demoparts\": [";
		{
			UString newLinePadding3 = newLinePadding2 + "  ";
			size_t i = 0;
			for (auto it = demoParts.begin(); it != demoParts.end(); ++it) {
				if (i != 0) {
					str += str += ",\n" + newLinePadding3;
				}
				else {
					str += str += "\n" + newLinePadding3;
				}
				str += (*it)->toString(newLinePadding3);
				i++;
			}
		}
		str += "\n" + newLinePadding2 + "]";
	}
	str += "\n" + newLinePadding + "}";

	return str;
}


UString DemoPlayerSettings::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::DemoPlayerSettings(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

