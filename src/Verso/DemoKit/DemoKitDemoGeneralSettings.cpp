#include <Verso/DemoKit/DemoKitDemoGeneralSettings.hpp>
#include <Verso/Math/ColorGenerator.hpp>

namespace Verso {


UString	defaultName = "";
UString	defaultVersion = "";
UString	defaultAuthors = "";
UString	defaultMusicFileName = "test.mp3";
AspectRatio defaultContentAspectRatio = AspectRatio();
PixelAspectRatio defaultPixelAspectRatio = PixelAspectRatio();
RgbaColorf	defaultBorderColor = ColorGenerator::getColor(ColorRgb::Black);
float defaultTargetFps = 60.0f;
float defaultDuration = 3 * 60.0f;
bool defaultLoop = false;


DemoKitDemoGeneralSettings::DemoKitDemoGeneralSettings() :
	name(defaultName),
	version(defaultVersion),
	authors(defaultAuthors),

	musicFileName(defaultMusicFileName),
	contentAspectRatio(defaultContentAspectRatio),
	pixelAspectRatio(defaultPixelAspectRatio),
	borderColor(defaultBorderColor),
	targetFps(defaultTargetFps),
	duration(defaultDuration),
	loop(defaultLoop)
{
}


DemoKitDemoGeneralSettings::DemoKitDemoGeneralSettings(const DemoKitDemoGeneralSettings& original) :
	name(original.name),
	version(original.version),
	authors(original.authors),

	musicFileName(original.musicFileName),
	contentAspectRatio(original.contentAspectRatio),
	pixelAspectRatio(original.pixelAspectRatio),
	borderColor(original.borderColor),
	targetFps(original.targetFps),
	duration(original.duration),
	loop(original.loop)
{
}


DemoKitDemoGeneralSettings::DemoKitDemoGeneralSettings(DemoKitDemoGeneralSettings&& original) :
	name(std::move(original.name)),
	version(std::move(original.version)),
	authors(std::move(original.authors)),

	musicFileName(std::move(original.musicFileName)),
	contentAspectRatio(std::move(original.contentAspectRatio)),
	pixelAspectRatio(std::move(original.pixelAspectRatio)),
	borderColor(std::move(original.borderColor)),
	targetFps(std::move(original.targetFps)),
	duration(std::move(original.duration)),
	loop(std::move(original.loop))
{
	// Clear original
}


DemoKitDemoGeneralSettings& DemoKitDemoGeneralSettings::operator =(const DemoKitDemoGeneralSettings& original)
{
	if (this != &original) {
		name = original.name;
		version = original.version;
		authors = original.authors;

		musicFileName = original.musicFileName;
		contentAspectRatio = original.contentAspectRatio;
		pixelAspectRatio = original.pixelAspectRatio;
		borderColor = original.borderColor;
		targetFps = original.targetFps;
		duration = original.duration;
		loop = original.loop;
	}
	return *this;
}


DemoKitDemoGeneralSettings& DemoKitDemoGeneralSettings::operator =(DemoKitDemoGeneralSettings&& original)
{
	if (this != &original) {
		name = std::move(original.name);
		version = std::move(original.version);
		authors = std::move(original.authors);

		musicFileName = std::move(original.musicFileName);
		contentAspectRatio = std::move(original.contentAspectRatio);
		pixelAspectRatio = std::move(original.pixelAspectRatio);
		borderColor = std::move(original.borderColor);
		targetFps = std::move(original.targetFps);
		duration = std::move(original.duration);
		loop = std::move(original.loop);

		// Clear original
	}
	return *this;
}


DemoKitDemoGeneralSettings::~DemoKitDemoGeneralSettings()
{
}


void DemoKitDemoGeneralSettings::importJson(const JsonPath& settings)
{
	name = settings.readString("name", false, defaultName);
	version = settings.readString("version", false, defaultVersion);
	authors = settings.readString("authors", false, defaultAuthors);

	musicFileName = settings.readString("music", false, defaultMusicFileName);
	contentAspectRatio = settings.readAspectRatio("contentAspectRatio", false, defaultContentAspectRatio);
	pixelAspectRatio = settings.readPixelAspectRatio("pixelAspectRatio", false, defaultPixelAspectRatio);
	borderColor = settings.readColor("borderColor", false, defaultBorderColor);
	targetFps = settings.readNumberd("targetFps", false, defaultTargetFps);
	duration = settings.readNumberd("duration", false, defaultDuration);
	loop = settings.readBool("loop", false, defaultLoop);
}


JSONValue* DemoKitDemoGeneralSettings::exportJson() const
{
	JSONObject obj;

	obj[L"name"] = new JSONValue(name.toUtf8Wstring());
	obj[L"version"] = new JSONValue(version.toUtf8Wstring());
	obj[L"authors"] = new JSONValue(authors.toUtf8Wstring());

	obj[L"music"] = new JSONValue(musicFileName.toUtf8Wstring());
	obj[L"contentAspectRatio"] = JsonPath::writeString(contentAspectRatio.toString());
	obj[L"pixelAspectRatio"] = JsonPath::writeString(pixelAspectRatio.toString());
	obj[L"borderColor"] = JsonPath::writeColor(borderColor, false); // discard alpha
	obj[L"targetFps"] = new JSONValue(targetFps);
	obj[L"duration"] = new JSONValue(duration);
	obj[L"loop"] = new JSONValue(loop);

	return new JSONValue(obj);
}


UString DemoKitDemoGeneralSettings::toString(const UString& newLinePadding) const
{
	UString str("{");
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += "\n" + newLinePadding2 + "\"name\": \"" + name + "\"";
		str += ",\n" + newLinePadding2 + "\"version\": \"" + version + "\"";
		str += ",\n" + newLinePadding2 + "\"authors\": \"" + authors + "\"";

		str += ",\n" + newLinePadding2 + "\"musicFileName\": \"" + musicFileName + "\"";
		str += ",\n" + newLinePadding2 + "\"contentAspectRatio\": \"" + contentAspectRatio.toString() + "\"";
		str += ",\n" + newLinePadding2 + "\"pixelAspectRatio\": \"" + pixelAspectRatio.toString() + "\"";
		str += ",\n" + newLinePadding2 + "\"borderColor\": \"" + borderColor.toString() + "\"";

		str += ",\n" + newLinePadding2 + "\"targetFps\": ";
		str.append2(targetFps);

		str += ",\n" + newLinePadding2 + "\"duration\": ";
		str.append2(duration);

		str += ",\n" + newLinePadding2 + "\"loop\": ";
		if (loop == true) {
			str += "true";
		}
		else {
			str += "false";
		}
	}
	str += "\n" + newLinePadding + "}";

	return str;
}


UString DemoKitDemoGeneralSettings::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::DemoKitDemoGeneralSettings(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso


