#include <Verso/DemoKit/DemoPaths.hpp>

namespace Verso {


DemoPaths::DemoPaths() :
	jsons(),
	models(),
	shaders(),
	shadersVerso(),
	music(),
	textures(),
	fonts(),
	particles(),
	gui(),
	guiIcons(),
	guiStyles(),
	javascript()
{
}


DemoPaths::DemoPaths(DemoPaths&& original) :
	jsons(std::move(original.jsons)),
	models(std::move(original.models)),
	shaders(std::move(original.shaders)),
	shadersVerso(std::move(original.shadersVerso)),
	music(std::move(original.music)),
	textures(std::move(original.textures)),
	fonts(std::move(original.fonts)),
	particles(std::move(original.particles)),
	gui(std::move(original.gui)),
	guiIcons(std::move(original.guiIcons)),
	guiStyles(std::move(original.guiStyles)),
	javascript(std::move(original.javascript))
{
	// Clear the original
}


DemoPaths& DemoPaths::operator =(DemoPaths&& original)
{
	if (this != &original) {
		jsons = std::move(original.jsons);
		models = std::move(original.models);
		shaders = std::move(original.shaders);
		shadersVerso = std::move(original.shadersVerso);
		music = std::move(original.music);
		textures = std::move(original.textures);
		fonts = std::move(original.fonts);
		particles = std::move(original.particles);
		gui = std::move(original.gui);
		guiIcons = std::move(original.guiIcons);
		guiStyles = std::move(original.guiStyles);
		javascript = std::move(original.javascript);

		// Clear the original
	}
	return *this;
}


DemoPaths::~DemoPaths()
{
}


void DemoPaths::importJson(const JsonPath& demoPathsJson, const UString& jsonFileNamePath)
{
	UString basePath(File::getBasePath());
	UString fileNamePath(File::addSlashToPathIfNeeded(File::getFileNamePath(jsonFileNamePath)));
	if (fileNamePath.beginsWith(basePath)) {
		fileNamePath = File::addSlashToPathIfNeeded(fileNamePath.substring(basePath.size()));
	}

	jsons = fileNamePath;
	models = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("models", false, "models"));
	shaders = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("shaders", false, "shaders"));
	shadersVerso = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("shadersVerso", false, "shaders/verso"));
	music = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("music", false, "music"));
	textures = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("textures", false, "textures"));
	fonts = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("fonts", false, "fonts"));
	particles = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("particles", false, "particles"));
	gui = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("gui", false, "gui"));
	guiIcons = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("guiIcons", false, "gui/icons"));
	guiStyles = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("guiStyles", false, "gui/styles"));
	javascript = fileNamePath + File::addSlashToPathIfNeeded(demoPathsJson.readString("javascript", false, "js"));
}


JSONValue* DemoPaths::exportJson() const
{
	JSONObject obj;
	obj[L"models"] = new JSONValue(models.toUtf8Wstring());
	obj[L"shaders"] = new JSONValue(shaders.toUtf8Wstring());
	obj[L"shadersVerso"] = new JSONValue(shadersVerso.toUtf8Wstring());
	obj[L"music"] = new JSONValue(music.toUtf8Wstring());
	obj[L"textures"] = new JSONValue(textures.toUtf8Wstring());
	obj[L"fonts"] = new JSONValue(fonts.toUtf8Wstring());
	obj[L"particles"] = new JSONValue(particles.toUtf8Wstring());
	obj[L"gui"] = new JSONValue(gui.toUtf8Wstring());
	obj[L"guiIcons"] = new JSONValue(guiIcons.toUtf8Wstring());
	obj[L"guiStyles"] = new JSONValue(guiStyles.toUtf8Wstring());
	obj[L"javascript"] = new JSONValue(javascript.toUtf8Wstring());

	return new JSONValue(obj);
}


UString DemoPaths::pathJsons() const
{
	return File::getBasePath() + jsons;
}


UString DemoPaths::pathModels() const
{
	return File::getBasePath() + models;
}


UString DemoPaths::pathShaders() const
{
	return File::getBasePath() + shaders;
}


UString DemoPaths::pathShadersVerso() const
{
	return File::getBasePath() + shadersVerso;
}


UString DemoPaths::pathMusic() const
{
	return File::getBasePath() + music;
}


UString DemoPaths::pathTextures() const
{
	return File::getBasePath() + textures;
}


UString DemoPaths::pathFonts() const
{
	return File::getBasePath() + fonts;
}


UString DemoPaths::pathParticles() const
{
	return File::getBasePath() + particles;
}


UString DemoPaths::pathGui() const
{
	return File::getBasePath() + gui;
}


UString DemoPaths::pathGuiIcons() const
{
	return File::getBasePath() + guiIcons;
}


UString DemoPaths::pathGuiStyles() const
{
	return File::getBasePath() + guiStyles;
}


UString DemoPaths::pathJavascript() const
{
	return File::getBasePath() + javascript;
}


UString DemoPaths::toString(const UString& newLinePadding) const
{
	UString str;

	str += "{ \"jsons\": \"" + jsons + "\"";
	{
		UString newLinePadding2 = newLinePadding + "  ";
		str += ",\n" + newLinePadding2 + "\"models\": \"" + models + "\"";
		str += ",\n" + newLinePadding2 + "\"shaders\": \"" + shaders + "\"";
		str += ",\n" + newLinePadding2 + "\"shadersVerso\": \"" + shadersVerso + "\"";
		str += ",\n" + newLinePadding2 + "\"music\": \"" + music + "\"";
		str += ",\n" + newLinePadding2 + "\"textures\": \"" + textures + "\"";
		str += ",\n" + newLinePadding2 + "\"fonts\": \"" + fonts + "\"";
		str += ",\n" + newLinePadding2 + "\"particles\": \"" + particles + "\"";
		str += ",\n" + newLinePadding2 + "\"gui\": \"" + gui + "\"";
		str += ",\n" + newLinePadding2 + "\"guiIcons\": \"" + guiIcons + "\"";
		str += ",\n" + newLinePadding2 + "\"guiStyles\": \"" + guiStyles + "\"";
		str += ",\n" + newLinePadding2 + "\"javascript\": \"" + javascript + "\"";
	}
	str += "\n" + newLinePadding + "}";

	return str;
}


UString DemoPaths::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::DemoPaths(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso

