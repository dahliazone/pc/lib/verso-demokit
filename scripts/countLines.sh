#!/bin/bash

wc -l ../include/Verso/*.hpp \
../include/Verso/DemoKit/*.hpp \
../include/Verso/DemoKit/DemoParts/*.hpp \
../include/Verso/Gui/*.hpp \
../src/Verso/DemoKit/*.cpp \
../src/Verso/DemoKit/DemoParts/*.cpp \
../src/Verso/Gui/*.cpp
