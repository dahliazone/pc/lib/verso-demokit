#ifndef VERSO_DEMOKIT_GUI_IMGUITIMELINE_HPP
#define VERSO_DEMOKIT_GUI_IMGUITIMELINE_HPP

#include <Verso/verso-demokit-common.hpp>

namespace Verso {


class DemoPlayerSettings;
class DemoPart;


VERSO_DEMOKIT_API bool demoTimeLine(DemoPlayerSettings& settings, float* timeNowSeconds, float* viewScrollPosSeconds, float* viewWidthSeconds,
							   DemoPart** selected);


} // End namespace ImGui

#endif // End header guard

