#ifndef VERSO_DEMOKIT_DEMOPARTS_SPRITES3D_HPP
#define VERSO_DEMOKIT_DEMOPARTS_SPRITES3D_HPP

#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/Render/Camera.hpp>
#include <Verso/Render/Sprite/Sprite3d.hpp>
#include <Verso/Render/ClearParam.hpp>
#include <Verso/Render/DepthParam.hpp>
#include <Verso/Render/Camera/CameraParam.hpp>
#include <Verso/Render/Sprite/Sprite3dParam.hpp>

namespace Verso {


class Sprites3d : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	CameraParam cameraParam;
	std::vector<Sprite3dParam> sprite3dParams;

	// State
	bool created;
	IWindowOpengl* window;
	CameraTarget camera;
	std::vector<Texture*> textures;
	std::vector<Sprite3d> sprites;

public:
	VERSO_DEMOKIT_API Sprites3d(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	Sprites3d(const Sprites3d& original) = delete;

	VERSO_DEMOKIT_API Sprites3d(Sprites3d&& original) noexcept;

	Sprites3d& operator =(const Sprites3d& original) = delete;

	VERSO_DEMOKIT_API virtual Sprites3d& operator =(Sprites3d&& original) noexcept;

	VERSO_DEMOKIT_API virtual ~Sprites3d() override;

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API virtual void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void destroy() noexcept override;

	VERSO_DEMOKIT_API virtual bool isCreated() const override;

	VERSO_DEMOKIT_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

public: // toString
	VERSO_DEMOKIT_API virtual UString toString(const UString& newLinePadding) const override;

	VERSO_DEMOKIT_API virtual UString toStringDebug(const UString& newLinePadding) const override;


	friend std::ostream& operator <<(std::ostream& ost, const Sprites3d& right)
	{
		return ost << right.toString("");
	}

};


} // End namespace Verso

#endif // End header guard

