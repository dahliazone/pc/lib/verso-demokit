#ifndef VERSO_DEMOKIT_DEMOPARTS_PARTICLES_HPP
#define VERSO_DEMOKIT_DEMOPARTS_PARTICLES_HPP

#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/Render/Particle/Sequencers/ParticleSequencer.hpp>
#include <Verso/Render/ClearParam.hpp>
#include <Verso/Render/DepthParam.hpp>
#include <Verso/Render/Particle/ParticlesParam.hpp>
#include <Verso/Render/Particle/BehaviourParam.hpp>
#include <Verso/Render/Particle/EmitterParam.hpp>
#include <Verso/Render/Particle/SequencerParam.hpp>

namespace Verso {

//
// example JSON (simple)
//

class Particles : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	ParticlesParam particlesParam;
	BehaviourParam behaviourParam;
	EmitterParam emitterParam;
	SequencerParam sequencerParam;

	// State
	bool created;
	ParticleBehaviour* particleBehaviour;
	ParticleSystem* particleSystem;
	ParticleEmitter* particleEmitter;
	ParticleSequencer* particleSequencer;

public:
	VERSO_DEMOKIT_API Particles(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	Particles(const Particles& original) = delete;

	VERSO_DEMOKIT_API Particles(Particles&& original) noexcept;

	VERSO_DEMOKIT_API virtual ~Particles() override;

	Particles& operator =(const Particles& original) = delete;

	VERSO_DEMOKIT_API virtual Particles& operator =(Particles&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API virtual void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_DEMOKIT_API virtual bool isCreated() const override;

	VERSO_DEMOKIT_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;
};


} // End namespace Verso

#endif // End header guard

