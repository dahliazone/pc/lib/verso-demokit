#pragma once

#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/Render/ClearParam.hpp>
#include <Verso/Render/DepthParam.hpp>
#include <Verso/Render/Font/FontFixedWidthParam.hpp>
#include <Verso/Render/Camera/CameraTarget.hpp>
#include <Verso/Render/Font/BitmapFontFixedWidth.hpp>

namespace Verso {


class Text : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	FontFixedWidthParam fontParam;
	UString text;
	Vector2fKeyframes relativePositionKeyframes;
	FloatKeyframes alphaKeyframes;
	Vector2f relativeCharacterSize;
	Vector2f relativeSpacing;
	RefScale refScale;
	FloatKeyframes angleZKeyframes;
	float revealCharactersPerSecond;

	// State
	bool created;
	IWindowOpengl* window;
	CameraTarget camera;
	BitmapFontFixedWidth* font;
	UString textToBeRendered;

public:
	VERSO_DEMOKIT_API Text(const DemoPaths* demoPaths, const JsonPath& demoPartJson,
		 std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	Text(const Text& original) = delete;

	VERSO_DEMOKIT_API Text(Text&& original) noexcept;

	VERSO_DEMOKIT_API virtual ~Text() override;

	Text& operator =(const Text& original) = delete;

	VERSO_DEMOKIT_API virtual Text& operator =(Text&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API virtual void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void destroy() noexcept override;

	VERSO_DEMOKIT_API virtual bool isCreated() const override;

	VERSO_DEMOKIT_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;
};


} // End namespace Verso
