#ifndef VERSO_DEMOKIT_DEMOPARTS_IMAGEVIEWER_HPP
#define VERSO_DEMOKIT_DEMOPARTS_IMAGEVIEWER_HPP

#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/Render/Vao.hpp>
#include <Verso/Math/Align.hpp>
#include <Verso/Math/FloatKeyframes.hpp>
#include <Verso/Math/Vector2Keyframes.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Render/ClearParam.hpp>
#include <Verso/Render/DepthParam.hpp>
#include <Verso/Render/Camera/CameraParam.hpp>
#include <Verso/Render/Texture/TextureParametersParam.hpp>

#include <Verso/Render/Camera/CameraTarget.hpp>
#include <Verso/Math/RefScale.hpp>

namespace Verso {


class ImageViewer : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	CameraParam cameraParam;
	BlendMode blendMode;
	TextureParametersParam textureParam;
	Vector2fKeyframes positionKeyframes;
	Align alignFromPosition;
	Vector2fKeyframes relSizeKeyframes;
	RefScale refScale;
	FloatKeyframes rotationAngleKeyframes;
	FloatKeyframes alphaKeyframes;
	Vector2fKeyframes textureOffsetKeyframes;

	// State
	bool created;
	Texture texture;
	CameraTarget camera;

public:
	VERSO_DEMOKIT_API ImageViewer(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	ImageViewer(const ImageViewer& original) = delete;

	VERSO_DEMOKIT_API ImageViewer(ImageViewer&& original) noexcept;

	virtual ImageViewer& operator =(const ImageViewer& original) = delete;

	VERSO_DEMOKIT_API virtual ImageViewer& operator =(ImageViewer&& original) noexcept;

	VERSO_DEMOKIT_API virtual ~ImageViewer() override;

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API virtual void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_DEMOKIT_API virtual bool isCreated() const override;

	VERSO_DEMOKIT_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;

public: // toString
	VERSO_DEMOKIT_API virtual UString toString(const UString& newLinePadding) const override;

	VERSO_DEMOKIT_API virtual UString toStringDebug(const UString& newLinePadding) const override;


	friend std::ostream& operator <<(std::ostream& ost, const ImageViewer& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

