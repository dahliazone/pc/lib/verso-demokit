#ifndef VERSO_DEMOKIT_DEMOPARTS_HEIGHTMAP_HPP
#define VERSO_DEMOKIT_DEMOPARTS_HEIGHTMAP_HPP

#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/Render/ClearParam.hpp>
#include <Verso/Render/DepthParam.hpp>
#include <Verso/Render/Material/MaterialLightmapsPhong3d_GridHeightmapDisplacement.hpp>
#include <Verso/Render/Vao.hpp>

namespace Verso {

//
// example JSON (simple)
//

class Heightmap : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	Vector2f unitSize;
	Vector2i gridSize;
	Vector3f offsetPosition;
	//UString sourceFileName;

	// State
	bool created;
	Vao vao;
	MaterialLightmapsPhong3d_GridHeightmapDisplacement material;

public:
	VERSO_DEMOKIT_API Heightmap(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	Heightmap(const Heightmap& original) = delete;

	VERSO_DEMOKIT_API Heightmap(Heightmap&& original) noexcept;

	VERSO_DEMOKIT_API virtual ~Heightmap() override;

	Heightmap& operator =(const Heightmap& original) = delete;

	VERSO_DEMOKIT_API virtual Heightmap& operator =(Heightmap&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API virtual void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_DEMOKIT_API virtual bool isCreated() const override;

	VERSO_DEMOKIT_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;
};


} // End namespace Verso

#endif // End header guard

