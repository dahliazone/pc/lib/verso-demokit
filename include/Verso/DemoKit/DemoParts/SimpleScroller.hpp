#ifndef VERSO_DEMOKIT_DEMOPARTS_SIMPLESCROLLER_HPP
#define VERSO_DEMOKIT_DEMOPARTS_SIMPLESCROLLER_HPP

#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/Render/ClearParam.hpp>
#include <Verso/Render/DepthParam.hpp>
#include <Verso/Render/Font/FontFixedWidthParam.hpp>
#include <Verso/Render/Camera.hpp>
#include <Verso/Render/Font/BitmapFontFixedWidth.hpp>

namespace Verso {


class SimpleScroller : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	FontFixedWidthParam fontParam;
	UString text;
	FloatKeyframes relativePositionYKeyframes;
	float scrollSpeedX;
	float scrollPosX;
	FloatKeyframes alphaKeyframes;
	Vector2fKeyframes relativeCharacterSizeKeyframes;
	Vector2fKeyframes relativeSpacingKeyframes;
	RefScale refScale;
	FloatKeyframes angleZKeyframes;

	// State
	bool created;
	IWindowOpengl* window;
	CameraTarget camera;
	float currentScrollPosX;
	BitmapFontFixedWidth* font;

public:
	VERSO_DEMOKIT_API SimpleScroller(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	SimpleScroller(const SimpleScroller& original) = delete;

	VERSO_DEMOKIT_API SimpleScroller(SimpleScroller&& original) noexcept;

	VERSO_DEMOKIT_API virtual ~SimpleScroller() override;

	SimpleScroller& operator =(const SimpleScroller& original) = delete;

	VERSO_DEMOKIT_API virtual SimpleScroller& operator =(SimpleScroller&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API virtual void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void destroy() noexcept override;

	VERSO_DEMOKIT_API virtual bool isCreated() const override;

	VERSO_DEMOKIT_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;
};


} // End namespace Verso

#endif // End header guard

