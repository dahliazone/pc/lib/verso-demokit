#ifndef VERSO_DEMOKIT_DEMOPARTS_IMGUITEST_HPP
#define VERSO_DEMOKIT_DEMOPARTS_IMGUITEST_HPP

#include <Verso/Gui/ImGuiVersoImpl.hpp>
#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/Render/ClearParam.hpp>
#include <Verso/Render/DepthParam.hpp>
#include <Verso/Math/RgbaColorf.hpp>

namespace Verso {

//
// example JSON
//
//	{ "name": "ImGuiTest", "type": "imguitest",
//		"start": 0, "duration": 3000, "priority": 0, "params": {
//			"clear": { "clearFlag": "SolidColor", "color": [0.45, 0.56, 0.60, 1] } }}
//

class ImGuiTest : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;

	// State
	bool created;
	ImGuiVersoImpl imgui;
	bool showTestWindow;
	bool showAnotherWindow;
	RgbaColorf bgColor;

public:
	VERSO_DEMOKIT_API ImGuiTest(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	ImGuiTest(const ImGuiTest& original) = delete;

	VERSO_DEMOKIT_API ImGuiTest(ImGuiTest&& original);

	VERSO_DEMOKIT_API virtual ~ImGuiTest() override;

	ImGuiTest& operator =(const ImGuiTest& original) = delete;

	VERSO_DEMOKIT_API virtual ImGuiTest& operator =(ImGuiTest&& original);

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API virtual void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_DEMOKIT_API virtual bool isCreated() const override;

	VERSO_DEMOKIT_API virtual void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_DEMOKIT_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;
};


} // End namespace Verso

#endif // End header guard

