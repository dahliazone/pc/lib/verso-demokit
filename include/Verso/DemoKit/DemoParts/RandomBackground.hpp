#ifndef VERSO_DEMOKIT_DEMOPARTS_RANDOMBACKGROUND_HPP
#define VERSO_DEMOKIT_DEMOPARTS_RANDOMBACKGROUND_HPP

#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/Render/ClearParam.hpp>
#include <Verso/Render/DepthParam.hpp>
#include <Verso/Render/Texture.hpp>
#include <Verso/Render/Camera/CameraTarget.hpp>
#include <Verso/Math/Random.hpp>

namespace Verso {

//
// minimal example JSON (grey background and paths.textures directory will be used)
// { "name": "Random background", "type": "verso.randombackground",
//		"start": 0, "duration": 60, "priority": 0, "params": {}},
//
// full example JSON (relative path from paths.textures will be used)
// { "name": "Random background", "type": "verso.randombackground",
//		"start": 0, "duration": 60, "priority": 0, "params": {
//			"sourcePath": "backgrounds/",
//			"clear": { "clearFlag": "SolidColor", "color": [ 1, 0, 1, 1 ] }
//		}},
//
class RandomBackground : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	UString sourcePath;

	// State
	bool created;
	Texture texBg;
	CameraTarget camera;

public:
	VERSO_DEMOKIT_API RandomBackground(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	RandomBackground(const RandomBackground& original) = delete;

	VERSO_DEMOKIT_API RandomBackground(RandomBackground&& original) noexcept;

	RandomBackground& operator =(const RandomBackground& original) = delete;

	VERSO_DEMOKIT_API virtual RandomBackground& operator =(RandomBackground&& original) noexcept;

	VERSO_DEMOKIT_API virtual ~RandomBackground() override;

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API virtual void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_DEMOKIT_API virtual bool isCreated() const override;

	VERSO_DEMOKIT_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;
};


} // End namespace Verso

#endif // End header guard

