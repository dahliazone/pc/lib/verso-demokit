#ifndef VERSO_DEMOKIT_DEMOPARTS_CLEARSCREEN_HPP
#define VERSO_DEMOKIT_DEMOPARTS_CLEARSCREEN_HPP

#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/Render/Opengl.hpp>
#include <Verso/Render/ClearParam.hpp>

namespace Verso {


class ClearScreen : public DemoPart
{
private:
	// Params
	ClearParam clearParam;

	// State
	bool created;

public:
	VERSO_DEMOKIT_API ClearScreen(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	ClearScreen(const ClearScreen& original) = delete;

	VERSO_DEMOKIT_API ClearScreen(ClearScreen&& original) noexcept;

	ClearScreen& operator =(const ClearScreen& original) = delete;

	VERSO_DEMOKIT_API virtual ClearScreen& operator =(ClearScreen&& original) noexcept;

	VERSO_DEMOKIT_API virtual ~ClearScreen() override;

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API virtual void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_DEMOKIT_API virtual bool isCreated() const override;

	VERSO_DEMOKIT_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;
};


} // End namespace Verso

#endif // End header guard

