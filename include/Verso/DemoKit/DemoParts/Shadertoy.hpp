#ifndef VERSO_DEMOKIT_DEMOPARTS_SHADERTOY_HPP
#define VERSO_DEMOKIT_DEMOPARTS_SHADERTOY_HPP

#include <Verso/Math/FloatKeyframes.hpp>
#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/Render/ClearParam.hpp>
#include <Verso/Render/DepthParam.hpp>
#include <Verso/Render/Shader/ShaderParam.hpp>
#include <Verso/DemoKit/ChannelParam.hpp>
#include <Verso/Render/Opengl/BlendMode.hpp>
#include <Verso/Render/ShaderProgram.hpp>
#include <Verso/Render/Vao/Vao.hpp>
#include <Verso/Render/Camera/CameraTarget.hpp>

namespace Verso {


class Shadertoy : public DemoPart
{
private:
	// Params
	ClearParam clearParam;
	DepthParam depthParam;
	BlendMode blendMode;
	ShaderParam shaderParam;
	ChannelParam channelParam;
	FloatKeyframes alphaKeyframes;

	// State
	bool created;
	ShaderProgram shader;
	Vao vao;
	std::vector<Texture*> channels;
	CameraTarget camera;

public:
	VERSO_DEMOKIT_API Shadertoy(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	Shadertoy(const Shadertoy& original) = delete;

	VERSO_DEMOKIT_API Shadertoy(Shadertoy&& original) noexcept;

	VERSO_DEMOKIT_API virtual ~Shadertoy() override;

	Shadertoy& operator =(const Shadertoy& original) = delete;

	VERSO_DEMOKIT_API virtual Shadertoy& operator =(Shadertoy&& original) noexcept;

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API virtual void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_DEMOKIT_API virtual bool isCreated() const override;

	VERSO_DEMOKIT_API virtual void render(IWindowOpengl& window, const FrameTimestamp& time) override;
};


} // End namespace Verso

#endif // End header guard

