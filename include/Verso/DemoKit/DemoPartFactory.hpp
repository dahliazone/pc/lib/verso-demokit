#pragma once

#include <Verso/DemoKit/IDemoPartFactory.hpp>

namespace Verso {


class VERSO_DEMOKIT_API DemoPartFactory : public IDemoPartFactory
{
public:
	DemoPartFactory();
	virtual ~DemoPartFactory() override;

public:
	virtual DemoPart* instance(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) const override;
};


} // End namespace Verso
