#ifndef VERSO_DEMOKIT_IDEMOKITAPP_HPP
#define VERSO_DEMOKIT_IDEMOKITAPP_HPP

#include <Verso/DemoKit/IDemoPartFactory.hpp>
#include <Verso/System/IProgramOpengl.hpp>
#include <Verso/DemoKit/DemoPlayer.hpp>

namespace Verso {


class VERSO_DEMOKIT_API IDemoKitApp : public IProgramOpengl
{
public:
	virtual ~IDemoKitApp() = 0;

//protected:
//	IDemoKitApp(const IDemoKitApp&) = default;
//	IDemoKitApp& operator=(const IDemoKitApp&) = default;

public: // IDemoKitApp interface
	virtual void preCreate(const AudioDevice& audioDevice, bool muteAudio) = 0;

	virtual UString getSourceJsonFileName() const = 0;

	virtual const DemoPlayerSettings& getDemoPlayerSettings() const = 0;

	virtual bool saveRenderFboToFile(const UString& fileName, const ImageSaveFormat& imageSaveFormat) const = 0;

public: // IProgramOpengl interface
	// ...
};


} // End namespace Verso

#endif // End header guard

