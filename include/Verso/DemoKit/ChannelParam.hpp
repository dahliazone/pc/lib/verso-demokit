#ifndef VERSO_DEMOKIT_CHANNELPARAM_HPP
#define VERSO_DEMOKIT_CHANNELPARAM_HPP

#include <Verso/verso-demokit-common.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class ChannelParam
{
public:
	std::vector<UString> sources;

public:
	VERSO_DEMOKIT_API ChannelParam();

	VERSO_DEMOKIT_API ChannelParam(const ChannelParam& original);

	VERSO_DEMOKIT_API ChannelParam(const ChannelParam&& original) noexcept;

	VERSO_DEMOKIT_API ChannelParam& operator =(const ChannelParam& original);

	VERSO_DEMOKIT_API ChannelParam& operator =(ChannelParam&& original) noexcept;

	VERSO_DEMOKIT_API ~ChannelParam();

public:
	VERSO_DEMOKIT_API void parse(const JsonPath& channelArray, bool required);

public: // toString
	VERSO_DEMOKIT_API UString toString(const UString& newLinePadding) const;

	VERSO_DEMOKIT_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const ChannelParam& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

