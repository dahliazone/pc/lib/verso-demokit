#ifndef VERSO_DEMOKIT_DEMOPLAYERDEBUGSETTINGS_HPP
#define VERSO_DEMOKIT_DEMOPLAYERDEBUGSETTINGS_HPP

#include <Verso/verso-demokit-common.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/JsonHelperDemoKit.hpp>
#include <Verso/DemoKit/PlayMode.hpp>
#include <Verso/DemoKit/DebugMode.hpp>

namespace Verso {


class DemoPlayerDebugSettings
{
public:
	PlayMode playMode;
	DebugMode debugMode;
	UString fontFileName;
	float fontSizeSmaller;
	float fontSizeBigger;
	bool playbackRangeEnabled;
	Ranged playbackRange;
    bool midiEnabled;

public:
	VERSO_DEMOKIT_API DemoPlayerDebugSettings();

	VERSO_DEMOKIT_API DemoPlayerDebugSettings(const DemoPlayerDebugSettings& original);

	VERSO_DEMOKIT_API DemoPlayerDebugSettings(DemoPlayerDebugSettings&& original);

	VERSO_DEMOKIT_API DemoPlayerDebugSettings& operator =(const DemoPlayerDebugSettings& original);

	VERSO_DEMOKIT_API DemoPlayerDebugSettings& operator =(DemoPlayerDebugSettings&& original);

	VERSO_DEMOKIT_API ~DemoPlayerDebugSettings();

public: // json
	VERSO_DEMOKIT_API void importJson(const JsonPath& parent);

	VERSO_DEMOKIT_API JSONValue* exportJson() const;

public: // toString
	VERSO_DEMOKIT_API UString toString(const UString& newLinePadding) const;

	VERSO_DEMOKIT_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoPlayerDebugSettings& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

