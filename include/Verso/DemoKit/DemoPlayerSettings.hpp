#ifndef VERSO_DEMOKIT_DEMOPLAYERSETTINGS_HPP
#define VERSO_DEMOKIT_DEMOPLAYERSETTINGS_HPP

#include <Verso/DemoKit/DemoPaths.hpp>
#include <Verso/DemoKit/DemoKitSetupDialogSettings.hpp>
#include <Verso/DemoKit/DemoKitDemoGeneralSettings.hpp>
#include <Verso/DemoKit/DemoPlayerDebugSettings.hpp>
#include <Verso/DemoKit/DemoPartFactory.hpp>

namespace Verso {


class DemoPlayerSettings
{
public: // static
	VERSO_DEMOKIT_API static UString supportedJsonFormat;

public: // Demo model
	UString sourceFileName;

	// data
	UString format;
	DemoKitDemoGeneralSettings general;
	DemoPaths demoPaths;
	DemoKitSetupDialogSettings setupDialog;
	DemoPlayerDebugSettings debug;
	std::vector<DemoPart*> demoParts;

private:
	const IDemoPartFactory* demoPartFactory;
	JsonPath rootPath;
	bool created;

public:
	VERSO_DEMOKIT_API DemoPlayerSettings();

	DemoPlayerSettings(const DemoPlayerSettings& original) = delete;

	VERSO_DEMOKIT_API DemoPlayerSettings(DemoPlayerSettings&& original);

	DemoPlayerSettings& operator =(const DemoPlayerSettings& original) = delete;

	VERSO_DEMOKIT_API DemoPlayerSettings& operator =(DemoPlayerSettings&& original);

	VERSO_DEMOKIT_API ~DemoPlayerSettings();

public: // Demo model
	VERSO_DEMOKIT_API void createFromFile(const IDemoPartFactory* factory, const UString& fileName);

	VERSO_DEMOKIT_API void destroy();

	VERSO_DEMOKIT_API bool isCreated() const;

	VERSO_DEMOKIT_API void createDemoParts(IWindowOpengl& window);

	VERSO_DEMOKIT_API void destroyDemoParts() VERSO_NOEXCEPT;

	VERSO_DEMOKIT_API void loadFromJson(const UString& fileName);

	VERSO_DEMOKIT_API void saveToJson(const UString& fileName);

private:
	VERSO_DEMOKIT_API std::int32_t processDemoPartsRecursive(const JsonPath& currentDemoPartJson, std::int32_t jsonOrder = 0, double startAccumulator = 0.0, int priorityAccumulator = 0);

public:
	VERSO_DEMOKIT_API void importJson(const JsonPath& parent);

	VERSO_DEMOKIT_API JSONValue* exportJson() const;

	VERSO_DEMOKIT_API std::int32_t findDemoPart(DemoPart* demoPart);

	VERSO_DEMOKIT_API bool moveDemoPart(DemoPart* demoPart, double newStart);

	VERSO_DEMOKIT_API bool resizeDemoPart(DemoPart* demoPart, double newDuration);

	// Must be called after changing any DemoPartSettings priority
	VERSO_DEMOKIT_API void updateSubsequentPriorities();

public: // toString
	VERSO_DEMOKIT_API UString toString(const UString& newLinePadding) const;

	VERSO_DEMOKIT_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoPlayerSettings& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

