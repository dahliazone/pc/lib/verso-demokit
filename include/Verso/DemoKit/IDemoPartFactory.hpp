#pragma once

#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class VERSO_DEMOKIT_API IDemoPartFactory
{
public:
	virtual ~IDemoPartFactory() = 0;

public:
	virtual DemoPart* instance(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator) const = 0;
};


} // End namespace Verso
