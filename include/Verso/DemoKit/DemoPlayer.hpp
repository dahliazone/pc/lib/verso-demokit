#ifndef VERSO_DEMOKIT_DEMOPLAYER_HPP
#define VERSO_DEMOKIT_DEMOPLAYER_HPP

#include <Verso/DemoKit/DemoPlayerSettings.hpp>
#include <Verso/DemoKit/PlayState.hpp>
#include <Verso/Audio/Audio2d.hpp>
#include <list>

namespace Verso {


class DemoPlayer
{
public:
	DemoPlayerSettings settings;

private:
	PlayState playState;
	PlayState previousRealPlayState;
	std::list<DemoPart*> pastDemoParts;
	std::list<DemoPart*> currentDemoParts;
	std::list<DemoPart*> futureDemoParts;
	double targetDeltaTime;
	double elapsedSeconds;
	Audio2d* audio2d;
	bool created;
	bool demoOver;

public:
	VERSO_DEMOKIT_API DemoPlayer();

	DemoPlayer(const DemoPlayer& original) = delete;

	VERSO_DEMOKIT_API DemoPlayer(DemoPlayer&& original);

	DemoPlayer& operator =(const DemoPlayer& original) = delete;

	VERSO_DEMOKIT_API DemoPlayer& operator =(DemoPlayer&& original);

	VERSO_DEMOKIT_API ~DemoPlayer();

public: // Player model
	VERSO_DEMOKIT_API void startDemo(IWindowOpengl& window);

	VERSO_DEMOKIT_API void stopDemo();

	VERSO_DEMOKIT_API void createFromFile(const IDemoPartFactory* factory, const UString& fileName);

	VERSO_DEMOKIT_API void createDemoParts(IWindowOpengl& window, Audio2d& audio2d);

	VERSO_DEMOKIT_API void destroy() VERSO_NOEXCEPT;

	VERSO_DEMOKIT_API void destroyDemoParts();


	VERSO_DEMOKIT_API bool isCreated() const
	{
		return created;
	}


	VERSO_DEMOKIT_API bool isDemoOver() const
	{
		return demoOver;
	}


	VERSO_DEMOKIT_API void handleInput(IWindowOpengl& window, const FrameTimestamp& time);

	VERSO_DEMOKIT_API void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event);

	VERSO_DEMOKIT_API void render(IWindowOpengl& window, const FrameTimestamp& time);

	VERSO_DEMOKIT_API void gotoAbsolute(IWindowOpengl& window, double absoluteSeconds);

	VERSO_DEMOKIT_API void gotoRelative(IWindowOpengl& window, double relativeSeconds);

private:
	VERSO_DEMOKIT_API void rewindAudio(double absoluteSeconds);

	VERSO_DEMOKIT_API bool updatePlayerModel(IWindowOpengl& window, double deltaTime);

	VERSO_DEMOKIT_API void divideDeltaTime(double deltaTime, std::vector<double>& times);

	VERSO_DEMOKIT_API bool updateFutureDemoPartsToCurrentDemoParts(IWindowOpengl& window, double secondsElapsedDemo);

	VERSO_DEMOKIT_API bool updateCurrentDemoPartsToPastDemoParts(const IWindowOpengl& window, double secondsElapsedDemo);

	VERSO_DEMOKIT_API void sortFutureDemoParts();

	VERSO_DEMOKIT_API void sortActiveDemoParts();

	VERSO_DEMOKIT_API void runActiveDemoParts(IWindowOpengl& window, double deltaTime, double secondsElapsedDemo);

	VERSO_DEMOKIT_API UString getActiveDemoParts() const;

	VERSO_DEMOKIT_API void printOutDemoParts() const;

public:

	VERSO_DEMOKIT_API const DemoPlayerSettings& getDemoPlayerSettings() const
	{
		return settings;
	}


	VERSO_DEMOKIT_API PlayState getPlayState() const
	{
		return playState;
	}


	VERSO_DEMOKIT_API void setPlayState(const PlayState& playState)
	{
		// If previous play state was "real" then save it
		if (this->playState == PlayState::Stopped ||
				this->playState == PlayState::Paused ||
				this->playState == PlayState::Playing) {
			this->previousRealPlayState = this->playState;
		}

		if (playState == PlayState::Playing) {
			rewindAudio(elapsedSeconds);
			audio2d->startMusic();
		}
		else if (playState == PlayState::Paused ||
				 playState == PlayState::Stopped) {
			audio2d->pauseMusic();
		}

		this->playState = playState;
	}


	VERSO_DEMOKIT_API PlayState getPreviousRealPlayState() const
	{
		return previousRealPlayState;
	}


	VERSO_DEMOKIT_API double getElapsedSeconds() const
	{
		return elapsedSeconds;
	}


	VERSO_DEMOKIT_API void addDemoPart(DemoPart* demoPart)
	{
		// Add to futureDemoParts
		futureDemoParts.push_back(demoPart);

		// Sort the futureDemoParts list
		futureDemoParts.sort();
	}


public: // toString
	VERSO_DEMOKIT_API UString toString() const
	{
		UString str;

		str += "\n\n==== Player model ====";
		str += "playState = \"";
		str += playStateToString(playState);
		str += "\"";

		return str;
	}


	VERSO_DEMOKIT_API UString toStringDebug() const
	{
		UString str("Verso::DemoPlayer(\n");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const DemoPlayer& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso

#endif // End header guard

