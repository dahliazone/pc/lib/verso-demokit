#ifndef VERSO_DEMOKIT_DEMOKITUI_HPP
#define VERSO_DEMOKIT_DEMOKITUI_HPP

#include <Verso/Gui/ImGuiVersoImpl.hpp>
#include <Verso/System/IProgramOpengl.hpp>
#include <Verso/Audio/Audio2d.hpp>
#include <Verso/Render/TextureAtlas.hpp>
#include <Verso/DemoKit/IDemoKitApp.hpp>
#include <Verso/Input/Midi/Midi.hpp>

struct ImFont;
struct ImVec2;

namespace Verso {


class DemoKitUi : public IProgramOpengl
{
public:
	bool created;
	ImGuiVersoImpl imgui;
	InputController* inputController;
	IDemoKitApp& demoKitApp;
	DemoPlayer& demoPlayer;
	ImFont* smallerFont;
	ImFont* biggerFont;
	TextureAtlas icons;
	UString selectedIconSet;
	RgbaColorf editorBgColor;
	Midi midi;
	AxisHandle a1;
	AxisHandle a2;
	AxisHandle a3;
	AxisHandle a4;
	ButtonHandle buttonPlayPause;
	ButtonHandle buttonRewindToStart;
	ButtonHandle buttonToggleFullscreen;
	ButtonHandle buttonToggleEditor;
	ButtonHandle b1;
	ButtonHandle b2;
	ButtonHandle b3;
	ButtonHandle b4;
	ButtonHandle b5;
	ButtonHandle b6;
	ButtonHandle b7;
	ButtonHandle b8;
	ButtonHandle b9;
	ButtonHandle b10;
	ButtonHandle b11;
	ButtonHandle b12;
	ButtonHandle b13;
	ImGuiStyleVerso style;

	// state
	bool enableUi;
	bool viewFullscreen2;
	bool viewPanelsToggle;
	bool viewMainControls;
	bool viewLeftPanel;
	bool viewRightPanel;
	bool viewHorizontalPanel;
	bool viewImGuiDemoWindow;
	bool viewTestWindow;
	int mainMenuHeight;
	DemoPart* selectedDemoPart;
	Texture* renderTexture;

public:
	VERSO_DEMOKIT_API DemoKitUi(IDemoKitApp& demoKitApp, DemoPlayer& demoPlayer);

	DemoKitUi(const DemoKitUi& original) = delete;

	VERSO_DEMOKIT_API DemoKitUi(DemoKitUi&& original) noexcept;

	DemoKitUi& operator =(const DemoKitUi& original) = delete;

	VERSO_DEMOKIT_API DemoKitUi& operator =(DemoKitUi&& original) noexcept;

	VERSO_DEMOKIT_API ~DemoKitUi() override;

public:
	VERSO_DEMOKIT_API bool getEnableUi() const;

	VERSO_DEMOKIT_API void updateRenderTexture(Texture* renderTexture);

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API virtual void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API void destroy() VERSO_NOEXCEPT override;

	VERSO_DEMOKIT_API bool isCreated() const override;

	VERSO_DEMOKIT_API void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_DEMOKIT_API void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_DEMOKIT_API void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_DEMOKIT_API bool isQuitting() const override;

	VERSO_DEMOKIT_API void quit() override;

private:
	bool imguiImageButton(
			const UString& subAtlasName, const UString& iconName, const UString& id, int framePadding = -1,
			const RgbaColorf& bgColor = RgbaColorf(0, 0, 0, 0), const RgbaColorf& tintColor = RgbaColorf(1, 1, 1, 1));

private:
	void menuBar(const IWindowOpengl& window);

	void menuBarFile(const IWindowOpengl& window);

	void menuBarEdit(const IWindowOpengl& window);

	void menuBarView(const IWindowOpengl& window);

	void panelMainView(const IWindowOpengl& window, const ImVec2& windowPos, const ImVec2& windowSize);

	void panelMainControls(IWindowOpengl& window);

	void panelLeft(const IWindowOpengl& window);

	void panelRight(const IWindowOpengl& window);

	void panelHorizontal(IWindowOpengl& window);

	void tabDemoPart(const IWindowOpengl& window);

	void tabMaterial(const IWindowOpengl& window);

	void tabPostProc(const IWindowOpengl& window);

	void tabDemo(const IWindowOpengl& window);

	void tabTimeline(IWindowOpengl& window);

	void showExampleAppConstrainedResize(bool* p_open);

	void showHelpMarker(const char* desc);

	void showExampleAppPropertyEditor(bool* p_open);

	void actionPlayPause(IWindowOpengl& window);

	void actionRewindToStart(IWindowOpengl& window);

	void actionToggleFullscreen(IWindowOpengl& window);

	void actionToggleEditor(IWindowOpengl& window);
};


} // End namespace Verso

#endif // End header guard

