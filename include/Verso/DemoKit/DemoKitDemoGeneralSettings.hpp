#ifndef VERSO_DEMOKIT_DEMOKITDEMOGENERALSETTINGS_HPP
#define VERSO_DEMOKIT_DEMOKITDEMOGENERALSETTINGS_HPP

#include <Verso/verso-demokit-common.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class DemoKitDemoGeneralSettings
{
public:
	UString name;
	UString version;
	UString authors;

	UString musicFileName;
	AspectRatio contentAspectRatio;
	PixelAspectRatio pixelAspectRatio;
	RgbaColorf borderColor;
	double targetFps;
	double duration;
	bool loop;

public:
	VERSO_DEMOKIT_API DemoKitDemoGeneralSettings();

	VERSO_DEMOKIT_API DemoKitDemoGeneralSettings(const DemoKitDemoGeneralSettings& original);

	VERSO_DEMOKIT_API DemoKitDemoGeneralSettings(DemoKitDemoGeneralSettings&& original);

	VERSO_DEMOKIT_API DemoKitDemoGeneralSettings& operator =(const DemoKitDemoGeneralSettings& original);

	VERSO_DEMOKIT_API DemoKitDemoGeneralSettings& operator =(DemoKitDemoGeneralSettings&& original);

	VERSO_DEMOKIT_API ~DemoKitDemoGeneralSettings();

public: // json
	VERSO_DEMOKIT_API void importJson(const JsonPath& settings);

	VERSO_DEMOKIT_API JSONValue* exportJson() const;

public: // toString
	VERSO_DEMOKIT_API UString toString(const UString& newLinePadding) const;

	VERSO_DEMOKIT_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoKitDemoGeneralSettings& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard
