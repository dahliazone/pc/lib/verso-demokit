#ifndef VERSO_DEMOKIT_DEMOPART_HPP
#define VERSO_DEMOKIT_DEMOPART_HPP

#include <Verso/DemoKit/DemoPartSettings.hpp>
#include <Verso/System/IProgramOpengl.hpp>
#include <Verso/System/JsonHelper.hpp>
#include <Verso/System/Logger.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/DemoKit/DemoPaths.hpp>

namespace Verso {


//
// \TODO: Should there be:
// virtual void activate()
// virtual void deActivate()
// Which are called before and after the demopart is running
//
class VERSO_DEMOKIT_API DemoPart : public IProgramOpengl
{
public: // static
	static size_t nextInstanceId;

public:
	const DemoPaths* demoPaths;

private:
	size_t instanceId;
	DemoPartSettings* settings;

	double elapsedSeconds;
	double surplusSeconds;
	double accumulatorSeconds;

	bool incrementalRun;
	double runMaxDeltaSeconds;
	bool incrementalRender;
	double renderMaxDeltaSeconds;

public:
	DemoPart(
			const DemoPaths* demoPaths, const JsonPath& demoPartJson,
			std::int32_t jsonOrder, double startAccumulator, int priorityAccumulator);

	DemoPart(const DemoPart& original) = delete;

	DemoPart(DemoPart&& original);

	virtual ~DemoPart() override;

	DemoPart& operator =(const DemoPart& original) = delete;

	DemoPart& operator =(DemoPart&& original);

public: // IProgramOpengl interface
	//virtual void create(IWindowOpengl& window) = 0;

	//virtual void destroy() VERSO_NOEXCEPT = 0;

	//virtual bool isCreated() const = 0;

	void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	//virtual void render(IWindowOpengl& window, const FrameTimestamp& time) = 0;

	bool isQuitting() const override;

	void quit() override;

public:
	// To use the DemoPart timing model then call this instead of handleInput()
	void update(IWindowOpengl& window, const FrameTimestamp& time); //double deltaSeconds, double secondsElapsed)

	DemoPartSettings& getSettings() const;

	bool hasStarted(double secondsElapsedDemo) const;

	bool hasEnded(double secondsElapsedDemo) const;

	bool isActive(double secondsElapsedDemo) const;

	bool collidesWithPriority(DemoPart* demoPart) const;

	bool collidesWithoutPriority(DemoPart* demoPart) const;

	size_t getInstanceId() const;

	UString getInstanceStr() const;

	UString getName() const;

	double getStartSeconds() const;

	double getEndSeconds() const;

	double getDurationSeconds() const;

	double getElapsedSeconds() const;

	void setElapsedSeconds(double elapsedSeconds);

	void addSurplusSeconds(double surplusSeconds);

	bool isIncrementalRun() const;

	bool isIncrementalRender() const;

	void enableIncrementalRun(double minFramesPerSecond, double maxDeltaSeconds);

	void disableIncrementalRun();

	void enableIncrementalRender(double minFramesPerSecond, double maxDeltaSeconds);

	void disableIncrementalRender();

public: // comparison
	bool operator < (const DemoPart& rhs) const;

	bool operator <= (const DemoPart& rhs) const;

	bool operator > (const DemoPart& rhs) const;

	bool operator >= (const DemoPart& rhs) const;

public: // toString
	virtual UString toString(const UString& newLinePadding) const;

	virtual UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoPart& right)
	{
		return ost << right.toString("");
	}
};



/**
 * Custom sort function for the class (sorts by start time, renderPriority & json order).
 */
inline bool DemoPartSortPredicateForStartSeconds(const DemoPart& lhs, const DemoPart& rhs)
{
	if (lhs.getSettings().start == rhs.getSettings().start) {
		if (lhs.getSettings().priority == rhs.getSettings().priority) {
			return lhs.getSettings().jsonOrder < rhs.getSettings().jsonOrder;
		}
		else {
			return lhs.getSettings().priority < rhs.getSettings().priority;
		}
	}
	else {
		return lhs.getSettings().start < rhs.getSettings().start;
	}
}


inline bool DemoPartPtrSortPredicateForStartSeconds(const DemoPart* lhs, const DemoPart* rhs)
{
	if (lhs->getSettings().start == rhs->getSettings().start) {
		if (lhs->getSettings().priority == rhs->getSettings().priority) {
			return lhs->getSettings().jsonOrder < rhs->getSettings().jsonOrder;
		}
		else {
			return lhs->getSettings().priority < rhs->getSettings().priority;
		}
	}
	else {
		return lhs->getSettings().start < rhs->getSettings().start;
	}
}


/**
 * Custom sort function for the class (sorts by renderPriority & json order).
 */
inline bool DemoPartSortPredicateForPriority(const DemoPart& lhs, const DemoPart& rhs)
{
	if (lhs.getSettings().priority == rhs.getSettings().priority) {
		return lhs.getSettings().jsonOrder < rhs.getSettings().jsonOrder;
	}
	else {
		return lhs.getSettings().priority < rhs.getSettings().priority;
	}
}


inline bool DemoPartPtrSortPredicateForPriority(const DemoPart* lhs, const DemoPart* rhs)
{
	if (lhs->getSettings().priority == rhs->getSettings().priority) {
		return lhs->getSettings().jsonOrder < rhs->getSettings().jsonOrder;
	}
	else {
		return lhs->getSettings().priority < rhs->getSettings().priority;
	}
}


} // End namespace Verso

#endif // End header guard

