#ifndef VERSO_DEMOKIT_DEMOPATHS_HPP
#define VERSO_DEMOKIT_DEMOPATHS_HPP

#include <Verso/verso-demokit-common.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class DemoPaths
{
public:
	UString jsons;
	UString models;
	UString shaders;
	UString shadersVerso;
	UString music;
	UString textures;
	UString fonts;
	UString particles;
	UString gui;
	UString guiIcons;
	UString guiStyles;
	UString javascript;

public:
	VERSO_DEMOKIT_API DemoPaths();

	DemoPaths(const DemoPaths& original) = delete;

	VERSO_DEMOKIT_API DemoPaths(DemoPaths&& original);

	DemoPaths& operator =(const DemoPaths& original) = delete;

	VERSO_DEMOKIT_API DemoPaths& operator =(DemoPaths&& original);

	VERSO_DEMOKIT_API ~DemoPaths();

public: // json
	VERSO_DEMOKIT_API void importJson(const JsonPath& demoPathsJson, const UString& jsonFileNamePath);

	VERSO_DEMOKIT_API JSONValue* exportJson() const;

public: // getters & setters
	VERSO_DEMOKIT_API UString pathJsons() const;

	VERSO_DEMOKIT_API UString pathModels() const;

	VERSO_DEMOKIT_API UString pathShaders() const;

	VERSO_DEMOKIT_API UString pathShadersVerso() const;

	VERSO_DEMOKIT_API UString pathMusic() const;

	VERSO_DEMOKIT_API UString pathTextures() const;

	VERSO_DEMOKIT_API UString pathFonts() const;

	VERSO_DEMOKIT_API UString pathParticles() const;

	VERSO_DEMOKIT_API UString pathGui() const;

	VERSO_DEMOKIT_API UString pathGuiIcons() const;

	VERSO_DEMOKIT_API UString pathGuiStyles() const;

	VERSO_DEMOKIT_API UString pathJavascript() const;

public: // toString
	VERSO_DEMOKIT_API UString toString(const UString& newLinePadding) const;

	VERSO_DEMOKIT_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoPaths& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard

