#ifndef VERSO_DEMOKIT_PLAYSTATE_HPP
#define VERSO_DEMOKIT_PLAYSTATE_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class PlayState : std::int8_t {
	Unset = 0,
	Stopped,
	Paused,
	Playing,
	Rewind,
	Forward,
	RewindFine,
	ForwardFine,
	RewindFast,
	ForwardFast
};


inline UString playStateToString(const PlayState& playState)
{
	if (playState ==  PlayState::Stopped)
		return "Stopped";
	else if (playState == PlayState::Paused)
		return "Paused";
	else if (playState == PlayState::Playing)
		return "Playing";
	else if (playState == PlayState::Rewind)
		return "Rewind";
	else if (playState == PlayState::Forward)
		return "Forward";
	else if (playState == PlayState::RewindFine)
		return "RewindFine";
	else if (playState == PlayState::ForwardFine)
		return "ForwardFine";
	else if (playState == PlayState::RewindFast)
		return "RewindFast";
	else if (playState == PlayState::ForwardFast)
		return "ForwardFast";
	else if (playState == PlayState::Unset)
		return "Unset";
	else
		return "Unknown value";
}



} // End namespace Verso

#endif // End header guard

