#ifndef VERSO_DEMOKIT_DEMOKITSETUPDIALOGSETTINGS_HPP
#define VERSO_DEMOKIT_DEMOKITSETUPDIALOGSETTINGS_HPP

#include <Verso/verso-demokit-common.hpp>
#include <Verso/System/JsonHelper.hpp>
#include <Verso/Gui/ImGuiStyleVerso.hpp>

namespace Verso {


class DemoKitSetupDialogSettings
{
public:
	UString windowTitle;
	UString logoFileName;
	UString fontFileName;
	RgbaColorf backgroundColor;
	UString startButtonText;
	UString quitButtonText;
	ImGuiStyleVerso style;

public:
	VERSO_DEMOKIT_API DemoKitSetupDialogSettings();

	VERSO_DEMOKIT_API DemoKitSetupDialogSettings(const DemoKitSetupDialogSettings& original);

	VERSO_DEMOKIT_API DemoKitSetupDialogSettings(DemoKitSetupDialogSettings&& original);

	VERSO_DEMOKIT_API DemoKitSetupDialogSettings& operator =(const DemoKitSetupDialogSettings& original);

	VERSO_DEMOKIT_API DemoKitSetupDialogSettings& operator =(DemoKitSetupDialogSettings&& original);

	VERSO_DEMOKIT_API ~DemoKitSetupDialogSettings();

public: // json
	VERSO_DEMOKIT_API void importJson(const JsonPath& settings);

	VERSO_DEMOKIT_API JSONValue* exportJson() const;

public: // toString
	VERSO_DEMOKIT_API UString toString(const UString& newLinePadding) const;

	VERSO_DEMOKIT_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoKitSetupDialogSettings& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso

#endif // End header guard
