#pragma once

#include <Verso/verso-demokit-common.hpp>
#include <Verso/System/JsonHelper.hpp>

namespace Verso {


class DemoPartSettings
{
public:
	UString name;
	UString type;
	double start;
	double duration;
	std::int32_t priority;
	std::int32_t subsequentPriority;
	std::int32_t duplicateLevel;
	std::int32_t jsonOrder;
	JSONValue paramsJsonValue;

public:
	VERSO_DEMOKIT_API DemoPartSettings(const JsonPath& demoPartJson,
									   std::int32_t jsonOrder,
									   double startAccumulator,
									   int priorityAccumulator);

	VERSO_DEMOKIT_API DemoPartSettings(const UString& name = "<unset>",
									   const UString& type = "<unset>",
									   double start = 0.0,
									   double duration = 1000.0,
									   std::int32_t priority = 0,
									   std::int32_t jsonOrder = 0,
									   const JSONValue& paramsJsonValue = JSONValue());

	DemoPartSettings(const DemoPartSettings& original) = delete;

	VERSO_DEMOKIT_API DemoPartSettings(DemoPartSettings&& original);

	DemoPartSettings& operator =(const DemoPartSettings& original) = delete;

	VERSO_DEMOKIT_API DemoPartSettings& operator =(DemoPartSettings&& original);

	VERSO_DEMOKIT_API ~DemoPartSettings();

public:
	VERSO_DEMOKIT_API void importJson(const JsonPath& demoPartJson,
									  std::int32_t jsonOrder,
									  double startAccumulator,
									  int priorityAccumulator);

	VERSO_DEMOKIT_API JSONValue* exportJson() const;

public: // toString
	VERSO_DEMOKIT_API UString toString(const UString& newLinePadding) const;

	VERSO_DEMOKIT_API UString toStringDebug(const UString& newLinePadding) const;


	friend std::ostream& operator <<(std::ostream& ost, const DemoPartSettings& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso
