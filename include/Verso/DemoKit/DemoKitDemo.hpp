#ifndef VERSO_DEMOKIT_DEMOKITDEMO_HPP
#define VERSO_DEMOKIT_DEMOKITDEMO_HPP

#include <Verso/DemoKit/IDemoKitApp.hpp>
#include <Verso/DemoKit/DemoKitUi.hpp>
#include <Verso/Render/Camera/CameraTarget.hpp>
#include <Verso/Render/ShaderProgram.hpp>

namespace Verso {


class DemoKitDemo : public IDemoKitApp
{
public:
	bool created;
	bool quitting;
	const IDemoPartFactory* demoPartFactory;
	UString sourceJsonFileName;
	IWindowOpengl* window;
	DemoPlayer demoPlayer;
	DemoKitUi* demoKitUi;
	AudioDevice audioDevice;
	bool muteAudio;

	CameraTarget* camera;
//	Texture testTexture;

	Rectf imageRect;

	Fbo* fbo;
//	Texture* iChannel0;
//	Texture* iDepth0;
//	Texture* iChannel1;
//	Texture* iChannel2;

public:
	VERSO_DEMOKIT_API DemoKitDemo(const IDemoPartFactory* demoPartFactory, const UString& sourceJsonFileName);

	DemoKitDemo(const DemoKitDemo& original) = delete;

	VERSO_DEMOKIT_API DemoKitDemo(DemoKitDemo&& original);

	DemoKitDemo& operator =(const DemoKitDemo& original) = delete;

	VERSO_DEMOKIT_API DemoKitDemo& operator =(DemoKitDemo&& original);

	VERSO_DEMOKIT_API ~DemoKitDemo() override;

public: // IProgramOpengl interface
	VERSO_DEMOKIT_API void create(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API void reset(IWindowOpengl& window) override;

	VERSO_DEMOKIT_API void destroy() VERSO_NOEXCEPT override;

	VERSO_DEMOKIT_API bool isCreated() const override;

	VERSO_DEMOKIT_API void handleInput(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_DEMOKIT_API void handleEvent(IWindowOpengl& window, const FrameTimestamp& time, const Event& event) override;

	VERSO_DEMOKIT_API void render(IWindowOpengl& window, const FrameTimestamp& time) override;

	VERSO_DEMOKIT_API bool isQuitting() const override;

	VERSO_DEMOKIT_API void quit() override;

public: // IDemoKitApp interface
	VERSO_DEMOKIT_API void preCreate(const AudioDevice& audioDevice, bool muteAudio) override;

	VERSO_DEMOKIT_API UString getSourceJsonFileName() const override;

	VERSO_DEMOKIT_API const DemoPlayerSettings& getDemoPlayerSettings() const override;

	VERSO_DEMOKIT_API bool saveRenderFboToFile(const UString& fileName, const ImageSaveFormat& imageSaveFormat) const override;
};


} // End namespace Verso

#endif // End header guard

