#pragma once

#include <Verso/verso-demokit-common.hpp>
#include <Verso/System/JsonHelperImgui.hpp>
#include <Verso/DemoKit/PlayMode.hpp>
#include <Verso/DemoKit/DebugMode.hpp>

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#include <imgui.h>

namespace Verso {

namespace JsonHelper {


VERSO_DEMOKIT_API PlayMode readPlayMode(const JsonPath& parent, const UString& name, bool required = false, const PlayMode& defaultValue = PlayMode::Production);
VERSO_DEMOKIT_API DebugMode readDebugMode(const JsonPath& parent, const UString& name, bool required = false, const DebugMode& defaultValue = DebugMode::Editor);


} // End namespace JsonHelper


} // End namespace Verso
