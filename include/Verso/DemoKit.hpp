#ifndef VERSO_DEMOKIT_MAININCLUDES_DEMOKIT_HPP
#define VERSO_DEMOKIT_MAININCLUDES_DEMOKIT_HPP


#include <Verso/DemoKit/DemoPart.hpp>
#include <Verso/DemoKit/DemoPartSettings.hpp>
#include <Verso/DemoKit/DemoPlayer.hpp>
#include <Verso/DemoKit/DemoKitSetupDialog.hpp>
#include <Verso/DemoKit/IDemoPartFactory.hpp>
#include <Verso/DemoKit/PlayMode.hpp>
#include <Verso/DemoKit/PlayState.hpp>
#include <Verso/DemoKit/DemoPartFactory.hpp>


#endif // End header guard

