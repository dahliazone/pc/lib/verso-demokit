#include <Verso/DemoKit.hpp>

using namespace std;
using namespace Verso;

int main(int argc, char* argv[])
{
	(void)argc; (void)argv;
	VERSO_LOG_INFO("verso-demokit/examples", "Hello verso-demokit example!");

	PlayMode playMode = PlayMode::Development;
	VERSO_LOG_INFO_VARIABLE("verso-demokit/examples", "playModeToString(playMode)", playModeToString(playMode));

	PlayState playState = PlayState::Rewind;
	VERSO_LOG_INFO_VARIABLE("verso-demokit/examples", "playStateToString(playState)", playStateToString(playState));

	return EXIT_SUCCESS;
}

