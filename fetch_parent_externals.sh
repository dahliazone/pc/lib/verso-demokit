#!/bin/bash

echo "verso-demokit: Fetching externals from repositories..."

if [ ! -d ../verso-imgui ]; then
        git clone https://gitlab.com/dahliazone/pc/lib/verso-imgui.git ../verso-imgui
else
        pushd ../verso-imgui
        git pull --rebase
        popd
fi

echo "verso-imgui: Resursing into..."
pushd ../verso-imgui
./fetch_parent_externals.sh
popd

echo "verso-demokit: All done"

