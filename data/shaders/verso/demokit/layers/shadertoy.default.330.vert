#version 330 core
precision highp float;

layout (location = 0) in vec2 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 fragCoord;

void main()
{
	fragCoord = position;
	//gl_Position = vec4(VertexPosition, 0.0, 1.0);
	gl_Position = projection * view * model * vec4(position, 0.0, 1.0);
}

